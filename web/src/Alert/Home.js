import React, { Fragment } from "react";
import { useAlert } from "react-alert";

const Home = () => {
    const alert = useAlert();

    return (
        <Fragment>
            <button
                style={{background:"transparent",border:"none",color:"red",fontWeight:"bold"}}
                onClick={() => {
                    alert.success("bu satadionlarni online band qilish uchun sayt");
                }}
            >
               nima bu ? |
            </button>
            <button
                style={{background:"transparent",border:"none",color:"red",fontWeight:"bold"}}
                onClick={() => {
                    alert.success("xaritadagi o'zingizga yaqin va maqul bo'lgan satdionlardan birini tanlang va buyurtma vaqtlari telelefon raqam kiriting stadionchi siz bilan bog'lanadi");
                }}
            >
                bu qanday ishlaydi ? |
            </button>
            <button
                style={{background:"transparent",border:"none",color:"red",fontWeight:"bold"}}
                onClick={() => {
                    alert.success("buyurtmani telegram @brbtbot orqali bersangiz ham bo'ladi agar sizda stadium bo'lsa un o'sha bot orqali ro'yxatdan o'tkazishingiz mumkin..!  @brbtbot=> syet markazidagi yashil logotiv ostida ");
                }}
            >
                malumot
            </button>
        </Fragment>
    );
};

export default Home;
