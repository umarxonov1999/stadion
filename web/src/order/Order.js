import React, {Component} from 'react';
import './Order.css'
import like from "../image/like.png";
import phone from "../image/telephone.png";
import mony from "../image/mony1.gif";
import {api, apiTg} from "../api/api";
import ImageZoom from 'react-medium-image-zoom'


import axios from "axios";


class Order extends Component {

    constructor(props) {
        super(props);
        this.state = {
            busyTimeState: [
                {
                    startDate: "18:00",
                    endDate: "19:50",
                    time: "2021-05-01"
                }
            ],
            orderCount: null,
            stadiumId: null,
            show: false,
            stationId: props.info.id,
            stadiumPrice: null,
            stadiumPhoto: props.info.photo
        }
    }

    render() {

        const {cancel, info} = this.props;

        const busyTime = (time, index) => {
            return (
                <p className={"timeBusy"}>
                    <span>{time.time}</span> ➡️ <span>{time.startDate}</span> - <span>{time.endDate}</span></p>
            )
        }

        const photo = (photo, index) => {
            console.log(photo)
            return (
                <ImageZoom
                    image={{
                        src: photo.link,
                        alt: index,
                        className: 'img',
                        style: {height: '40px'}
                    }}
                    zoomImage={{
                        style: {width: '500px', height: '900px', position: "absolute"},
                        src: photo,
                        alt: index
                    }}
                />
            )
        }

        const sendOrder = () => {
            let day = document.getElementById("day").value;
            let telephone = document.getElementById("telephone").value;
            let startDate = document.getElementById("startDate").value;
            let endDate = document.getElementById("endDate").value;

            console.log(day + "  " + telephone + "  " + startDate + "  " + endDate + "  " + info.id);

            let obj = {
                stadiumId: info.id,
                startDate: startDate,
                endDate: endDate,
                time: day,
                phoneNumber: telephone
            };

            console.log(obj)
            axios.post(apiTg + "order/clientCreate", obj).then(res => {
                cancel(res.data.message, '#05fd00')
            }).catch(err => {
                console.error(err.response.data);
                cancel(err.response.data.message, '#f83a3a')
            });

        }

        const getPrice = () => {
            let startDate = document.getElementById("startDate").value;
            let endDate = document.getElementById("endDate").value;
            if (info.id && startDate && endDate) {
                axios.get(api + "order/price/" + info.id + "/" + startDate + "/" + endDate).then(res => {
                    if (res.data.objectKoinot < 0) {
                        this.setState({stadiumPrice: null})
                    } else {
                        this.setState({stadiumPrice: res.data.objectKoinot})
                    }
                });
            }
        }

        return (
            <div className={"formOrder"}>
                <div className="signupSection">
                    <div className="info">

                        {/*<p className={"lesson"}>{info.address}</p>*/}
                        <p className={"lesson"}>
                            <a href={info.phone_number} className={"lesson"}>
                                <img src={phone} alt="stadium" width={"25px"}/>
                                {info.phone_number}
                            </a>


                        </p>
                        <img src={like} alt="stadium" width={"25px"}/>
                        {info.stadium_like}
                        <p className={"lesson"}>ish vaqti {info.opening_time} - {info.closing_time}</p>
                        <p className={"lesson"}> o'lchami {info.height} X {info.width}</p>
                        <p className={"lesson"}>{info.change_price_time} gacha narxi {info.price_day_time}</p>
                        <p className={"lesson"}>{info.change_price_time} dan keyin narxi {info.price_night_time}</p>


                        <div className={"menuPhoto"}>

                            {info.photo.map(photo)}

                        </div>


                        <div className={"overflow"}>
                            <h4>bron qilingan vaqtlar</h4>
                            {info.orders.map(busyTime)}
                        </div>

                    </div>
                    <form className="signupForm" name="signupform">
                        <div onClick={() => cancel("", '#00f3a2')} className={"cancelOrderForm"}>
                            <h3>X</h3>
                        </div>


                        <h2>band qilish</h2>
                        <ul className="noBullet">
                            <li>
                                <label htmlFor="telephone"/>
                                <input
                                    className="inputFields"
                                    id="telephone"
                                    name="telephone"
                                    placeholder="telefon nomer"
                                    type="text"
                                />
                            </li>
                            <li>
                                <label htmlFor="day">kun </label>
                                <input
                                    onChange={getPrice}
                                    className="inputFields"
                                    id="day"
                                    name="day"
                                    placeholder="Username"
                                    type="date"
                                />
                            </li>
                            <li>
                                <label htmlFor="startDate">qaysi vaqtdan </label>
                                <input
                                    onChange={getPrice}
                                    className="inputFields"
                                    id="startDate"
                                    name="startDate"
                                    type="time"
                                    min="09:00"
                                    max="18:00"
                                />
                            </li>
                            <li>
                                <label htmlFor="endDate">qaysi vaqt gacha </label>
                                <input
                                    onChange={getPrice}
                                    className="inputFields"
                                    id="endDate"
                                    name="endDate"
                                    type="time"
                                    min="09:00"
                                    max="18:00"
                                />
                            </li>
                            <li id="center-btn">
                                <i className={"sum"}>{this.state.stadiumPrice} {this.state.stadiumPrice ?
                                    <img src={mony} width={"30px"}/> : ""}</i>
                                <input type="button" onClick={sendOrder} id="join-btn" name="join" alt="yuborish"
                                       value="yuborish"/>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        );
    }
}

export default Order;
