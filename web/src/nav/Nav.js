import React, {Component} from 'react';
import koinot from "../image/logo.png";
import user from "../image/user.gif";
import order from "../image/order.gif";
import CountUp from "react-countup";
import "./nav.css"

class Nav extends Component {
    render() {
        const {orderCount, userCount} = this.props;

        return (
            <div>

                <div className="container">
                    <div className="backgroundImg">
                        <img src={"https://ae01.alicdn.com/kf/Hed4f955738cc45cfaf2d475341e5c22dE/-.jpg"} alt="stadium"
                             width={"30%"}/>

                        <img src={"https://pixy.org/src/64/thumbs350/643906.jpg"} alt="stadium" width={"30%"}/>
                    </div>

                    <div className="logoContent">
                        <a href={"https://t.me/brbtbot"}><img src={koinot} alt="stadium" className={"logo"}/></a>
                    </div>

                    <div className="countImg">
                        <div className="userCount">
                            <img src={user} alt="user"/>
                            <p>

                                <CountUp
                                    start={0.0}
                                    end={userCount}
                                    duration={3}
                                    separator="5"
                                    decimals={0}
                                    // decimal=","
                                    prefix="Odam "
                                    // suffix=" left"
                                    onEnd={() => console.log('Ended! 👏')}
                                    onStart={() => console.log('Started! 💨')}
                                >
                                </CountUp>
                            </p>

                        </div>
                        <div className="orderCount">
                            <img src={order} alt="order"/>
                            <p>

                                <CountUp
                                    start={0.0}
                                    end={orderCount}
                                    duration={3}
                                    separator="5"
                                    decimals={0}
                                    // decimal=","
                                    prefix="Buyurtma "
                                    // suffix=" left"
                                    onEnd={() => console.log('Ended! 👏')}
                                    onStart={() => console.log('Started! 💨')}
                                >
                                </CountUp>
                            </p>

                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

export default Nav;
