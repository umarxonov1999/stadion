import React, {Component} from 'react';
import {GoogleApiWrapper, Map, Marker} from 'google-maps-react';
import axios from "axios";
import marker from './marker.png'
import Order from "../../order/Order";
import Nav from "../../nav/Nav";
import {api} from "../../api/api";

class GoogleMaps extends Component {

    constructor(props) {
        super(props);
        this.state = {
            infoStadium: [],
            userCount: null,
            orderCount: null,
            stadiumInfo: {},
            show: false,
            cancelMessage: true,
            message: null,
        }
    }

    componentDidMount() {
        axios.get(api + "stadium").then(res => {
            this.setState({infoStadium: res.data.objectKoinot.stadiums})
            this.setState({userCount: res.data.objectKoinot.userCount})
            this.setState({orderCount: res.data.objectKoinot.orderCount})
        });
    }

    render() {
        const oderStadium = (id) => {

            axios.get(api + "stadium/" + id).then(res => {
                console.log(res.data.objectKoinot)
                this.setState({stadiumInfo: res.data.objectKoinot})
                this.setState({show: true})
            });

        }

        const cancel = (message, color) => {
            console.log("color " + color)
            console.log("message " + message)
            this.setState({show: false});
            this.setState({message: message});
            if (message.length > 0) {
                let alert = document.getElementById("alerts");
                alert.style.display = "block"
            }
            let alert = document.getElementById("alerts");
            console.log("div  " + alert);
            alert.style.backgroundColor = color;
            alert.style.color = "black";

        }

        const cancelMessage = () => {
            let alert = document.getElementById("alerts");
            alert.style.display = "none"
        }

        const points = (point, index) => {
            return (
                <Marker
                    onClick={() => oderStadium(point.id)}
                    title={point.name}
                    name={'SOMA'}
                    position={{lat: point.lat, lng: point.lng}}
                    icon={{
                        url: marker,
                    }}
                />
            )


        }

        return (
            <div>


                <div className="alert" id="alerts">
                    <span className="closebtn" onClick={cancelMessage}>&times;</span>
                    <strong>BRBT </strong> {this.state.message}
                </div>


                <Nav
                    userCount={this.state.userCount}
                    orderCount={this.state.orderCount}
                />
                {
                    this.state.show ?
                        <Order
                            cancel={cancel}
                            info={this.state.stadiumInfo}
                        /> : ""
                }

                <Map
                    google={this.props.google}
                    options={{}}
                    initialCenter={
                        {
                            lat: 41.26625608278661,
                            lng: 69.23837961789673,
                        }
                    }
                    zoom={12}
                    onClick={this.onMapClicked}
                    style={{width: '100%', height: '100%', position: 'relative', display: "inline"}}
                    className={'map'}
                >
                    {this.state.infoStadium.map(points)}
                </Map>
            </div>
        );
    }
}


export default GoogleApiWrapper({
    apiKey: ("AIzaSyCJoTS-1FoSMMXA6Y2ogl8MxMACY-CMEL4")
})(GoogleMaps)
