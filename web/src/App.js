import './App.css';
import GoogleMaps from "./map/googleMaps/googleMaps";
import {ScrollTop} from 'react-window-scroll-top';
import {positions, Provider} from "react-alert";
import AlertTemplate from "react-alert-template-basic";
import React from "react";
import Home from "./Alert/Home";


function App() {
    const options = {
        timeout: 500000,
        position: positions.TOP_CENTER
    };
    return (
        <div className="App">
            <Provider template={AlertTemplate} {...options}>
                <Home/>
            </Provider>
            <GoogleMaps/>
            <ScrollTop
                delay={2}
                placement="bottom"
                size="10px"
                text="up"
            />

        </div>
    );
}

export default App;
