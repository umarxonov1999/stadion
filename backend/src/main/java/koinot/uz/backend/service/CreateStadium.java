package koinot.uz.backend.service;

import koinot.uz.backend.bot.BRBT_Bot;
import koinot.uz.backend.entity.Stadium;
import koinot.uz.backend.entity.StadiumPhoto;
import koinot.uz.backend.entity.User;
import koinot.uz.backend.entity.enums.AttachmentTypeEnumWhy;
import koinot.uz.backend.entity.enums.UserState;
import koinot.uz.backend.entity.enums.UserType;
import koinot.uz.backend.exception.ExceptionSend;
import koinot.uz.backend.repository.StadiumPhotoRepository;
import koinot.uz.backend.repository.StadiumRepository;
import koinot.uz.backend.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.send.SendSticker;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Service
@Slf4j
public class CreateStadium {

    @Autowired
    BRBT_Bot BRBTBot;

    @Autowired
    koinot.uz.backend.service.WorkingALot workingALot;

    @Autowired
    UserRepository userRepository;

    @Autowired
    StadiumRepository stadiumRepository;

    @Autowired
    StadiumPhotoRepository stadiumPhotoRepository;

    @Autowired
    koinot.uz.backend.service.Restar restar;

    @Autowired
    ExceptionSend exceptionSend;


    public void registerStadiumBoss(Update update) {
        try {
            int message_id = update.getCallbackQuery().getMessage().getMessageId();
            long chat_id = update.getCallbackQuery().getMessage().getChatId();
            Integer id = update.getCallbackQuery().getFrom().getId();
            User user = userRepository.findAllByTelegramId(id);
            user.setType(UserType.USER);
            user.setState(UserState.GET_PHONE_NUMBER);
            workingALot.deleteMsg(chat_id, message_id);
            List<Stadium> allByUser = stadiumRepository.findAllByUserAndDelete(user, false);
            userRepository.save(user);

            if ((allByUser.size() == 1) && !allByUser.get(0).isActive()) {
                restar.getPhoNumberButton(user.getLanguage(), chat_id);
            } else {
                workingALot.getPhoNumberButton(user.getLanguage(), chat_id);
            }
        } catch (Exception e) {
            exceptionSend.senException("register stadium boss => ", e, null);
            log.error("register stadium boss => ", e);
        }

    }

    public void getPhoneNumber(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            if ((update.getMessage().hasContact() || (update.getMessage().hasText()) && Pattern.compile(
                            "^[+][9][9][8][0-9]{9}$")
                    .matcher(update.getMessage().hasContact() ? update.getMessage()
                            .getContact()
                            .getPhoneNumber() : update.getMessage().getText())
                    .matches())) {
                Stadium stadium = new Stadium();
                String tel;
                if (update.getMessage().hasContact()) {
                    tel = update.getMessage().getContact().getPhoneNumber();
                    stadium.setPhone_number(tel.startsWith("+") ? tel : "+" + tel);
                    user.setPhoneNumber(user.getPhoneNumber() == null ? (tel.startsWith("+") ? tel : "+" + tel) : user.getPhoneNumber());
                } else {
                    tel = update.getMessage().getText();
                    user.setPhoneNumber(user.getPhoneNumber() == null ? (tel.startsWith("+") ? tel : "+" + tel) : user.getPhoneNumber());
                    stadium.setPhone_number(tel.startsWith("+") ? tel : "+" + tel);
                }
                User user1 = user;

                if (userRepository.existsAllByPhoneNumber(tel.startsWith("+") ? tel : "+" + tel)) {
                    User u = userRepository.findAllByPhoneNumber(tel.startsWith("+") ? tel : "+" + tel);
                    if (u.getTelegramId() == null && u.getFirstName().equals("null") && u.getLastName()
                            .equals("null")) {
                        user = u;
                        user.setTelegramId(user1.getTelegramId());
                        userRepository.delete(user1);
                    }
                }

                stadium.setUser(user);
                user.setActiveStadium(stadiumRepository.save(stadium));
                user.setState(UserState.SEND_LOCATION);
                userRepository.save(user);
                List<Stadium> allByUser = stadiumRepository.findAllByUserAndDelete(user, false);
                if ((allByUser.size() == 1) && !allByUser.get(0).isActive()) {
                    restar.getLocationButton(user.getLanguage(), chat_id);
                } else {
                    workingALot.getLocationButton(language, chat_id);
                }
            } else {
                workingALot.getPhoNumberButton(language, chat_id);
            }

        } catch (Exception e) {
            exceptionSend.senException("get phone number => ", e, user);
            log.error("get phone number => ", e);
        }
    }

    public void getLocation(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            Stadium stadium = user.getActiveStadium();
            if (update.getMessage().hasLocation()) {
                stadium.setLatitude(update.getMessage().getLocation().getLatitude());
                stadium.setLongitude(update.getMessage().getLocation().getLongitude());
                stadiumRepository.save(stadium);
                user.setState(UserState.SEND_ADDRESS);

                workingALot.sendSticker(chat_id, BotAnswerString.GET_ADDRESS_STICKER);
                List<Stadium> allByUser = stadiumRepository.findAllByUserAndDelete(user, false);
                if ((allByUser.size() == 1) && !allByUser.get(0).isActive()) {
                    workingALot.cancelAll(language,
                            chat_id,
                            language.equals(BotAnswerString.uz) ? BotAnswerString.ENTER_THE_ADDRESS_OF_THIS_STADIUM_MANUALLY_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.ENTER_THE_ADDRESS_OF_THIS_STADIUM_MANUALLY_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.ENTER_THE_ADDRESS_OF_THIS_STADIUM_MANUALLY_UZL : BotAnswerString.ENTER_THE_ADDRESS_OF_THIS_STADIUM_MANUALLY_EN);
                } else {
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.WHAT_IS_NAME_THIS_STADIUM_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.WHAT_IS_NAME_THIS_STADIUM_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.WHAT_IS_NAME_THIS_STADIUM_UZL : BotAnswerString.WHAT_IS_NAME_THIS_STADIUM_EN);
                    BRBTBot.execute(sendMessage);
                }

                userRepository.save(user);

            } else {
                workingALot.getLocationButton(language, chat_id);
            }
        } catch (Exception e) {
            exceptionSend.senException("get location for creat stadium  => ", e, user);
            log.error("get location for creat stadium  => ", e);
        }

    }

    public void getAddress(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            Stadium stadium = user.getActiveStadium();
            if (update.getMessage().hasText() && update.getMessage().getText().length() > 0) {
                stadium.setAddress(update.getMessage().getText());
                try {
                    stadiumRepository.save(stadium);
                    user.setState(UserState.SEND_NAME);
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.WHAT_IS_NAME_THIS_STADIUM_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.WHAT_IS_NAME_THIS_STADIUM_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.WHAT_IS_NAME_THIS_STADIUM_UZL : BotAnswerString.WHAT_IS_NAME_THIS_STADIUM_EN);
                    workingALot.sendSticker(chat_id, BotAnswerString.GET_NAME_STICKER);
                    BRBTBot.execute(sendMessage);
                    userRepository.save(user);

                } catch (Exception e) {
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.ERROR_STADIUM_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.ERROR_STADIUM_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.ERROR_STADIUM_UZL : BotAnswerString.ERROR_STADIUM_EN);
                    BRBTBot.execute(sendMessage);
                }
            } else {

                workingALot.sendSticker(chat_id, BotAnswerString.GET_ADDRESS_STICKER);

                SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                        .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.ENTER_THE_ADDRESS_OF_THIS_STADIUM_MANUALLY_UZ : language.equals(
                                BotAnswerString.ru) ? BotAnswerString.ENTER_THE_ADDRESS_OF_THIS_STADIUM_MANUALLY_RU : language.equals(
                                BotAnswerString.uzl) ? BotAnswerString.ENTER_THE_ADDRESS_OF_THIS_STADIUM_MANUALLY_UZL : BotAnswerString.ENTER_THE_ADDRESS_OF_THIS_STADIUM_MANUALLY_EN);
                BRBTBot.execute(sendMessage);
            }
        } catch (Exception e) {
            exceptionSend.senException("get address stadium => ", e, user);
            log.error("get address stadium => ", e);
        }

    }

    public void getName(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            Stadium stadium = user.getActiveStadium();
            if (update.getMessage().hasText()) {
                stadium.setName(update.getMessage().getText().replace('_', ' '));
                try {
                    stadiumRepository.save(stadium);
                    user.setState(UserState.OPEN_TIME_STADIUM);
                    List<String> strings = new ArrayList<>();
                    strings.add(language.equals(BotAnswerString.uz) ? BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_UZ : language.equals(
                            BotAnswerString.ru) ? BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_RU : language.equals(
                            BotAnswerString.uzl) ? BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_UZL : BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_EN);

                    workingALot.sendSticker(chat_id, BotAnswerString.GET_OPEN_STICKER);

                    BRBTBot.execute(workingALot.keyboard(chat_id,
                            strings,
                            language.equals(BotAnswerString.uz) ? BotAnswerString.WHEN_THE_STADIUM_STARTS_TO_WORK_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.WHEN_THE_STADIUM_STARTS_TO_WORK_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.WHEN_THE_STADIUM_STARTS_TO_WORK_UZL : BotAnswerString.WHEN_THE_STADIUM_STARTS_TO_WORK_EN));

                    userRepository.save(user);

                } catch (Exception e) {
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.ERROR_NAME_STADIUM_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.ERROR_NAME_STADIUM_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.ERROR_NAME_STADIUM_UZL : BotAnswerString.ERROR_NAME_STADIUM_EN);
                    BRBTBot.execute(sendMessage);
                }
            } else {
                workingALot.sendSticker(chat_id, BotAnswerString.GET_NAME_STICKER);
                SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                        .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.WHAT_IS_NAME_THIS_STADIUM_UZ : language.equals(
                                BotAnswerString.ru) ? BotAnswerString.WHAT_IS_NAME_THIS_STADIUM_RU : language.equals(
                                BotAnswerString.uzl) ? BotAnswerString.WHAT_IS_NAME_THIS_STADIUM_UZL : BotAnswerString.WHAT_IS_NAME_THIS_STADIUM_EN);
                BRBTBot.execute(sendMessage);
            }
        } catch (Exception e) {
            exceptionSend.senException("stadium get name  => ", e, user);
            log.error("stadium get name  => ", e);
        }

    }

    public void getStadiumStartWork(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            Stadium stadium = user.getActiveStadium();
            if (update.getMessage().hasText()) {
                String s = update.getMessage().getText();
                if (s.equals(BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_EN) || s.equals(BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_RU) || s.equals(
                        BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_UZL) || s.equals(BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_UZ)) {

                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.SEND_ME_PHOTO_FOR_STADIUM_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.SEND_ME_PHOTO_FOR_STADIUM_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.SEND_ME_PHOTO_FOR_STADIUM_UZL : BotAnswerString.SEND_ME_PHOTO_FOR_STADIUM_EN);

                    workingALot.sendSticker(chat_id, BotAnswerString.GET_PHOTO_STICKER);
                    BRBTBot.execute(sendMessage);
                    user.setState(UserState.GET_PHOTO_STADIUM);
                    userRepository.save(user);
                } else {
                    if (workingALot.getDate(s) != null) {
                        stadium.setOpening_time(workingALot.getDate(s));
                        stadiumRepository.save(stadium);
                        user.setState(UserState.CLOSE_TIME_STADIUM);
                        SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                                .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.WHEN_THE_STADIUM_CLOSE_TO_WORK_UZ : language.equals(
                                        BotAnswerString.ru) ? BotAnswerString.WHEN_THE_STADIUM_CLOSE_TO_WORK_RU : language.equals(
                                        BotAnswerString.uzl) ? BotAnswerString.WHEN_THE_STADIUM_CLOSE_TO_WORK_UZL : BotAnswerString.WHEN_THE_STADIUM_CLOSE_TO_WORK_EN);
                        workingALot.sendSticker(chat_id, BotAnswerString.GET_CLOSE_STICKER);
                        BRBTBot.execute(sendMessage);
                        userRepository.save(user);
                    } else {
                        SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                                .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.DATE_FORMAT_UZ : language.equals(
                                        BotAnswerString.ru) ? BotAnswerString.DATE_FORMAT_RU : language.equals(
                                        BotAnswerString.uzl) ? BotAnswerString.DATE_FORMAT_UZL : BotAnswerString.DATE_FORMAT_EN);
                        BRBTBot.execute(sendMessage);
                    }
                }

            } else {
                List<String> strings = new ArrayList<>();
                strings.add(language.equals(BotAnswerString.uz) ? BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_UZ : language.equals(
                        BotAnswerString.ru) ? BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_RU : language.equals(
                        BotAnswerString.uzl) ? BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_UZL : BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_EN);

                workingALot.sendSticker(chat_id, BotAnswerString.GET_OPEN_STICKER);

                BRBTBot.execute(workingALot.keyboard(chat_id,
                        strings,
                        language.equals(BotAnswerString.uz) ? BotAnswerString.WHEN_THE_STADIUM_STARTS_TO_WORK_UZ : language.equals(
                                BotAnswerString.ru) ? BotAnswerString.WHEN_THE_STADIUM_STARTS_TO_WORK_RU : language.equals(
                                BotAnswerString.uzl) ? BotAnswerString.WHEN_THE_STADIUM_STARTS_TO_WORK_UZL : BotAnswerString.WHEN_THE_STADIUM_STARTS_TO_WORK_EN));
            }
        } catch (Exception e) {
            exceptionSend.senException("stadium start work  => ", e, user);
            log.error("stadium start work  => ", e);
        }

    }

    public void getStadiumFinishWork(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            Stadium stadium = user.getActiveStadium();
            if (update.getMessage().hasText()) {
                String s = update.getMessage().getText();
                if (s.equals(BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_EN) || s.equals(BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_RU) || s.equals(
                        BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_UZL) || s.equals(BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_UZ)) {

                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.SEND_ME_PHOTO_FOR_STADIUM_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.SEND_ME_PHOTO_FOR_STADIUM_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.SEND_ME_PHOTO_FOR_STADIUM_UZL : BotAnswerString.SEND_ME_PHOTO_FOR_STADIUM_EN);

                    BRBTBot.execute(sendMessage);
                    workingALot.sendSticker(chat_id, BotAnswerString.GET_PHOTO_STICKER);
                    user.setState(UserState.GET_PHOTO_STADIUM);
                    userRepository.save(user);

                } else {
                    if (workingALot.getDate(s) != null) {
                        stadium.setClosing_time(workingALot.getDate(s));
                        stadiumRepository.save(stadium);
                        user.setState(UserState.GET_PHOTO_STADIUM);
                        SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                                .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.SEND_ME_PHOTO_FOR_STADIUM_UZ : language.equals(
                                        BotAnswerString.ru) ? BotAnswerString.SEND_ME_PHOTO_FOR_STADIUM_RU : language.equals(
                                        BotAnswerString.uzl) ? BotAnswerString.SEND_ME_PHOTO_FOR_STADIUM_UZL : BotAnswerString.SEND_ME_PHOTO_FOR_STADIUM_EN);
                        BRBTBot.execute(sendMessage);

                        workingALot.sendSticker(chat_id, BotAnswerString.GET_PHOTO_STICKER);

                        userRepository.save(user);
                    } else {
                        SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                                .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.DATE_FORMAT_UZ : language.equals(
                                        BotAnswerString.ru) ? BotAnswerString.DATE_FORMAT_RU : language.equals(
                                        BotAnswerString.uzl) ? BotAnswerString.DATE_FORMAT_UZL : BotAnswerString.DATE_FORMAT_EN);
                        BRBTBot.execute(sendMessage);
                    }
                }
            }
        } catch (Exception e) {
            exceptionSend.senException(" get stadium finished work time  => ", e, user);
            log.error(" get stadium finished work time  => ", e);
        }

    }

    public void getPhoto(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            Stadium stadium = user.getActiveStadium();
            int l = stadiumPhotoRepository.countStadiumPhotoByStadiumAndWhy(stadium,
                    AttachmentTypeEnumWhy.STADIUM_PHOTO);

            if (update.getMessage().hasPhoto() && 10 > l) {
                workingALot.savePhoto(update, stadium, user);
                l = l + 1;
                List<String> strings = new ArrayList<>();
                strings.add(language.equals(BotAnswerString.uz) ? BotAnswerString.NO_ANOTHER_PICTURE_UZ : language.equals(
                        BotAnswerString.ru) ? BotAnswerString.NO_ANOTHER_PICTURE_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.NO_ANOTHER_PICTURE_UZL : BotAnswerString.NO_ANOTHER_PICTURE_EN);
                BRBTBot.execute(workingALot.keyboard(chat_id,
                        strings,
                        language.equals(BotAnswerString.uz) ? "(" + l + ")  " + BotAnswerString.IS_THERE_ANOTHER_PICTURE_UZ : language.equals(
                                BotAnswerString.ru) ? "(" + l + ")  " + BotAnswerString.IS_THERE_ANOTHER_PICTURE_RU : language.equals(
                                BotAnswerString.uzl) ? "(" + l + ")  " + BotAnswerString.IS_THERE_ANOTHER_PICTURE_UZL : "(" + l + ")  " + BotAnswerString.IS_THERE_ANOTHER_PICTURE_EN));
                if (l == 10) {
                    user.setState(UserState.GET_PRICE_DAYLIGHT_STADIUM);
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.DAYLIGHT_PRICE_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.DAYLIGHT_PRICE_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.DAYLIGHT_PRICE_UZL : BotAnswerString.DAYLIGHT_PRICE_EN);

                    workingALot.sendSticker(chat_id, BotAnswerString.GET_DAYLIGHT_PRICE_STICKER);

                    BRBTBot.execute(sendMessage);
                    userRepository.save(user);
                }
                //
            } else {
                if ((update.getMessage().hasText() && (update.getMessage()
                        .getText()
                        .equals(BotAnswerString.NO_ANOTHER_PICTURE_UZ) || update.getMessage()
                        .getText()
                        .equals(BotAnswerString.NO_ANOTHER_PICTURE_RU) || update.getMessage()
                        .getText()
                        .equals(BotAnswerString.NO_ANOTHER_PICTURE_UZL) || update.getMessage()
                        .getText()
                        .equals(BotAnswerString.NO_ANOTHER_PICTURE_EN)) || l == 10)) {
                    user.setState(UserState.GET_PRICE_DAYLIGHT_STADIUM);
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.DAYLIGHT_PRICE_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.DAYLIGHT_PRICE_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.DAYLIGHT_PRICE_UZL : BotAnswerString.DAYLIGHT_PRICE_EN);

                    workingALot.sendSticker(chat_id, BotAnswerString.GET_DAYLIGHT_PRICE_STICKER);

                    BRBTBot.execute(sendMessage);
                    userRepository.save(user);

                } else {
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.SEND_ME_PHOTO_FOR_STADIUM_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.SEND_ME_PHOTO_FOR_STADIUM_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.SEND_ME_PHOTO_FOR_STADIUM_UZL : BotAnswerString.SEND_ME_PHOTO_FOR_STADIUM_EN);
                    workingALot.sendSticker(chat_id, BotAnswerString.GET_PHOTO_STICKER);

                    BRBTBot.execute(sendMessage);
                }
            }
        } catch (Exception e) {
            exceptionSend.senException("get stadium photo => ", e, user);
            log.error("get stadium photo => ", e);
        }

    }

    public void getPriceDaylight(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            Stadium stadium = user.getActiveStadium();
            if (update.getMessage().hasText()) {
                String price = update.getMessage().getText();
                if (workingALot.getPrice(price) != null) {
                    stadium.setPrice_day_time(workingALot.getPrice(price));
                    stadiumRepository.save(stadium);
                    user.setState(UserState.GET_PRICE_NIGHT_STADIUM);
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.NIGHT_PRICE_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.NIGHT_PRICE_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.NIGHT_PRICE_UZL : BotAnswerString.NIGHT_PRICE_EN);

                    workingALot.sendSticker(chat_id, BotAnswerString.GET_NIGHT_PRICE_STICKER);

                    BRBTBot.execute(sendMessage);

                    userRepository.save(user);

                } else {
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.FORMAT_PRICE_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.FORMAT_PRICE_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.FORMAT_PRICE_UZL : BotAnswerString.FORMAT_PRICE_EN);
                    BRBTBot.execute(sendMessage);
                }

            } else {
                workingALot.sendSticker(chat_id, BotAnswerString.GET_DAYLIGHT_PRICE_STICKER);

                SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                        .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.DAYLIGHT_PRICE_UZ : language.equals(
                                BotAnswerString.ru) ? BotAnswerString.DAYLIGHT_PRICE_RU : language.equals(
                                BotAnswerString.uzl) ? BotAnswerString.DAYLIGHT_PRICE_UZL : BotAnswerString.DAYLIGHT_PRICE_EN);
                BRBTBot.execute(sendMessage);
            }
        } catch (Exception e) {
            exceptionSend.senException("get price daylight for stadium => ", e, user);
            log.error("get price daylight for stadium => ", e);
        }

    }

    public void getPriceNightlight(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            Stadium stadium = user.getActiveStadium();
            if (update.getMessage().hasText()) {
                String price = update.getMessage().getText();
                if (workingALot.getPrice(price) != null) {
                    stadium.setPrice_night_time(workingALot.getPrice(price));
                    stadiumRepository.save(stadium);
                    user.setState(UserState.GET_NIGHT_TIME_FOR_CHANGE_PRICE);
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setReplyToMessageId(update.getMessage().getMessageId())
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.GET_NIGHT_TIME_FOR_CHANGE_PRICE_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.GET_NIGHT_TIME_FOR_CHANGE_PRICE_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.GET_NIGHT_TIME_FOR_CHANGE_PRICE_UZL : BotAnswerString.GET_NIGHT_TIME_FOR_CHANGE_PRICE_EN);

                    workingALot.sendSticker(chat_id, BotAnswerString.GET_TIME_FOR_CHANGE_PRICE_STICKER);

                    BRBTBot.execute(sendMessage);
                    userRepository.save(user);

                } else {
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.FORMAT_PRICE_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.FORMAT_PRICE_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.FORMAT_PRICE_UZL : BotAnswerString.FORMAT_PRICE_EN);
                    BRBTBot.execute(sendMessage);
                }

            } else {
                workingALot.sendSticker(chat_id, BotAnswerString.GET_NIGHT_PRICE_STICKER);

                SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                        .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.NIGHT_PRICE_UZ : language.equals(
                                BotAnswerString.ru) ? BotAnswerString.NIGHT_PRICE_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.NIGHT_PRICE_UZL : BotAnswerString.NIGHT_PRICE_EN);
                BRBTBot.execute(sendMessage);
            }
        } catch (Exception e) {
            exceptionSend.senException("get price nightlight for stadium  => ", e, user);
            log.error("get price nightlight for stadium  => ", e);
        }
    }

    public void getTimeForChangePrice(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            Stadium stadium = user.getActiveStadium();
            if (update.getMessage().hasText()) {
                String s = update.getMessage().getText();
                if (workingALot.getDate(s) != null) {
                    stadium.setChange_price_time(workingALot.getDate(s));
                    stadiumRepository.save(stadium);
                    user.setState(UserState.GET_STADIUM_SIZE);
                    BRBTBot.execute(new SendPhoto().setChatId(chat_id)
                            .setPhoto(BotAnswerString.GET_SIZE_FOR_STADIUM_PHOTO)
                            .setCaption("Photo"));
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.GET_STADIUM_SIZE_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.GET_STADIUM_SIZE_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.GET_STADIUM_SIZE_UZL : BotAnswerString.GET_STADIUM_SIZE_EN);
                    BRBTBot.execute(sendMessage);
                    userRepository.save(user);

                } else {
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.DATE_FORMAT_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.DATE_FORMAT_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.DATE_FORMAT_UZL : BotAnswerString.DATE_FORMAT_EN);
                    BRBTBot.execute(sendMessage);
                }

            } else {
                workingALot.sendSticker(chat_id, BotAnswerString.GET_TIME_FOR_CHANGE_PRICE_STICKER);
                SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                        .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.GET_NIGHT_TIME_FOR_CHANGE_PRICE_UZ : language.equals(
                                BotAnswerString.ru) ? BotAnswerString.GET_NIGHT_TIME_FOR_CHANGE_PRICE_RU : language.equals(
                                BotAnswerString.uzl) ? BotAnswerString.GET_NIGHT_TIME_FOR_CHANGE_PRICE_UZL : BotAnswerString.GET_NIGHT_TIME_FOR_CHANGE_PRICE_EN);
                BRBTBot.execute(sendMessage);
            }
        } catch (Exception e) {
            exceptionSend.senException("get stadium time for change price => ", e, user);
            log.error("get stadium time for change price => ", e);
        }

    }

    public void getStadiumSize(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            Stadium stadium = user.getActiveStadium();
            if (update.getMessage().hasText()) {
                String s = update.getMessage().getText().replace(" ", "");
                if (workingALot.getHeightStadium(s) != null && workingALot.getWidthStadium(s) != null) {
                    stadium.setHeight(workingALot.getHeightStadium(s));
                    stadium.setWidth(workingALot.getWidthStadium(s));
                    stadiumRepository.save(stadium);
                    user.setState(UserState.VERIFY_STADIUM);
                    SendSticker sendSticker = new SendSticker().setChatId(chat_id)
                            .setSticker(BotAnswerString.GET_SUCCESSFUL_STICKER);
                    BRBTBot.execute(sendSticker);
                    workingALot.getStadiumInfo(language, chat_id, stadium);
                    userRepository.save(user);

                } else {
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.DATE_SIZE_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.DATE_SIZE_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.DATE_SIZE_UZL : BotAnswerString.DATE_SIZE_EN);
                    BRBTBot.execute(sendMessage);
                }

            } else {
                SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                        .setReplyToMessageId(update.getMessage().getMessageId())
                        .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.GET_STADIUM_SIZE_UZ : language.equals(
                                BotAnswerString.ru) ? BotAnswerString.GET_STADIUM_SIZE_RU : language.equals(
                                BotAnswerString.uzl) ? BotAnswerString.GET_STADIUM_SIZE_UZL : BotAnswerString.GET_STADIUM_SIZE_EN);
                BRBTBot.execute(sendMessage);
            }
        } catch (Exception e) {
            exceptionSend.senException("get stadium size  => ", e, user);
            log.error("restart client => ", e);
        }

    }

    ////*********************************************************//////

    public void deleteStadium(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            long message_id = update.getCallbackQuery().getMessage().getMessageId();
            List<String> list = new ArrayList<>();

            try {
                Stadium stadium = user.getActiveStadium();
                user.setActiveStadium(null);
                userRepository.save(user);
                List<StadiumPhoto> byStadium = stadiumPhotoRepository.findByStadiumIdAndWhyOrderByCreatedAtDesc(stadium.getId(),
                        AttachmentTypeEnumWhy.STADIUM_PHOTO);
                for (StadiumPhoto stadiumPhoto : byStadium) {
                    stadiumPhoto.setWhy(AttachmentTypeEnumWhy.DELETE_STADIUM_PHOTO);
                    stadiumPhotoRepository.save(stadiumPhoto);
                }
                stadiumRepository.delete(stadium);

                List<Stadium> allStadium1 = stadiumRepository.findAllByUserAndDelete(user, false);

                if (allStadium1.isEmpty()) {
                    user.setType(UserType.USER);
                }
                user.setState(UserState.DEFAULT);
                userRepository.save(user);
                for (Stadium stadium1 : allStadium1) {
                    list.add(stadium1.getName());
                }
                list.add(BotAnswerString.CREATE);
                String s = language.equals(BotAnswerString.uz) ? BotAnswerString.DELETE_SUCCESSFUL_UZ : language.equals(
                        BotAnswerString.ru) ? BotAnswerString.DELETE_SUCCESSFUL_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.DELETE_SUCCESSFUL_UZL : BotAnswerString.DELETE_SUCCESSFUL_EN;
                BRBTBot.execute(workingALot.editInline(chat_id, (int) message_id, list, s));
            } catch (Exception e) {
                List<Stadium> allStadium = stadiumRepository.findAllByUserAndDelete(user, false);
                for (Stadium stadium1 : allStadium) {
                    list.add(stadium1.getName());
                }
                list.add(BotAnswerString.CREATE);
                String s = language.equals(BotAnswerString.uz) ? BotAnswerString.DELETE_FAILED_UZ : language.equals(
                        BotAnswerString.ru) ? BotAnswerString.DELETE_FAILED_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.DELETE_FAILED_UZL : BotAnswerString.DELETE_FAILED_EN;
                BRBTBot.execute(workingALot.editInline(chat_id, (int) message_id, list, s));
            }
        } catch (Exception e) {
            exceptionSend.senException("delete stadium => ", e, user);
            log.error("delete stadium => ", e);
        }

    }

    public void editStadium(Update update, User user, String language) {

        try {
            user.setState(UserState.EDIT);
            userRepository.save(user);

            long chat_id = user.getTelegramId();

            String answer = language.equals(BotAnswerString.uz) ? BotAnswerString.WHAT_DO_YOU_WANT_TO_CHANGE_THE_STADIUM_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.WHAT_DO_YOU_WANT_TO_CHANGE_THE_STADIUM_RU : language.equals(
                    BotAnswerString.uzl) ? BotAnswerString.WHAT_DO_YOU_WANT_TO_CHANGE_THE_STADIUM_UZL : BotAnswerString.WHAT_DO_YOU_WANT_TO_CHANGE_THE_STADIUM_EN;

            List<String> stringList = new ArrayList<>();

            stringList.add(language.equals(BotAnswerString.uz) ? BotAnswerString.PHONE_NUMBER_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.PHONE_NUMBER_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.PHONE_NUMBER_UZL : BotAnswerString.PHONE_NUMBER_EN);

            stringList.add(language.equals(BotAnswerString.uz) ? BotAnswerString.SIZE_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.SIZE_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.SIZE_UZL : BotAnswerString.SIZE_EN);

            stringList.add(language.equals(BotAnswerString.uz) ? BotAnswerString.WORKING_HOURS_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.WORKING_HOURS_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.WORKING_HOURS_UZL : BotAnswerString.WORKING_HOURS_EN);

            stringList.add(language.equals(BotAnswerString.uz) ? BotAnswerString.PRICE_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.PRICE_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.PRICE_UZL : BotAnswerString.PRICE_EN);

            stringList.add(language.equals(BotAnswerString.uz) ? BotAnswerString.LOCATION_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.LOCATION_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.LOCATION_UZL : BotAnswerString.LOCATION_EN);

            stringList.add(language.equals(BotAnswerString.uz) ? BotAnswerString.ADDRESS_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.ADDRESS_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.ADDRESS_UZL : BotAnswerString.ADDRESS_EN);

            stringList.add(language.equals(BotAnswerString.uz) ? BotAnswerString.NAME_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.NAME_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.NAME_UZL : BotAnswerString.NAME_EN);

            stringList.add(language.equals(BotAnswerString.uz) ? BotAnswerString.TIME_OF_EVENING_PRICE_CHANGE_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.TIME_OF_EVENING_PRICE_CHANGE_RU : language.equals(
                    BotAnswerString.uzl) ? BotAnswerString.TIME_OF_EVENING_PRICE_CHANGE_UZL : BotAnswerString.TIME_OF_EVENING_PRICE_CHANGE_EN);

            stringList.add(language.equals(BotAnswerString.uz) ? BotAnswerString.STADIUM_PHOTO_CHANGE_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.STADIUM_PHOTO_CHANGE_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.STADIUM_PHOTO_CHANGE_UZL : BotAnswerString.STADIUM_PHOTO_CHANGE_EN);
            stringList.add(BotAnswerString.BACK);
            long message_id = update.getCallbackQuery().getMessage().getMessageId();
            BRBTBot.execute(workingALot.editInline(chat_id, (int) message_id, stringList, answer));
        } catch (Exception e) {
            exceptionSend.senException("edit stadium menu for admin => ", e, user);
            log.error("edit stadium menu for admin => ", e);
        }

    }

    public void verifyStadium(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            long message_id = update.getCallbackQuery().getMessage().getMessageId();
            Stadium stadium = user.getActiveStadium();
            stadium.setVerify(true);
            user.setState(UserState.DEFAULT);
            user.setType(UserType.ADMIN);
            userRepository.save(user);
            stadiumRepository.save(stadium);
            String info = "";
            List<String> list = new ArrayList<>();

            List<Stadium> allStadium = stadiumRepository.findAllByUserAndDelete(user, false);
            for (Stadium stadium1 : allStadium) {
                list.add(stadium1.getName());
                info = info.concat(workingALot.getInfoStadium(stadium1, language));
            }
            list.add(BotAnswerString.CREATE);
            BRBTBot.execute(workingALot.editInline(chat_id, (int) message_id, list, info));
            workingALot.getStadiumDashButton(language, chat_id);
            for (User user1 : userRepository.findAllByType(UserType.SUPPER_ADMIN)) {
                workingALot.adminMessage(user1.getLanguage(),
                        Long.valueOf(user1.getTelegramId()),
                        user.getActiveStadium());
            }
        } catch (Exception e) {
            exceptionSend.senException("verify stadium => ", e, user);
            log.error("verify stadium => ", e);
        }

    }

    public void changeWorkingHours(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            user.setState(UserState.EDIT_OPEN_TIME_STADIUM);
            userRepository.save(user);
            List<String> strings = new ArrayList<>();

            EditMessageText editMessageText = new EditMessageText().setChatId(chat_id)
                    .setText(BotAnswerString.CHANGED)
                    .setMessageId(update.getCallbackQuery().getMessage().getMessageId());
            BRBTBot.execute(editMessageText);

            strings.add(language.equals(BotAnswerString.uz) ? BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_RU : language.equals(
                    BotAnswerString.uzl) ? BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_UZL : BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_EN);
            BRBTBot.execute(workingALot.keyboard(chat_id,
                    strings,
                    language.equals(BotAnswerString.uz) ? BotAnswerString.WHEN_THE_STADIUM_STARTS_TO_WORK_UZ : language.equals(
                            BotAnswerString.ru) ? BotAnswerString.WHEN_THE_STADIUM_STARTS_TO_WORK_RU : language.equals(
                            BotAnswerString.uzl) ? BotAnswerString.WHEN_THE_STADIUM_STARTS_TO_WORK_UZL : BotAnswerString.WHEN_THE_STADIUM_STARTS_TO_WORK_EN));
        } catch (Exception e) {
            exceptionSend.senException("change working hours => ", e, user);
            log.error("change working hours => ", e);
        }

    }

    public void getEditStadiumStartWork(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            Stadium stadium = user.getActiveStadium();
            if (update.getMessage().hasText()) {
                String s = update.getMessage().getText();
                if (s.equals(BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_EN) || s.equals(BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_RU) || s.equals(
                        BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_UZL) || s.equals(BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_UZ)) {
                    stadium.setOpening_time(null);
                    stadium.setClosing_time(null);
                    stadiumRepository.save(stadium);
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.CHANGED_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.CHANGED_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.CHANGED_UZL : BotAnswerString.CHANGED_EN);
                    BRBTBot.execute(sendMessage);
                    user.setState(UserState.DEFAULT);
                    userRepository.save(user);
                } else {
                    if (workingALot.getDate(s) != null) {
                        stadium.setOpening_time(workingALot.getDate(s));
                        stadiumRepository.save(stadium);
                        user.setState(UserState.EDIT_CLOSE_TIME_STADIUM);

                        SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                                .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.WHEN_THE_STADIUM_CLOSE_TO_WORK_UZ : language.equals(
                                        BotAnswerString.ru) ? BotAnswerString.WHEN_THE_STADIUM_CLOSE_TO_WORK_RU : language.equals(
                                        BotAnswerString.uzl) ? BotAnswerString.WHEN_THE_STADIUM_CLOSE_TO_WORK_UZL : BotAnswerString.WHEN_THE_STADIUM_CLOSE_TO_WORK_EN);
                        BRBTBot.execute(sendMessage);
                        userRepository.save(user);

                    } else {
                        SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                                .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.DATE_FORMAT_UZ : language.equals(
                                        BotAnswerString.ru) ? BotAnswerString.DATE_FORMAT_RU : language.equals(
                                        BotAnswerString.uzl) ? BotAnswerString.DATE_FORMAT_UZL : BotAnswerString.DATE_FORMAT_EN);
                        BRBTBot.execute(sendMessage);
                    }
                }
            }
        } catch (Exception e) {
            exceptionSend.senException("edit start working hours => ", e, user);
            log.error("edit start working hours => ", e);
        }

    }

    public void getEditStadiumFinishWork(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            Stadium stadium = user.getActiveStadium();
            if (update.getMessage().hasText()) {
                String s = update.getMessage().getText();
                if (s.equals(BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_EN) || s.equals(BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_RU) || s.equals(
                        BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_UZL) || s.equals(BotAnswerString.THIS_STADIUM_ALWAYS_OPEN_UZ)) {
                    stadium.setOpening_time(null);
                    stadium.setClosing_time(null);
                    stadiumRepository.save(stadium);
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.CHANGED_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.CHANGED_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.CHANGED_UZL : BotAnswerString.CHANGED_EN);
                    user.setState(UserState.VERIFY_STADIUM);
                    BRBTBot.execute(sendMessage);
                    workingALot.getStadiumInfo(language, chat_id, stadium);
                    userRepository.save(user);

                } else {
                    if (workingALot.getDate(s) != null) {
                        stadium.setClosing_time(workingALot.getDate(s));
                        stadiumRepository.save(stadium);
                        SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                                .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.CHANGED_UZ : language.equals(
                                        BotAnswerString.ru) ? BotAnswerString.CHANGED_RU : language.equals(
                                        BotAnswerString.uzl) ? BotAnswerString.CHANGED_UZL : BotAnswerString.CHANGED_EN);
                        BRBTBot.execute(sendMessage);
                        user.setState(UserState.VERIFY_STADIUM);
                        workingALot.getStadiumInfo(language, chat_id, stadium);
                        userRepository.save(user);

                    } else {
                        SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                                .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.DATE_FORMAT_UZ : language.equals(
                                        BotAnswerString.ru) ? BotAnswerString.DATE_FORMAT_RU : language.equals(
                                        BotAnswerString.uzl) ? BotAnswerString.DATE_FORMAT_UZL : BotAnswerString.DATE_FORMAT_EN);
                        BRBTBot.execute(sendMessage);
                    }
                }
            }
        } catch (Exception e) {
            exceptionSend.senException("edit finished time => ", e, user);
            log.error("edit finished time => ", e);
        }

    }

    public void changePrice(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            user.setState(UserState.EDIT_GET_PRICE_DAYLIGHT_STADIUM);
            userRepository.save(user);
            EditMessageText editMessageText = new EditMessageText().setChatId(chat_id)
                    .setText(BotAnswerString.CHANGED)
                    .setMessageId(update.getCallbackQuery().getMessage().getMessageId());
            BRBTBot.execute(editMessageText);
            SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                    .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.DAYLIGHT_PRICE_UZ : language.equals(
                            BotAnswerString.ru) ? BotAnswerString.DAYLIGHT_PRICE_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.DAYLIGHT_PRICE_UZL : BotAnswerString.DAYLIGHT_PRICE_EN);
            BRBTBot.execute(sendMessage);
        } catch (Exception e) {
            exceptionSend.senException("change price => ", e, user);
            log.error("change price => ", e);
        }

    }

    public void getEditPriceDaylight(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            Stadium stadium = user.getActiveStadium();
            if (update.getMessage().hasText()) {
                String price = update.getMessage().getText();
                if (workingALot.getPrice(price) != null) {
                    stadium.setPrice_day_time(workingALot.getPrice(price));
                    stadiumRepository.save(stadium);
                    user.setState(UserState.EDIT_GET_PRICE_NIGHT_STADIUM);
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.NIGHT_PRICE_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.NIGHT_PRICE_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.NIGHT_PRICE_UZL : BotAnswerString.NIGHT_PRICE_EN);
                    BRBTBot.execute(sendMessage);
                    userRepository.save(user);

                } else {
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.FORMAT_PRICE_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.FORMAT_PRICE_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.FORMAT_PRICE_UZL : BotAnswerString.FORMAT_PRICE_EN);
                    BRBTBot.execute(sendMessage);
                }

            } else {
                SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                        .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.DAYLIGHT_PRICE_UZ : language.equals(
                                BotAnswerString.ru) ? BotAnswerString.DAYLIGHT_PRICE_RU : language.equals(
                                BotAnswerString.uzl) ? BotAnswerString.DAYLIGHT_PRICE_UZL : BotAnswerString.DAYLIGHT_PRICE_EN);
                BRBTBot.execute(sendMessage);
            }
        } catch (Exception e) {
            exceptionSend.senException("edit price daylight => ", e, user);
            log.error("edit price daylight => ", e);
        }

    }

    public void getEditPriceNightlight(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            Stadium stadium = user.getActiveStadium();
            if (update.getMessage().hasText()) {
                String price = update.getMessage().getText();
                if (workingALot.getPrice(price) != null) {
                    stadium.setPrice_night_time(workingALot.getPrice(price));
                    stadiumRepository.save(stadium);
                    user.setState(UserState.VERIFY_STADIUM);
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.CHANGED_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.CHANGED_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.CHANGED_UZL : BotAnswerString.CHANGED_EN);
                    BRBTBot.execute(sendMessage);
                    workingALot.getStadiumInfo(language, chat_id, stadium);
                    userRepository.save(user);

                } else {
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.FORMAT_PRICE_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.FORMAT_PRICE_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.FORMAT_PRICE_UZL : BotAnswerString.FORMAT_PRICE_EN);
                    BRBTBot.execute(sendMessage);
                }

            } else {
                SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                        .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.NIGHT_PRICE_UZ : language.equals(
                                BotAnswerString.ru) ? BotAnswerString.NIGHT_PRICE_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.NIGHT_PRICE_UZL : BotAnswerString.NIGHT_PRICE_EN);
                BRBTBot.execute(sendMessage);
            }
        } catch (Exception e) {
            exceptionSend.senException("change price nightlight => ", e, user);
            log.error("change price nightlight => ", e);
        }

    }

    public void changeLocation(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            user.setState(UserState.EDIT_SEND_LOCATION);
            userRepository.save(user);

            EditMessageText editMessageText = new EditMessageText().setChatId(chat_id)
                    .setText(BotAnswerString.CHANGED)
                    .setMessageId(update.getCallbackQuery().getMessage().getMessageId());
            BRBTBot.execute(editMessageText);
            workingALot.getLocationButton(language, chat_id);
        } catch (Exception e) {
            exceptionSend.senException("change location stadium => ", e, user);
            log.error("change location stadium => ", e);
        }

    }

    public void getEditLocation(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            Stadium stadium = user.getActiveStadium();
            if (update.getMessage().hasLocation()) {
                stadium.setLatitude(update.getMessage().getLocation().getLatitude());
                stadium.setLongitude(update.getMessage().getLocation().getLongitude());
                stadiumRepository.save(stadium);
                SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                        .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.CHANGED_UZ : language.equals(
                                BotAnswerString.ru) ? BotAnswerString.CHANGED_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.CHANGED_UZL : BotAnswerString.CHANGED_EN);
                BRBTBot.execute(sendMessage);
                workingALot.getStadiumInfo(language, chat_id, stadium);
                user.setState(UserState.VERIFY_STADIUM);
                userRepository.save(user);

            } else {
                SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                        .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.ERROR_STADIUM_UZ : language.equals(
                                BotAnswerString.ru) ? BotAnswerString.ERROR_STADIUM_RU : language.equals(
                                BotAnswerString.uzl) ? BotAnswerString.ERROR_STADIUM_UZL : BotAnswerString.ERROR_STADIUM_EN);
                BRBTBot.execute(sendMessage);
            }
        } catch (Exception e) {
            exceptionSend.senException(" get edit location stadium => ", e, user);
            log.error(" get edit location stadium => ", e);
        }

    }

    public void getEditAddress(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            Stadium stadium = user.getActiveStadium();
            if (update.getMessage().hasText()) {
                stadium.setAddress(update.getMessage().getText());
                try {
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.CHANGED_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.CHANGED_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.CHANGED_UZL : BotAnswerString.CHANGED_EN);
                    BRBTBot.execute(sendMessage);
                    workingALot.getStadiumInfo(language, chat_id, stadium);
                    user.setState(UserState.VERIFY_STADIUM);
                    userRepository.save(user);

                } catch (Exception e) {
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.ERROR_STADIUM_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.ERROR_STADIUM_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.ERROR_STADIUM_UZL : BotAnswerString.ERROR_STADIUM_EN);
                    BRBTBot.execute(sendMessage);
                }
            } else {
                SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                        .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.ENTER_THE_ADDRESS_OF_THIS_STADIUM_MANUALLY_UZ : language.equals(
                                BotAnswerString.ru) ? BotAnswerString.ENTER_THE_ADDRESS_OF_THIS_STADIUM_MANUALLY_RU : language.equals(
                                BotAnswerString.uzl) ? BotAnswerString.ENTER_THE_ADDRESS_OF_THIS_STADIUM_MANUALLY_UZL : BotAnswerString.ENTER_THE_ADDRESS_OF_THIS_STADIUM_MANUALLY_EN);
                BRBTBot.execute(sendMessage);
            }
        } catch (Exception e) {
            exceptionSend.senException("change address stadium => ", e, user);
            log.error("change address stadium => ", e);
        }

    }

    public void changeName(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            user.setState(UserState.EDIT_SEND_NAME);
            userRepository.save(user);

            EditMessageText editMessageText = new EditMessageText().setChatId(chat_id)
                    .setText(BotAnswerString.CHANGED)
                    .setMessageId(update.getCallbackQuery().getMessage().getMessageId());
            BRBTBot.execute(editMessageText);

            SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                    .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.WHAT_IS_NAME_THIS_STADIUM_UZ : language.equals(
                            BotAnswerString.ru) ? BotAnswerString.WHAT_IS_NAME_THIS_STADIUM_RU : language.equals(
                            BotAnswerString.uzl) ? BotAnswerString.WHAT_IS_NAME_THIS_STADIUM_UZL : BotAnswerString.WHAT_IS_NAME_THIS_STADIUM_EN);
            BRBTBot.execute(sendMessage);
        } catch (Exception e) {
            exceptionSend.senException("change name for stadium => ", e, user);
            log.error("change name for stadium => ", e);
        }

    }

    public void getEditName(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            Stadium stadium = user.getActiveStadium();
            if (update.getMessage().hasText()) {
                stadium.setName(update.getMessage().getText());
                try {
                    stadiumRepository.save(stadium);
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.CHANGED_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.CHANGED_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.CHANGED_UZL : BotAnswerString.CHANGED_EN);
                    BRBTBot.execute(sendMessage);
                    workingALot.getStadiumInfo(language, chat_id, stadium);
                    user.setState(UserState.VERIFY_STADIUM);
                    userRepository.save(user);

                } catch (Exception e) {
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.ERROR_NAME_STADIUM_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.ERROR_NAME_STADIUM_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.ERROR_NAME_STADIUM_UZL : BotAnswerString.ERROR_NAME_STADIUM_EN);
                    BRBTBot.execute(sendMessage);
                }
            } else {
                SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                        .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.WHAT_IS_NAME_THIS_STADIUM_UZ : language.equals(
                                BotAnswerString.ru) ? BotAnswerString.WHAT_IS_NAME_THIS_STADIUM_RU : language.equals(
                                BotAnswerString.uzl) ? BotAnswerString.WHAT_IS_NAME_THIS_STADIUM_UZL : BotAnswerString.WHAT_IS_NAME_THIS_STADIUM_EN);
                BRBTBot.execute(sendMessage);
            }
        } catch (Exception e) {
            exceptionSend.senException("change get name for stadium => ", e, user);
            log.error("change get name for stadium => ", e);
        }

    }

    public void changeTimeOfEveningPrice(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            user.setState(UserState.EDIT_GET_NIGHT_TIME_FOR_CHANGE_PRICE);

            EditMessageText editMessageText = new EditMessageText().setChatId(chat_id)
                    .setText(BotAnswerString.CHANGED)
                    .setMessageId(update.getCallbackQuery().getMessage().getMessageId());
            BRBTBot.execute(editMessageText);

            SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                    .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.GET_NIGHT_TIME_FOR_CHANGE_PRICE_UZ : language.equals(
                            BotAnswerString.ru) ? BotAnswerString.GET_NIGHT_TIME_FOR_CHANGE_PRICE_RU : language.equals(
                            BotAnswerString.uzl) ? BotAnswerString.GET_NIGHT_TIME_FOR_CHANGE_PRICE_UZL : BotAnswerString.GET_NIGHT_TIME_FOR_CHANGE_PRICE_EN);
            BRBTBot.execute(sendMessage);
            userRepository.save(user);
        } catch (Exception e) {
            exceptionSend.senException("change time of evening price => ", e, user);
            log.error("change time of evening price => ", e);
        }

    }

    public void getEditTimeForChangePrice(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            Stadium stadium = user.getActiveStadium();
            if (update.getMessage().hasText()) {
                String s = update.getMessage().getText();
                if (workingALot.getDate(s) != null) {
                    stadium.setChange_price_time(workingALot.getDate(s));
                    stadiumRepository.save(stadium);

                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.CHANGED_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.CHANGED_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.CHANGED_UZL : BotAnswerString.CHANGED_EN);
                    BRBTBot.execute(sendMessage);
                    workingALot.getStadiumInfo(language, chat_id, stadium);
                    user.setState(UserState.VERIFY_STADIUM);
                    userRepository.save(user);
                } else {
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.DATE_FORMAT_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.DATE_FORMAT_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.DATE_FORMAT_UZL : BotAnswerString.DATE_FORMAT_EN);
                    BRBTBot.execute(sendMessage);
                }

            } else {
                SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                        .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.GET_NIGHT_TIME_FOR_CHANGE_PRICE_UZ : language.equals(
                                BotAnswerString.ru) ? BotAnswerString.GET_NIGHT_TIME_FOR_CHANGE_PRICE_RU : language.equals(
                                BotAnswerString.uzl) ? BotAnswerString.GET_NIGHT_TIME_FOR_CHANGE_PRICE_UZL : BotAnswerString.GET_NIGHT_TIME_FOR_CHANGE_PRICE_EN);
                BRBTBot.execute(sendMessage);
            }
        } catch (Exception e) {
            exceptionSend.senException("change get price nightlight => ", e, user);
            log.error("change get price nightlight => ", e);
        }

    }

    public void changePhoto(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            user.setState(UserState.EDIT_GET_PHOTO_STADIUM);
            userRepository.save(user);
            Stadium stadium = user.getActiveStadium();
            List<StadiumPhoto> byStadium = stadiumPhotoRepository.findByStadiumIdAndWhyOrderByCreatedAtDesc(stadium.getId(),
                    AttachmentTypeEnumWhy.STADIUM_PHOTO);
            for (StadiumPhoto stadiumPhoto : byStadium) {
                stadiumPhoto.setWhy(AttachmentTypeEnumWhy.DELETE_STADIUM_PHOTO);
                stadiumPhotoRepository.save(stadiumPhoto);
            }

            SendMessage delete = new SendMessage().setChatId(chat_id)
                    .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.ALL_PHOTO_DELETE_UZ : language.equals(
                            BotAnswerString.ru) ? BotAnswerString.ALL_PHOTO_DELETE_RU : language.equals(
                            BotAnswerString.uzl) ? BotAnswerString.ALL_PHOTO_DELETE_UZL : BotAnswerString.ALL_PHOTO_DELETE_EN);
            BRBTBot.execute(delete);

            EditMessageText editMessageText = new EditMessageText().setChatId(chat_id)
                    .setText(BotAnswerString.CHANGED)
                    .setMessageId(update.getCallbackQuery().getMessage().getMessageId());
            BRBTBot.execute(editMessageText);

            SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                    .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.SEND_ME_PHOTO_FOR_STADIUM_UZ : language.equals(
                            BotAnswerString.ru) ? BotAnswerString.SEND_ME_PHOTO_FOR_STADIUM_RU : language.equals(
                            BotAnswerString.uzl) ? BotAnswerString.SEND_ME_PHOTO_FOR_STADIUM_UZL : BotAnswerString.SEND_ME_PHOTO_FOR_STADIUM_EN);
            BRBTBot.execute(sendMessage);
        } catch (Exception e) {
            exceptionSend.senException("change photo for stadium => ", e, user);
            log.error("change photo for stadium => ", e);
        }

    }

    public void getEditPhoto(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            Stadium stadium = user.getActiveStadium();
            int l = stadiumPhotoRepository.countStadiumPhotoByStadiumAndWhy(stadium,
                    AttachmentTypeEnumWhy.STADIUM_PHOTO);
            if (update.getMessage().hasPhoto() && 10 > l) {
                workingALot.savePhoto(update, stadium, user);
                l = l + 1;
                List<String> strings = new ArrayList<>();
                strings.add(language.equals(BotAnswerString.uz) ? BotAnswerString.NO_ANOTHER_PICTURE_UZ : language.equals(
                        BotAnswerString.ru) ? BotAnswerString.NO_ANOTHER_PICTURE_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.NO_ANOTHER_PICTURE_UZL : BotAnswerString.NO_ANOTHER_PICTURE_EN);
                BRBTBot.execute(workingALot.keyboard(chat_id,
                        strings,
                        language.equals(BotAnswerString.uz) ? "(" + l + ")  " + BotAnswerString.IS_THERE_ANOTHER_PICTURE_UZ : language.equals(
                                BotAnswerString.ru) ? "(" + l + ")  " + BotAnswerString.IS_THERE_ANOTHER_PICTURE_RU : language.equals(
                                BotAnswerString.uzl) ? "(" + l + ")  " + BotAnswerString.IS_THERE_ANOTHER_PICTURE_UZL : "(" + l + ")  " + BotAnswerString.IS_THERE_ANOTHER_PICTURE_EN));
                if (l == 10) {
                    user.setState(UserState.VERIFY_STADIUM);
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.DAYLIGHT_PRICE_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.DAYLIGHT_PRICE_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.DAYLIGHT_PRICE_UZL : BotAnswerString.DAYLIGHT_PRICE_EN);

                    workingALot.sendSticker(chat_id, BotAnswerString.GET_SUCCESSFUL_STICKER);
                    workingALot.getStadiumInfo(language, chat_id, user.getActiveStadium());
                    BRBTBot.execute(sendMessage);
                    userRepository.save(user);
                }

            } else {
                String message = update.getMessage().getText();
                if ((update.getMessage()
                        .hasText() && (message.equals(BotAnswerString.NO_ANOTHER_PICTURE_UZ) || message.equals(
                        BotAnswerString.NO_ANOTHER_PICTURE_RU) || message.equals(BotAnswerString.NO_ANOTHER_PICTURE_UZL) || message.equals(
                        BotAnswerString.NO_ANOTHER_PICTURE_EN)) || l == 10)) {
                    user.setState(UserState.VERIFY_STADIUM);
                    userRepository.save(user);
                    workingALot.getStadiumInfo(language, chat_id, user.getActiveStadium());
                } else {
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.SEND_ME_PHOTO_FOR_STADIUM_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.SEND_ME_PHOTO_FOR_STADIUM_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.SEND_ME_PHOTO_FOR_STADIUM_UZL : BotAnswerString.SEND_ME_PHOTO_FOR_STADIUM_EN);
                    BRBTBot.execute(sendMessage);
                }
            }
        } catch (Exception e) {
            exceptionSend.senException("change get photo for stadium => ", e, user);
            log.error("change get photo for stadium => ", e);
        }

    }

    public void back(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            user.setState(UserState.VERIFY_STADIUM);
            userRepository.save(user);
            long message_id = update.getCallbackQuery().getMessage().getMessageId();

            List<String> stringList = new ArrayList<>();
            stringList.add(language.equals(BotAnswerString.uz) ? BotAnswerString.DELETE_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.DELETE_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.DELETE_UZL : BotAnswerString.DELETE_EN);
            stringList.add(language.equals(BotAnswerString.uz) ? BotAnswerString.EDIT_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.EDIT_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.EDIT_UZL : BotAnswerString.EDIT_EN);
            stringList.add(language.equals(BotAnswerString.uz) ? BotAnswerString.VERiFY_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.VERiFY_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.VERiFY_UZL : BotAnswerString.VERiFY_EN);
            BRBTBot.execute(workingALot.editInline(chat_id,
                    (int) message_id,
                    stringList,
                    workingALot.getInfoStadium(user.getActiveStadium(), user.getLanguage())));
        } catch (Exception e) {
            exceptionSend.senException("go to back => ", e, user);
            log.error("go to back => ", e);
        }
    }

    public void setActiveStadium(Update update, User user, String data, String language) {
        try {
            int message_id = update.getCallbackQuery().getMessage().getMessageId();
            long chat_id = user.getTelegramId();
            Stadium forDelete = user.getActiveStadium();
            int countPhoto = stadiumPhotoRepository.countStadiumPhotoByStadiumAndWhy(forDelete,
                    AttachmentTypeEnumWhy.STADIUM_PHOTO);
            for (int i = -1; i < countPhoto + 3; i++) {
                workingALot.deleteMsg(chat_id, message_id - i);
            }
            Stadium stadium = stadiumRepository.findByName(data);
            user.setActiveStadium(stadium);
            user.setState(UserState.VERIFY_STADIUM);
            userRepository.save(user);

            workingALot.getStadium(language, chat_id, user.getActiveStadium());
        } catch (Exception e) {
            exceptionSend.senException("stadium active for boss => ", e, user);
            log.error("stadium active for boss => ", e);
        }

    }

    public void changeAddress(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            user.setState(UserState.EDIT_SEND_ADDRESS);
            SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                    .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.ENTER_THE_ADDRESS_OF_THIS_STADIUM_MANUALLY_UZ : language.equals(
                            BotAnswerString.ru) ? BotAnswerString.ENTER_THE_ADDRESS_OF_THIS_STADIUM_MANUALLY_RU : language.equals(
                            BotAnswerString.uzl) ? BotAnswerString.ENTER_THE_ADDRESS_OF_THIS_STADIUM_MANUALLY_UZL : BotAnswerString.ENTER_THE_ADDRESS_OF_THIS_STADIUM_MANUALLY_EN);
            workingALot.sendSticker(chat_id, BotAnswerString.GET_ADDRESS_STICKER);
            BRBTBot.execute(sendMessage);
            userRepository.save(user);
        } catch (Exception e) {
            exceptionSend.senException("change address stadium for boss => ", e, user);
            log.error("change address stadium for boss => ", e);
        }

    }

    public void changePhoneNumber(Update update, User user, String language) {
        try {
            user.setType(UserType.USER);
            user.setState(UserState.EDIT_PHONE_NUMBER);
            workingALot.getPhoNumberButton(user.getLanguage(), update.getCallbackQuery().getMessage().getChatId());
            userRepository.save(user);
        } catch (Exception e) {
            exceptionSend.senException("change phone number => ", e, user);
            log.error("change phone number => ", e);
        }

    }

    public void getEditPhoneNumber(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            if ((update.getMessage().hasContact() || (update.getMessage().hasText()) && Pattern.compile(
                            "^[+][9][9][8][0-9]{9}$")
                    .matcher(update.getMessage().hasContact() ? update.getMessage()
                            .getContact()
                            .getPhoneNumber() : update.getMessage().getText())
                    .matches())) {
                Stadium stadium = user.getActiveStadium();
                String tel;
                if (update.getMessage().hasContact()) {
                    tel = update.getMessage().getContact().getPhoneNumber();
                    stadium.setPhone_number(tel.startsWith("+") ? tel : "+" + tel);
                    user.setPhoneNumber(user.getPhoneNumber() == null ? (tel.startsWith("+") ? tel : "+" + tel) : user.getPhoneNumber());
                } else {
                    tel = update.getMessage().getText();
                    user.setPhoneNumber(user.getPhoneNumber() == null ? (tel.startsWith("+") ? tel : "+" + tel) : user.getPhoneNumber());
                    stadium.setPhone_number(tel.startsWith("+") ? tel : "+" + tel);
                }
                stadiumRepository.save(stadium);
                SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                        .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.CHANGED_UZ : language.equals(
                                BotAnswerString.ru) ? BotAnswerString.CHANGED_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.CHANGED_UZL : BotAnswerString.CHANGED_EN);
                BRBTBot.execute(sendMessage);
                workingALot.getStadiumInfo(language, chat_id, stadium);
                user.setState(UserState.VERIFY_STADIUM);
                userRepository.save(user);
            } else {
                workingALot.getPhoNumberButton(language, chat_id);
            }

        } catch (TelegramApiException e) {
            exceptionSend.senException("change get phone number  => ", e, user);
            log.error("change get phone number  => ", e);
        }
    }

    public void changeSize(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            user.setState(UserState.GET_STADIUM_SIZE);
            BRBTBot.execute(new SendPhoto().setChatId(chat_id)
                    .setPhoto(BotAnswerString.GET_SIZE_FOR_STADIUM_PHOTO)
                    .setCaption("Photo"));
            SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                    .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.GET_STADIUM_SIZE_UZ : language.equals(
                            BotAnswerString.ru) ? BotAnswerString.GET_STADIUM_SIZE_RU : language.equals(
                            BotAnswerString.uzl) ? BotAnswerString.GET_STADIUM_SIZE_UZL : BotAnswerString.GET_STADIUM_SIZE_EN);
            BRBTBot.execute(sendMessage);
            userRepository.save(user);
        } catch (Exception e) {
            exceptionSend.senException("change size => ", e, user);
            log.error("change size => ", e);
        }

    }
}
