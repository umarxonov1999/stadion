package koinot.uz.backend.service;

import koinot.uz.backend.bot.BRBT_Bot;
import koinot.uz.backend.entity.*;
import koinot.uz.backend.entity.enums.AttachmentTypeEnumWhy;
import koinot.uz.backend.entity.enums.UserType;
import koinot.uz.backend.exception.ExceptionSend;
import koinot.uz.backend.payload.RegionLocation;
import koinot.uz.backend.payload.RestMyStadium;
import koinot.uz.backend.payload.StadiumDistance;
import koinot.uz.backend.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.ActionType;
import org.telegram.telegrambots.meta.api.methods.GetFile;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.*;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.File;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.PhotoSize;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.media.InputMedia;
import org.telegram.telegrambots.meta.api.objects.media.InputMediaPhoto;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Slf4j
public class WorkingALot {

    @Value("${upload.folder}")
    private String path;

    @Autowired
    BRBT_Bot BRBTBot;

    @Autowired
    WorkingALot workingALot;


    @Autowired
    StadiumPhotoRepository stadiumPhotoRepository;

    @Autowired
    StadiumRepository stadiumRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    OrderMoreWorking orderMoreWorking;

    @Autowired
    OrderActiveRepository orderActiveRepository;

    @Autowired
    OrderArchiveRepository orderArchiveRepository;

    @Autowired
    ExceptionSend exceptionSend;

    @Autowired
    IpAddressRepository ipAddressRepository;


    public HashMap<String, RegionLocation> getRegion() {
        HashMap<String, RegionLocation> map = new HashMap<String, RegionLocation>();
        map.put("Buxoro", new RegionLocation(40.23889549538938, 63.69305323923948));
        map.put("Xorazm", new RegionLocation(41.43169684098479, 60.814373456000936));
        map.put("Qoraqalpog’iston", new RegionLocation(43.8749571516692, 58.99633430869262));
        map.put("Andijon", new RegionLocation(40.78707843793689, 72.31277069904911));
        map.put("Jizzax", new RegionLocation(40.421077297551946, 67.5185093149638));
        map.put("Qashqadaryo", new RegionLocation(38.87503051415241, 66.06669570759992));
        map.put("Namangan", new RegionLocation(40.995563697565366, 71.24041018206712));
        map.put("Samarqand", new RegionLocation(39.89958673656362, 66.34418384153305));
        map.put("Sirdaryo", new RegionLocation(40.44738498965444, 68.6336399743867));
        map.put("Toshkent viloyati", new RegionLocation(41.19637267272812, 69.72155462331722));
        map.put("Toshkent shahar", new RegionLocation(41.297384035935984, 69.28811719500963));
        map.put("Farg’ona", new RegionLocation(40.37937721932437, 71.78428116879721));
        map.put("Navoiy", new RegionLocation(41.56390304189849, 64.18489352842364));

        return map;
    }

    public SendMessage keyboard(Long chatId, List<String> stringList, String text) {
        SendMessage message = new SendMessage() // Create a message object object
                .setChatId(chatId).setText(text);
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        keyboardMarkup.setResizeKeyboard(true);
        keyboardMarkup.setSelective(true);
        keyboardMarkup.setOneTimeKeyboard(true);
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow row;
        for (int i = 0; i < stringList.size(); i++) {
            row = new KeyboardRow();
            row.add(stringList.get(i));
            if (i < stringList.size() - 1) {
                row.add(stringList.get(i + 1));
                i++;
            }
            keyboard.add(row);
        }
        keyboardMarkup.setKeyboard(keyboard);
        message.setReplyMarkup(keyboardMarkup);
        return message;
    }

    public void getClientLocation(String language, Long chat_id, int msgId, String text) {
        try {
            deleteMsg(chat_id, msgId);
            orderMoreWorking.getRegionBtn(chat_id,
                    language,
                    (language.equals(BotAnswerString.uz) ? BotAnswerString.HELLO_MR_UZ : language.equals(
                            BotAnswerString.ru) ? BotAnswerString.HELLO_MR_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.HELLO_MR_UZL : BotAnswerString.HELLO_MR_EN) + text);
        } catch (Exception e) {
            exceptionSend.senException("after get client location delete message and send message conflict => ",
                    e,
                    null);
            log.error("after get client location delete message and send message conflict => ", e);
        }

    }

    public void cancelAll(String language, Long chat_id, String text) {
        try {
            SendMessage sendMessage = new SendMessage().setChatId(chat_id).setText(text);

            ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
            sendMessage.setReplyMarkup(replyKeyboardMarkup);
            replyKeyboardMarkup.setSelective(true);
            replyKeyboardMarkup.setResizeKeyboard(true);
            replyKeyboardMarkup.setOneTimeKeyboard(true);

            List<KeyboardRow> keyboard = new ArrayList<>();
            KeyboardRow keyboardFirstRow = new KeyboardRow();
            KeyboardButton keyboardButton = new KeyboardButton();

            keyboardButton.setText(language.equals(BotAnswerString.uz) ? BotAnswerString.RESTART_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.RESTART_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.RESTART_UZL : BotAnswerString.RESTART_EN);

            keyboardFirstRow.add(keyboardButton);

            keyboard.add(keyboardFirstRow);
            replyKeyboardMarkup.setKeyboard(keyboard);
            BRBTBot.execute(sendMessage);
        } catch (Exception e) {
            exceptionSend.senException("restart client => ", e, null);
            log.error("restart client => ", e);
        }

    }

    public SendSticker keyboardSticker(Long chatId, List<String> stringList, String text) {
        SendSticker message = new SendSticker() // Create a message object object
                .setChatId(chatId).setSticker(text);
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        keyboardMarkup.setResizeKeyboard(true);
        keyboardMarkup.setSelective(true);
        keyboardMarkup.setOneTimeKeyboard(true);
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow row;
        for (int i = 0; i < stringList.size(); i++) {
            row = new KeyboardRow();
            row.add(stringList.get(i));
            if (i < stringList.size() - 1) {
                row.add(stringList.get(i + 1));
                i++;
            }
            keyboard.add(row);
        }
        keyboardMarkup.setKeyboard(keyboard);
        message.setReplyMarkup(keyboardMarkup);
        return message;
    }

    public SendMessage inline(Long chatId, List<String> stringList, String text) {
        SendMessage sendMessage = new SendMessage().setChatId(chatId).setText(text);
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> listListRows = new ArrayList<>();
        for (int i = 0; i < stringList.size(); i++) {
            List<InlineKeyboardButton> inlineKeyboardButton = new ArrayList<>();
            inlineKeyboardButton.add(new InlineKeyboardButton().setText(stringList.get(i))
                    .setCallbackData(stringList.get(i).replace(' ', '_')));
            if (i < stringList.size() - 1) {
                inlineKeyboardButton.add(new InlineKeyboardButton().setText(stringList.get(i + 1))
                        .setCallbackData(stringList.get(i + 1).replace(' ', '_')));
                i++;
            }
            listListRows.add(inlineKeyboardButton);
        }
        inlineKeyboardMarkup.setKeyboard(listListRows);
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        return sendMessage;
    }

    public SendMessage inlineClientList(Long chatId, List<String> stringList, String text, int prev, int next) {
        SendMessage sendMessage = new SendMessage().setChatId(chatId).setText(text);
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> listListRows = new ArrayList<>();
        for (int i = 0; i < stringList.size(); i++) {
            List<InlineKeyboardButton> inlineKeyboardButton = new ArrayList<>();
            inlineKeyboardButton.add(new InlineKeyboardButton().setText(stringList.get(i))
                    .setCallbackData(BotAnswerString.koinot + stringList.get(i)));
            if (i < stringList.size() - 1) {
                inlineKeyboardButton.add(new InlineKeyboardButton().setText(stringList.get(i + 1))
                        .setCallbackData(BotAnswerString.koinot + stringList.get(i + 1)));
                i++;
            }
            listListRows.add(inlineKeyboardButton);
        }

        List<InlineKeyboardButton> inlineKeyboardButton = new ArrayList<>();
        inlineKeyboardButton.add(new InlineKeyboardButton().setText(BotAnswerString.PREV)
                .setCallbackData(BotAnswerString._koinot_prev_ + prev));
        inlineKeyboardButton.add(new InlineKeyboardButton().setText(BotAnswerString.CANCEL)
                .setCallbackData("cancel" + BotAnswerString.CANCEL));
        inlineKeyboardButton.add(new InlineKeyboardButton().setText(BotAnswerString.NEXT)
                .setCallbackData(BotAnswerString._koinot_next_ + next));
        listListRows.add(inlineKeyboardButton);

        inlineKeyboardMarkup.setKeyboard(listListRows);
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        return sendMessage;
    }

    public EditMessageText inlineClientListEdit(Long chatId, List<String> stringList, String text, int prev, int next,
                                                Integer msgId, int countNext) {
        EditMessageText sendMessage = new EditMessageText().setChatId(chatId).setText(text).setMessageId(msgId);
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> listListRows = new ArrayList<>();
        for (int i = 0; i < stringList.size(); i++) {
            List<InlineKeyboardButton> inlineKeyboardButton = new ArrayList<>();
            inlineKeyboardButton.add(new InlineKeyboardButton().setText(stringList.get(i))
                    .setCallbackData("koinot" + stringList.get(i)));
            if (i < stringList.size() - 1) {
                inlineKeyboardButton.add(new InlineKeyboardButton().setText(stringList.get(i + 1))
                        .setCallbackData("koinot" + stringList.get(i + 1)));
                i++;
            }
            listListRows.add(inlineKeyboardButton);
        }

        List<InlineKeyboardButton> inlineKeyboardButton = new ArrayList<>();
        inlineKeyboardButton.add(new InlineKeyboardButton().setText(BotAnswerString.PREV + " " + prev)
                .setCallbackData("_koinot_prev_" + prev));
        inlineKeyboardButton.add(new InlineKeyboardButton().setText(BotAnswerString.CANCEL)
                .setCallbackData("cancel" + BotAnswerString.CANCEL));
        inlineKeyboardButton.add(new InlineKeyboardButton().setText((countNext < 0 ? "0" : countNext) + " " + BotAnswerString.NEXT)
                .setCallbackData("_koinot_next_" + next));
        listListRows.add(inlineKeyboardButton);

        inlineKeyboardMarkup.setKeyboard(listListRows);
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        return sendMessage;
    }

    public SendMessage inlineOne(Long chatId, List<String> stringList, String text) {
        SendMessage sendMessage = new SendMessage().setChatId(chatId).setText(text);
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> listListRows = new ArrayList<>();
        for (int i = 0; i < stringList.size(); i++) {
            List<InlineKeyboardButton> inlineKeyboardButton = new ArrayList<>();
            inlineKeyboardButton.add(new InlineKeyboardButton().setText(stringList.get(i))
                    .setCallbackData(stringList.get(i).replace(' ', '_')));
            listListRows.add(inlineKeyboardButton);
        }
        inlineKeyboardMarkup.setKeyboard(listListRows);
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        return sendMessage;
    }

    public EditMessageText editInline(Long chatId, Integer msgId, List<String> stringList, String text) {
        EditMessageText EditMessageText = new EditMessageText().setChatId(chatId)
                .setText(text)
                .setMessageId(msgId);
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> listListRows = new ArrayList<>();
        for (int i = 0; i < stringList.size(); i++) {
            List<InlineKeyboardButton> inlineKeyboardButton = new ArrayList<>();
            inlineKeyboardButton.add(new InlineKeyboardButton().setText(stringList.get(i))
                    .setCallbackData(stringList.get(i).replace(' ', '_')));
            if (i < stringList.size() - 1) {
                inlineKeyboardButton.add(new InlineKeyboardButton().setText(stringList.get(i + 1))
                        .setCallbackData(stringList.get(i + 1).replace(' ', '_')));
                i++;
            }
            listListRows.add(inlineKeyboardButton);
        }
        inlineKeyboardMarkup.setKeyboard(listListRows);
        EditMessageText.setReplyMarkup(inlineKeyboardMarkup);
        return EditMessageText;
    }

    public EditMessageText editInlineNotEditText(Long chatId, Integer msgId, List<String> stringList) {
        EditMessageText EditMessageText = new EditMessageText().setChatId(chatId).setMessageId(msgId);
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> listListRows = new ArrayList<>();
        for (int i = 0; i < stringList.size(); i++) {
            List<InlineKeyboardButton> inlineKeyboardButton = new ArrayList<>();
            inlineKeyboardButton.add(new InlineKeyboardButton().setText(stringList.get(i))
                    .setCallbackData(stringList.get(i).replace(' ', '_')));
            if (i < stringList.size() - 1) {
                inlineKeyboardButton.add(new InlineKeyboardButton().setText(stringList.get(i + 1))
                        .setCallbackData(stringList.get(i + 1).replace(' ', '_')));
                i++;
            }
            listListRows.add(inlineKeyboardButton);
        }
        inlineKeyboardMarkup.setKeyboard(listListRows);
        EditMessageText.setReplyMarkup(inlineKeyboardMarkup);
        return EditMessageText;
    }

    public void adminMessage(String language, long chat_id, Stadium stadium) {

        try {
            String answer = getInfoStadium(stadium, language);

            SendLocation sendLocation = new SendLocation().setChatId(chat_id)
                    .setLatitude((float) stadium.getLatitude())
                    .setLongitude((float) stadium.getLongitude());
            BRBTBot.execute(sendLocation);

            workingALot.sendStadiumPhoto(stadium, chat_id);

            String verify = language.equals(BotAnswerString.uz) ? BotAnswerString.VERiFY_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.VERiFY_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.VERiFY_UZL : BotAnswerString.VERiFY_EN;

            String delete = language.equals(BotAnswerString.uz) ? BotAnswerString.DELETE_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.DELETE_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.DELETE_UZL : BotAnswerString.DELETE_EN;

            String workingTime = language.equals(BotAnswerString.uz) ? BotAnswerString.WORKING_HOURS_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.WORKING_HOURS_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.WORKING_HOURS_UZL : BotAnswerString.WORKING_HOURS_EN;

            String price = language.equals(BotAnswerString.uz) ? BotAnswerString.PRICE_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.PRICE_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.PRICE_UZL : BotAnswerString.PRICE_EN;

            String address = language.equals(BotAnswerString.uz) ? BotAnswerString.ADDRESS_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.ADDRESS_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.ADDRESS_UZL : BotAnswerString.ADDRESS_EN;

            String location = language.equals(BotAnswerString.uz) ? BotAnswerString.LOCATION_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.LOCATION_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.LOCATION_UZL : BotAnswerString.LOCATION_EN;

            String phone = language.equals(BotAnswerString.uz) ? BotAnswerString.PHONE_NUMBER_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.PHONE_NUMBER_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.PHONE_NUMBER_UZL : BotAnswerString.PHONE_NUMBER_EN;

            String name = language.equals(BotAnswerString.uz) ? BotAnswerString.NAME_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.NAME_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.NAME_UZL : BotAnswerString.NAME_EN;

            String timeForChangePrice = language.equals(BotAnswerString.uz) ? BotAnswerString.TIME_OF_EVENING_PRICE_CHANGE_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.TIME_OF_EVENING_PRICE_CHANGE_RU : language.equals(
                    BotAnswerString.uzl) ? BotAnswerString.TIME_OF_EVENING_PRICE_CHANGE_UZL : BotAnswerString.TIME_OF_EVENING_PRICE_CHANGE_EN;

            String photo = language.equals(BotAnswerString.uz) ? BotAnswerString.STADIUM_PHOTO_CHANGE_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.STADIUM_PHOTO_CHANGE_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.STADIUM_PHOTO_CHANGE_UZL : BotAnswerString.STADIUM_PHOTO_CHANGE_EN;

            String size = language.equals(BotAnswerString.uz) ? BotAnswerString.SIZE_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.SIZE_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.SIZE_UZL : BotAnswerString.SIZE_EN;

            SendMessage sendMessage = new SendMessage().setChatId(chat_id).setText(answer);
            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> listListRows = new ArrayList<>();

            List<InlineKeyboardButton> deleteAndverifyBBtn = new ArrayList<>();
            deleteAndverifyBBtn.add(new InlineKeyboardButton().setText(delete)
                    .setCallbackData(stadium.getId() + "_" + delete));

            deleteAndverifyBBtn.add(new InlineKeyboardButton().setText(verify)
                    .setCallbackData(stadium.getId() + "_" + verify));
            listListRows.add(deleteAndverifyBBtn);

            List<InlineKeyboardButton> workingTimeAndPriceBtn = new ArrayList<>();
            workingTimeAndPriceBtn.add(new InlineKeyboardButton().setText(workingTime)
                    .setCallbackData(stadium.getId() + "_" + workingTime));

            workingTimeAndPriceBtn.add(new InlineKeyboardButton().setText(price)
                    .setCallbackData(stadium.getId() + "_" + price));
            listListRows.add(workingTimeAndPriceBtn);

            List<InlineKeyboardButton> addressAndLocationBtn = new ArrayList<>();
            addressAndLocationBtn.add(new InlineKeyboardButton().setText(address)
                    .setCallbackData(stadium.getId() + "_" + address));

            addressAndLocationBtn.add(new InlineKeyboardButton().setText(location)
                    .setCallbackData(stadium.getId() + "_" + location));
            listListRows.add(addressAndLocationBtn);

            List<InlineKeyboardButton> phoneAndNameBtn = new ArrayList<>();
            phoneAndNameBtn.add(new InlineKeyboardButton().setText(phone)
                    .setCallbackData(stadium.getId() + "_" + phone));

            phoneAndNameBtn.add(new InlineKeyboardButton().setText(name)
                    .setCallbackData(stadium.getId() + "_" + name));
            listListRows.add(phoneAndNameBtn);

            List<InlineKeyboardButton> timeForChangePriceAndPhotoBtn = new ArrayList<>();
            timeForChangePriceAndPhotoBtn.add(new InlineKeyboardButton().setText(timeForChangePrice)
                    .setCallbackData(stadium.getId() + "_" + timeForChangePrice));

            timeForChangePriceAndPhotoBtn.add(new InlineKeyboardButton().setText(photo)
                    .setCallbackData(stadium.getId() + "_" + photo));
            listListRows.add(timeForChangePriceAndPhotoBtn);

            List<InlineKeyboardButton> sizeBtn = new ArrayList<>();
            sizeBtn.add(new InlineKeyboardButton().setText(size).setCallbackData(stadium.getId() + "_" + size));
            listListRows.add(sizeBtn);

            inlineKeyboardMarkup.setKeyboard(listListRows);
            sendMessage.setReplyMarkup(inlineKeyboardMarkup);

            BRBTBot.execute(sendMessage);
        } catch (Exception e) {
            exceptionSend.senException("send admin message => ", e, null);
            log.error("send admin message => ", e);
        }

    }

    public void getLanguage(String language, long chat_id) {
        try {
            List<String> strings = new ArrayList<>();
            strings.add(BotAnswerString.en);
            strings.add(BotAnswerString.ru);
            strings.add(BotAnswerString.uzl);
            strings.add(BotAnswerString.uz);

            BRBTBot.execute(inline(chat_id,
                    strings,
                    (BotAnswerString.HELLO_CHOOSE_A_LANGUAGE_EN
                            + System.lineSeparator() +
                            BotAnswerString.HELLO_CHOOSE_A_LANGUAGE_RU
                            + System.lineSeparator() +
                            BotAnswerString.HELLO_CHOOSE_A_LANGUAGE_UZL
                            + System.lineSeparator() +
                            BotAnswerString.HELLO_CHOOSE_A_LANGUAGE_UZ
                    )));


//            if(language == null){
//                BRBTBot.execute( inline( chat_id,
//                        strings,
//                        BotAnswerString.HELLO_CHOOSE_A_LANGUAGE_EN ) ); // Sending our message object to user
//            }else{
//                switch (language) {
//                    case ( BotAnswerString.en ):
//                    case ( "en" ):
//                        BRBTBot.execute( inline( chat_id,
//                                strings,
//                                BotAnswerString.HELLO_CHOOSE_A_LANGUAGE_EN ) ); // Sending our message object to user
//                        break;
//                    case ( BotAnswerString.ru ):
//                    case ( "ru" ):
//                        BRBTBot.execute( inline( chat_id,
//                                strings,
//                                BotAnswerString.HELLO_CHOOSE_A_LANGUAGE_RU ) ); // Sending our message object to user
//                        break;
//                    case ( "uz" ):
//                    case ( BotAnswerString.uzl ):
//                        BRBTBot.execute( inline( chat_id,
//                                strings,
//                                BotAnswerString.HELLO_CHOOSE_A_LANGUAGE_UZL ) ); // Sending our message object to user
//                        break;
//                    case ( BotAnswerString.uz ):
//                        BRBTBot.execute( inline( chat_id,
//                                strings,
//                                BotAnswerString.HELLO_CHOOSE_A_LANGUAGE_UZ ) ); // Sending our message object to user
//                        break;
//                    default:
//                        BRBTBot.execute( inline( chat_id,
//                                strings,
//                                BotAnswerString.HELLO_CHOOSE_A_LANGUAGE_EN ) ); // Sending our message object to user
//                        break;
//                }
//            }
        } catch (Exception e) {
            exceptionSend.senException("get language  => ", e, null);
            log.error("get language  => ", e);
        }

    }

    public double distance(double lon1, double lat1, double lon2, double lat2, double el1, double el2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(lat1)) * Math.cos(
                Math.toRadians(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }

    public void getStadiumInfo(String language, long chat_id, Stadium stadium) {
        try {

            sendStadiumPhoto(stadium, chat_id);

            SendLocation sendLocation = new SendLocation().setChatId(chat_id)
                    .setLatitude((float) stadium.getLatitude())
                    .setLongitude((float) stadium.getLongitude());
            BRBTBot.execute(sendLocation);

            List<String> stringList = new ArrayList<>();
            stringList.add(language.equals(BotAnswerString.uz) ? BotAnswerString.DELETE_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.DELETE_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.DELETE_UZL : BotAnswerString.DELETE_EN);
            stringList.add(language.equals(BotAnswerString.uz) ? BotAnswerString.EDIT_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.EDIT_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.EDIT_UZL : BotAnswerString.EDIT_EN);
            stringList.add(language.equals(BotAnswerString.uz) ? BotAnswerString.VERiFY_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.VERiFY_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.VERiFY_UZL : BotAnswerString.VERiFY_EN);
            BRBTBot.execute(inline(chat_id, stringList, getInfoStadium(stadium, language)));
        } catch (Exception e) {
            exceptionSend.senException("get stadium info => ", e, null);
            log.error("get stadium info => ", e);
        }

    }

    public void getStadiumInfoClient(String language, User user, Stadium stadium) {
        try {

            long chat_id = user.getTelegramId();

            sendStadiumPhoto(stadium, chat_id);

            SendLocation sendLocation = new SendLocation().setChatId(chat_id)
                    .setLatitude((float) stadium.getLatitude())
                    .setLongitude((float) stadium.getLongitude());
            BRBTBot.execute(sendLocation);

            String text = getInfoStadium(stadium, language);
            int dis = (int) distance(stadium.getLongitude(),
                    stadium.getLatitude(),
                    user.getLongitude(),
                    user.getLatitude(),
                    0,
                    0);
            String distance = language.equals(BotAnswerString.uz) ? BotAnswerString.DISTANCE_UZ + ": " + dis / 1000 + " Km (" + dis % 1000 + " m)" + System.lineSeparator() : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.DISTANCE_RU + ": " + dis / 1000 + " Km (" + dis % 1000 + " m)" + System.lineSeparator() : language.equals(
                    BotAnswerString.uzl) ? BotAnswerString.DISTANCE_UZL + ": " + dis / 1000 + " Km (" + dis % 1000 + " m)" + System.lineSeparator() : BotAnswerString.DISTANCE_EN + ": " + dis / 1000 + " Km (" + dis % 1000 + " m)" + System.lineSeparator();

            text = distance.concat(text);
            String order = "";
            for (OrdersActive ordersActive : orderActiveRepository.findAllByActiveAndStadiumId(true,
                    stadium.getId())) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date date = ordersActive.getTime();

                order = order + formatter.format(date) + "  ➡️      " + workingALot.getParsDade(ordersActive.getStartDate()) + " - " + workingALot.getParsDade(
                        ordersActive.getEndDate()) + System.lineSeparator();
            }
            text = text.concat((language.equals(BotAnswerString.uz) ? BotAnswerString.ORDER_LIST_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.ORDER_LIST_RU : language.equals(
                    BotAnswerString.uzl) ? BotAnswerString.ORDER_LIST_UZL : BotAnswerString.ORDER_LIST_EN) + System.lineSeparator());
            text = text.concat(order);
            if (!(order.length() > 3)) {
                text = text.concat(BotAnswerString.NOT_FOUND);
            }
            String orderText = language.equals(BotAnswerString.uz) ? BotAnswerString.ORDER_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.ORDER_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.ORDER_UZL : BotAnswerString.ORDER_EN;

            SendMessage sendMessage = new SendMessage().setChatId(chat_id).setText(text);
            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> listListRows = new ArrayList<>();
            List<InlineKeyboardButton> inlineKeyboardButton = new ArrayList<>();

            inlineKeyboardButton.add(new InlineKeyboardButton().setText(BotAnswerString.CANCEL)
                    .setCallbackData(BotAnswerString.cancel_client + stadium.getName()));

            inlineKeyboardButton.add(new InlineKeyboardButton().setText(orderText)
                    .setCallbackData(BotAnswerString.get_order + stadium.getName()));

            listListRows.add(inlineKeyboardButton);
            inlineKeyboardMarkup.setKeyboard(listListRows);
            sendMessage.setReplyMarkup(inlineKeyboardMarkup);
            BRBTBot.execute(sendMessage);
        } catch (Exception e) {
            exceptionSend.senException("get Stadium Info Client => ", e, null);
            log.error("get Stadium Info Client => ", e);
        }

    }

    public void getStadium(String language, long chat_id, Stadium stadium) {
        try {

            String answer = getInfoStadium(stadium, language);

            for (OrdersActive ordersActive : orderActiveRepository.findAllByActiveAndStadiumId(true,
                    stadium.getId())) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date date = ordersActive.getTime();

                answer = answer + formatter.format(date) + "  ➡️      " + workingALot.getParsDade(ordersActive.getStartDate()) + " - " + workingALot.getParsDade(
                        ordersActive.getEndDate()) + System.lineSeparator();
            }
            sendStadiumPhoto(stadium, chat_id);

            SendLocation sendLocation = new SendLocation().setChatId(chat_id)
                    .setLatitude((float) stadium.getLatitude())
                    .setLongitude((float) stadium.getLongitude());
            BRBTBot.execute(sendLocation);

            List<String> stringList = new ArrayList<>();
            stringList.add(language.equals(BotAnswerString.uz) ? BotAnswerString.DELETE_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.DELETE_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.DELETE_UZL : BotAnswerString.DELETE_EN);
            stringList.add(language.equals(BotAnswerString.uz) ? BotAnswerString.EDIT_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.EDIT_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.EDIT_UZL : BotAnswerString.EDIT_EN);
            stringList.add(language.equals(BotAnswerString.uz) ? BotAnswerString.VERiFY_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.VERiFY_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.VERiFY_UZL : BotAnswerString.VERiFY_EN);

            BRBTBot.execute(inline(chat_id, stringList, answer));
        } catch (Exception e) {
            exceptionSend.senException("get stadium setting for admin  => ", e, null);
            log.error("get stadium setting for admin  => ", e);
        }

    }

    public void deleteMsg(Long chatId, Integer msgId) {
        try {
            DeleteMessage deleteMessage = new DeleteMessage();
            deleteMessage.setChatId(chatId);
            deleteMessage.setMessageId(msgId);
            BRBTBot.execute(deleteMessage);
        } catch (Exception e) {
            log.error("message not delete => ", e);

        }
    }

    public void getStadiumDashButton(String language, long chat_id) {
        try {
            List<String> strings = new ArrayList<>();
            strings.add(language.equals(BotAnswerString.uz) ? BotAnswerString.STADIUM_DASHBOARD_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.STADIUM_DASHBOARD_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.STADIUM_DASHBOARD_UZL : BotAnswerString.STADIUM_DASHBOARD_EN);

            strings.add(language.equals(BotAnswerString.uz) ? BotAnswerString.CHANGE_LANGUAGE_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.CHANGE_LANGUAGE_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.CHANGE_LANGUAGE_UZL : BotAnswerString.CHANGE_LANGUAGE_EN);

            BRBTBot.execute(keyboard(chat_id,
                    strings,
                    language.equals(BotAnswerString.uz) ? BotAnswerString.STADIUM_CHANGE_MESSAGE_UZ : language.equals(
                            BotAnswerString.ru) ? BotAnswerString.STADIUM_CHANGE_MESSAGE_RU : language.equals(
                            BotAnswerString.uzl) ? BotAnswerString.STADIUM_CHANGE_MESSAGE_UZL : BotAnswerString.STADIUM_CHANGE_MESSAGE_EN));
        } catch (Exception e) {
            exceptionSend.senException("get stadium dashboard btn => ", e, null);
            log.error("get stadium dashboard btn => ", e);
        }

    }

    public void getLocationButton(String language, long chat_id) {
        try {
            SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                    .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.GET_ADDRESS_STADIUM_UZ : language.equals(
                            BotAnswerString.ru) ? BotAnswerString.GET_ADDRESS_STADIUM_RU : language.equals(
                            BotAnswerString.uzl) ? BotAnswerString.GET_ADDRESS_STADIUM_UZL : BotAnswerString.GET_ADDRESS_STADIUM_EN);
            ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
            sendMessage.setReplyMarkup(replyKeyboardMarkup);
            replyKeyboardMarkup.setSelective(true);
            replyKeyboardMarkup.setResizeKeyboard(true);
            replyKeyboardMarkup.setOneTimeKeyboard(true);
            List<KeyboardRow> keyboard = new ArrayList<>();
            KeyboardRow keyboardFirstRow = new KeyboardRow();
            KeyboardButton keyboardButton = new KeyboardButton();
            keyboardButton.setText(language.equals(BotAnswerString.uz) ? BotAnswerString.SEND_MY_CURRENT_LOCATION_UZ : language.equals(
                            BotAnswerString.ru) ? BotAnswerString.SEND_MY_CURRENT_LOCATION_RU : language.equals(
                            BotAnswerString.uzl) ? BotAnswerString.SEND_MY_CURRENT_LOCATION_UZL : BotAnswerString.SEND_MY_CURRENT_LOCATION_EN)
                    .setRequestLocation(true);
            keyboardFirstRow.add(keyboardButton);
            keyboard.add(keyboardFirstRow);
            replyKeyboardMarkup.setKeyboard(keyboard);
            SendSticker sendSticker = new SendSticker().setChatId(chat_id)
                    .setSticker(BotAnswerString.GET_LOCATION_STICKER);
            BRBTBot.execute(sendSticker);
            BRBTBot.execute(sendMessage);
        } catch (Exception e) {
            exceptionSend.senException("get location btn => ", e, null);
            log.error("get location btn => ", e);
        }

    }

    public void getPhoNumberButton(String language, long chat_id) {
        SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.GET_PHONE_NUMBER_STADIUM_UZ : language.equals(
                        BotAnswerString.ru) ? BotAnswerString.GET_PHONE_NUMBER_STADIUM_RU : language.equals(
                        BotAnswerString.uzl) ? BotAnswerString.GET_PHONE_NUMBER_STADIUM_UZL : BotAnswerString.GET_PHONE_NUMBER_STADIUM_EN);
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(true);
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow keyboardFirstRow = new KeyboardRow();
        KeyboardButton keyboardButton = new KeyboardButton();
        keyboardButton.setText(language.equals(BotAnswerString.uz) ? BotAnswerString.SEND_THIS_PHONE_NUMBER_UZ : language.equals(
                        BotAnswerString.ru) ? BotAnswerString.SEND_THIS_PHONE_NUMBER_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.SEND_THIS_PHONE_NUMBER_UZL : BotAnswerString.SEND_THIS_PHONE_NUMBER_EN)
                .setRequestContact(true);
        keyboardFirstRow.add(keyboardButton);
        keyboard.add(keyboardFirstRow);
        replyKeyboardMarkup.setKeyboard(keyboard);
        try {
            SendSticker sendSticker = new SendSticker().setChatId(chat_id)
                    .setSticker(BotAnswerString.GET_PHONE_STICKER);
            BRBTBot.execute(sendSticker);
            BRBTBot.execute(sendMessage);
        } catch (TelegramApiException e) {
            exceptionSend.senException("get phone number btn => ", e, null);
            log.error("get phone number btn => ", e);
        }
    }

    public void getClientPhoNumberButton(String language, long chat_id) {
        SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.GET_CLIENT_PHONE_NUMBER_STADIUM_UZ : language.equals(
                        BotAnswerString.ru) ? BotAnswerString.GET_CLIENT_PHONE_NUMBER_STADIUM_RU : language.equals(
                        BotAnswerString.uzl) ? BotAnswerString.GET_CLIENT_PHONE_NUMBER_STADIUM_UZL : BotAnswerString.GET_CLIENT_PHONE_NUMBER_STADIUM_EN).setParseMode(ParseMode.HTML);
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(true);
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow keyboardFirstRow = new KeyboardRow();
        KeyboardButton keyboardButton = new KeyboardButton();
        keyboardButton.setText(language.equals(BotAnswerString.uz) ? BotAnswerString.SEND_THIS_PHONE_NUMBER_UZ : language.equals(
                        BotAnswerString.ru) ? BotAnswerString.SEND_THIS_PHONE_NUMBER_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.SEND_THIS_PHONE_NUMBER_UZL : BotAnswerString.SEND_THIS_PHONE_NUMBER_EN)
                .setRequestContact(true);
        keyboardFirstRow.add(keyboardButton);
        keyboard.add(keyboardFirstRow);
        replyKeyboardMarkup.setKeyboard(keyboard);
        try {
            BRBTBot.execute(sendMessage);
        } catch (TelegramApiException e) {
            exceptionSend.senException("get client phone number btn => ", e, null);
            log.error("get client phone number btn => ", e);
        }
    }

    public void sendSticker(long chat_id, String sticker) {
        try {
            SendSticker sendSticker = new SendSticker().setChatId(chat_id).setSticker(sticker);
            BRBTBot.execute(sendSticker);
        } catch (Exception e) {
            exceptionSend.senException("send sticker => ", e, null);
            log.error("send sticker => ", e);
        }

    }

    public void sendAdminsAlert(User user, String text) {
        try {
            for (User user1 : userRepository.findAllByType(UserType.SUPPER_ADMIN)) {
                String name = user.getFirstName() != null ? user.getFirstName() : " ";
                String lastname = user.getLastName() != null ? user.getLastName() : " ";
                SendMessage sendMessage = new SendMessage().setChatId(Long.valueOf(user1.getTelegramId()))
                        .setText(name + " " + lastname + System.lineSeparator() + text);
                BRBTBot.execute(sendMessage);
            }

        } catch (Exception e) {
            exceptionSend.senException("send admins alert => ", e, null);
            log.error("send admins alert => ", e);
        }
    }

    public String getInfoStadium(Stadium stadium, String language) {

        String size = language.equals(BotAnswerString.uz) ? BotAnswerString.SIZE_UZ + ": " + stadium.getHeight() + "X" + stadium.getWidth() + System.lineSeparator() : language.equals(
                BotAnswerString.ru) ? BotAnswerString.SIZE_RU + ": " + stadium.getWidth() + "X" + stadium.getHeight() + System.lineSeparator() : language.equals(
                BotAnswerString.uzl) ? BotAnswerString.SIZE_UZL + ": " + stadium.getWidth() + "X" + stadium.getHeight() + System.lineSeparator() : BotAnswerString.SIZE_EN + ": " + stadium.getWidth() + "X" + stadium.getHeight() + System.lineSeparator();

        String phone = language.equals(BotAnswerString.uz) ? BotAnswerString.PHONE_NUMBER_UZ + ": " + stadium.getPhone_number() + System.lineSeparator() : language.equals(
                BotAnswerString.ru) ? BotAnswerString.PHONE_NUMBER_RU + ": " + stadium.getPhone_number() + System.lineSeparator() : language.equals(
                BotAnswerString.uzl) ? BotAnswerString.PHONE_NUMBER_UZL + ": " + stadium.getPhone_number() + System.lineSeparator() : BotAnswerString.PHONE_NUMBER_EN + ": " + stadium.getPhone_number() + System.lineSeparator();

        String address = language.equals(BotAnswerString.uz) ? BotAnswerString.ADDRESS_UZ + ": " + stadium.getAddress() + System.lineSeparator() : language.equals(
                BotAnswerString.ru) ? BotAnswerString.ADDRESS_RU + ": " + stadium.getAddress() + System.lineSeparator() : language.equals(
                BotAnswerString.uzl) ? BotAnswerString.ADDRESS_UZL + ": " + stadium.getAddress() + System.lineSeparator() : BotAnswerString.ADDRESS_EN + ": " + stadium.getAddress() + System.lineSeparator();

        String name = (language.equals(BotAnswerString.uz) ? BotAnswerString.NAME_UZ + ": " + stadium.getName() + System.lineSeparator() : language.equals(
                BotAnswerString.ru) ? BotAnswerString.NAME_RU + ": " + stadium.getName() + System.lineSeparator() : language.equals(
                BotAnswerString.uzl) ? BotAnswerString.NAME_UZL + ": " + stadium.getName() + System.lineSeparator() : BotAnswerString.NAME_EN + ": " + stadium.getName() + System.lineSeparator()) + System.lineSeparator();

//        String like = stadium.getStadium_like() != null ? BotAnswerString.LIKE + stadium.getStadium_like() + System.lineSeparator() : BotAnswerString.LIKE + "0" + System.lineSeparator();

        String like = language.equals(BotAnswerString.uz) ? BotAnswerString.RATING_UZ + ": " + stadium.getStadium_like() + "❤️‍🔥" + System.lineSeparator() : language.equals(
                BotAnswerString.ru) ? BotAnswerString.RATING_RU + ": " + stadium.getStadium_like() + "❤️‍🔥" + System.lineSeparator() : language.equals(
                BotAnswerString.uzl) ? BotAnswerString.RATING_UZL + ": " + stadium.getStadium_like() + "❤️‍🔥" + System.lineSeparator() : BotAnswerString.RATING_EN + ": " + stadium.getStadium_like() + "❤️‍🔥" + System.lineSeparator();

        String openTime = language.equals(BotAnswerString.uz) ? BotAnswerString.WORKING_HOURS_UZ + ": " + BotAnswerString.ALWAYS_OPEN_UZ + System.lineSeparator() : language.equals(
                BotAnswerString.ru) ? BotAnswerString.WORKING_HOURS_RU + ": " + BotAnswerString.ALWAYS_OPEN_RU + System.lineSeparator() : language.equals(
                BotAnswerString.uzl) ? BotAnswerString.WORKING_HOURS_UZL + ": " + BotAnswerString.ALWAYS_OPEN_UZL + System.lineSeparator() : BotAnswerString.WORKING_HOURS_EN + ": " + BotAnswerString.ALWAYS_OPEN_EN + System.lineSeparator();


        String price = System.lineSeparator() + (language.equals(BotAnswerString.uz) ? BotAnswerString.PRICE_UZ : language.equals(
                BotAnswerString.ru) ? BotAnswerString.PRICE_RU : language.equals(
                BotAnswerString.uzl) ? BotAnswerString.PRICE_UZL : BotAnswerString.PRICE_EN) + System.lineSeparator();

        if (stadium.getOpening_time() != null && stadium.getClosing_time() != null) {
            openTime = language.equals(BotAnswerString.uz) ? BotAnswerString.WORKING_HOURS_UZ + ": " + getParsDade(
                    stadium.getOpening_time()) + " - " + getParsDade(stadium.getClosing_time()) + System.lineSeparator() : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.WORKING_HOURS_RU + ": " + getParsDade(stadium.getOpening_time()) + " - " + getParsDade(
                    stadium.getClosing_time()) + System.lineSeparator() : language.equals(BotAnswerString.uzl) ? BotAnswerString.WORKING_HOURS_UZL + ": " + getParsDade(
                    stadium.getOpening_time()) + " - " + getParsDade(stadium.getClosing_time()) + System.lineSeparator() : BotAnswerString.WORKING_HOURS_EN + ": " + getParsDade(
                    stadium.getOpening_time()) + " - " + getParsDade(stadium.getClosing_time()) + System.lineSeparator();
        }
        String timePriceAfter = "";
        String timePriceBefore = "";
        if (stadium.getChange_price_time() != null) {

            timePriceAfter = language.equals(BotAnswerString.uz) ? getParsDade(stadium.getChange_price_time()) + " " + BotAnswerString.PRICE_AFTER_UZ + " " + stadium.getPrice_night_time() + BotAnswerString.MANY + System.lineSeparator() : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.PRICE_AFTER_RU + getParsDade(stadium.getChange_price_time()) + " " + stadium.getPrice_night_time() + BotAnswerString.MANY + System.lineSeparator() : language.equals(
                    BotAnswerString.uzl) ? getParsDade(stadium.getChange_price_time()) + " " + BotAnswerString.PRICE_AFTER_UZL + " " + stadium.getPrice_night_time() + BotAnswerString.MANY + System.lineSeparator() : BotAnswerString.PRICE_AFTER_EN + getParsDade(
                    stadium.getChange_price_time()) + " " + stadium.getPrice_night_time() + BotAnswerString.MANY + System.lineSeparator();

            timePriceBefore = language.equals(BotAnswerString.uz) ? getParsDade(stadium.getChange_price_time()) + " " + BotAnswerString.PRICE_BEFORE_UZ + " " + stadium.getPrice_day_time() + BotAnswerString.MANY + System.lineSeparator() : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.PRICE_BEFORE_RU + getParsDade(stadium.getChange_price_time()) + " " + stadium.getPrice_day_time() + BotAnswerString.MANY + System.lineSeparator() : language.equals(
                    BotAnswerString.uzl) ? getParsDade(stadium.getChange_price_time()) + " " + BotAnswerString.PRICE_BEFORE_UZL + " " + stadium.getPrice_day_time() + BotAnswerString.MANY + System.lineSeparator() : BotAnswerString.PRICE_BEFORE_EN + getParsDade(
                    stadium.getChange_price_time()) + " " + stadium.getPrice_day_time() + BotAnswerString.MANY + System.lineSeparator();
        }

        return size.concat(phone)
                .concat(address)
                .concat(like)
                .concat(openTime)
                .concat(price)
                .concat(timePriceAfter)
                .concat(timePriceBefore)
                .concat(name)
                .concat(BotAnswerString.GET_NEW_LINE_STICKER);
    }

    public String getInfoStadiumForClient(StadiumDistance stadium, String language) {
        long d = (int) stadium.getDistance();

        String distance = language.equals(BotAnswerString.uz) ? BotAnswerString.DISTANCE_UZ + ": " + d / 1000 + " Km (" + d % 1000 + " m)" + System.lineSeparator() : language.equals(
                BotAnswerString.ru) ? BotAnswerString.DISTANCE_RU + ": " + d / 1000 + " Km (" + d % 1000 + " m)" + " Km" + System.lineSeparator() : language.equals(
                BotAnswerString.uzl) ? BotAnswerString.DISTANCE_UZL + ": " + d / 1000 + " Km (" + d % 1000 + " m)" + System.lineSeparator() : BotAnswerString.DISTANCE_EN + ": " + d / 1000 + " Km (" + d % 1000 + " m)" + " Km" + System.lineSeparator();

        String size = language.equals(BotAnswerString.uz) ? BotAnswerString.SIZE_UZ + ": " + stadium.getWidth() + "X" + stadium.getHeight() + System.lineSeparator() : language.equals(
                BotAnswerString.ru) ? BotAnswerString.SIZE_RU + ": " + stadium.getWidth() + "X" + stadium.getHeight() + System.lineSeparator() : language.equals(
                BotAnswerString.uzl) ? BotAnswerString.SIZE_UZL + ": " + stadium.getWidth() + "X" + stadium.getHeight() + System.lineSeparator() : BotAnswerString.SIZE_EN + ": " + stadium.getWidth() + "X" + stadium.getHeight() + System.lineSeparator();

        String phone = language.equals(BotAnswerString.uz) ? BotAnswerString.PHONE_NUMBER_UZ + ": " + stadium.getPhoneNumber() + System.lineSeparator() : language.equals(
                BotAnswerString.ru) ? BotAnswerString.PHONE_NUMBER_RU + ": " + stadium.getPhoneNumber() + System.lineSeparator() : language.equals(
                BotAnswerString.uzl) ? BotAnswerString.PHONE_NUMBER_UZL + ": " + stadium.getPhoneNumber() + System.lineSeparator() : BotAnswerString.PHONE_NUMBER_EN + ": " + stadium.getPhoneNumber() + System.lineSeparator();

        String address = language.equals(BotAnswerString.uz) ? BotAnswerString.ADDRESS_UZ + ": " + stadium.getAddress() + System.lineSeparator() : language.equals(
                BotAnswerString.ru) ? BotAnswerString.ADDRESS_RU + ": " + stadium.getAddress() + System.lineSeparator() : language.equals(
                BotAnswerString.uzl) ? BotAnswerString.ADDRESS_UZL + ": " + stadium.getAddress() + System.lineSeparator() : BotAnswerString.ADDRESS_EN + ": " + stadium.getAddress() + System.lineSeparator();

        String name = (language.equals(BotAnswerString.uz) ? BotAnswerString.NAME_UZ + ": " + stadium.getName() + System.lineSeparator() : language.equals(
                BotAnswerString.ru) ? BotAnswerString.NAME_RU + ": " + stadium.getName() + System.lineSeparator() : language.equals(
                BotAnswerString.uzl) ? BotAnswerString.NAME_UZL + ": " + stadium.getName() + System.lineSeparator() : BotAnswerString.NAME_EN + stadium.getName() + ": " + System.lineSeparator()) + System.lineSeparator();

//        String like = stadium.getStadiumLike() != null ? BotAnswerString.LIKE + stadium.getStadiumLike() + System.lineSeparator() : BotAnswerString.LIKE + "0" + System.lineSeparator();

        String like = language.equals(BotAnswerString.uz) ? BotAnswerString.RATING_UZ + ": " + stadium.getStadiumLike() + "❤️‍🔥" + System.lineSeparator() : language.equals(
                BotAnswerString.ru) ? BotAnswerString.RATING_RU + ": " + stadium.getStadiumLike() + "❤️‍🔥" + System.lineSeparator() : language.equals(
                BotAnswerString.uzl) ? BotAnswerString.RATING_UZL + ": " + stadium.getStadiumLike() + "❤️‍🔥" + System.lineSeparator() : BotAnswerString.RATING_EN + ": " + stadium.getStadiumLike() + "❤️‍🔥" + System.lineSeparator();


        String price = System.lineSeparator() + (language.equals(BotAnswerString.uz) ? BotAnswerString.PRICE_UZ : language.equals(
                BotAnswerString.ru) ? BotAnswerString.PRICE_RU : language.equals(
                BotAnswerString.uzl) ? BotAnswerString.PRICE_UZL : BotAnswerString.PRICE_EN) + System.lineSeparator();


        String openTime = language.equals(BotAnswerString.uz) ? BotAnswerString.WORKING_HOURS_UZ + ": " + BotAnswerString.ALWAYS_OPEN_UZ + System.lineSeparator() : language.equals(
                BotAnswerString.ru) ? BotAnswerString.WORKING_HOURS_RU + ": " + BotAnswerString.ALWAYS_OPEN_RU + System.lineSeparator() : language.equals(
                BotAnswerString.uzl) ? BotAnswerString.WORKING_HOURS_UZL + ": " + BotAnswerString.ALWAYS_OPEN_UZL + System.lineSeparator() : BotAnswerString.WORKING_HOURS_EN + ": " + BotAnswerString.ALWAYS_OPEN_EN + System.lineSeparator();

        if (stadium.getOpeningTime() != null && stadium.getClosingTime() != null) {
            openTime = language.equals(BotAnswerString.uz) ? BotAnswerString.WORKING_HOURS_UZ + ": " + getParsDade(
                    stadium.getOpeningTime()) + " - " + getParsDade(stadium.getClosingTime()) + System.lineSeparator() : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.WORKING_HOURS_RU + ": " + getParsDade(stadium.getOpeningTime()) + " - " + getParsDade(
                    stadium.getClosingTime()) + System.lineSeparator() : language.equals(BotAnswerString.uzl) ? BotAnswerString.WORKING_HOURS_UZL + ": " + getParsDade(
                    stadium.getOpeningTime()) + " - " + getParsDade(stadium.getClosingTime()) + System.lineSeparator() : BotAnswerString.WORKING_HOURS_EN + ": " + getParsDade(
                    stadium.getOpeningTime()) + " - " + getParsDade(stadium.getClosingTime()) + System.lineSeparator();
        }
        String timePriceAfter = "";
        String timePriceBefore = "";
        if (stadium.getChangePriceTime() != null) {
            timePriceAfter = language.equals(BotAnswerString.uz) ? getParsDade(stadium.getChangePriceTime()) + " " + BotAnswerString.PRICE_AFTER_UZ + " " + stadium.getPriceNightTime() + BotAnswerString.MANY + System.lineSeparator() : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.PRICE_AFTER_RU + getParsDade(stadium.getChangePriceTime()) + " " + stadium.getPriceNightTime() + BotAnswerString.MANY + System.lineSeparator() : language.equals(
                    BotAnswerString.uzl) ? getParsDade(stadium.getChangePriceTime()) + " " + BotAnswerString.PRICE_AFTER_UZL + " " + stadium.getPriceNightTime() + BotAnswerString.MANY + System.lineSeparator() : BotAnswerString.PRICE_AFTER_EN + getParsDade(
                    stadium.getChangePriceTime()) + " " + stadium.getPriceNightTime() + BotAnswerString.MANY + System.lineSeparator();

            timePriceBefore = language.equals(BotAnswerString.uz) ? getParsDade(stadium.getChangePriceTime()) + " " + BotAnswerString.PRICE_BEFORE_UZ + " " + stadium.getPriceDayTime() + BotAnswerString.MANY + System.lineSeparator() : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.PRICE_BEFORE_RU + getParsDade(stadium.getChangePriceTime()) + " " + stadium.getPriceDayTime() + BotAnswerString.MANY + System.lineSeparator() : language.equals(
                    BotAnswerString.uzl) ? getParsDade(stadium.getChangePriceTime()) + " " + BotAnswerString.PRICE_BEFORE_UZL + " " + stadium.getPriceDayTime() + BotAnswerString.MANY + System.lineSeparator() : BotAnswerString.PRICE_BEFORE_EN + getParsDade(
                    stadium.getChangePriceTime()) + " " + stadium.getPriceDayTime() + BotAnswerString.MANY + System.lineSeparator();
        }


        return distance.concat(size)
                .concat(phone)
                .concat(address)
                .concat(like)
                .concat(openTime)
                .concat(price)
                .concat(timePriceAfter)
                .concat(timePriceBefore)
                .concat(System.lineSeparator())
                .concat(name)
                .concat(BotAnswerString.GET_NEW_LINE_STICKER);
    }

    /*
    income 09:30
    return 570
     */
    public Date getDate(String date) {
        if (date.length() == 5 && (date.contains(":") || date.contains(" ") || date.contains("-") || date.contains(
                "."))) {
            int clock;
            int minute;
            if (date.startsWith("0")) {
                clock = Integer.parseInt(date.substring(1, 2));
                if (date.substring(3).startsWith("0")) {
                    minute = Integer.parseInt(date.substring(4));
                } else {
                    minute = Integer.parseInt(date.substring(3));
                }
            } else {
                clock = Integer.parseInt(date.substring(0, 2));
                if (date.substring(3).startsWith("0")) {
                    minute = Integer.parseInt(date.substring(4));
                } else {
                    minute = Integer.parseInt(date.substring(3));
                }
            }
            if (clock >= 0 && clock < 25 && minute >= 0 && minute < 60) {
                Calendar calendar = orderMoreWorking.getDate();
                calendar.set(Calendar.HOUR_OF_DAY, clock);
                calendar.set(Calendar.MINUTE, minute);
                return calendar.getTime();
            }
        }
        return null;
    }

    public Date getDateTime(String date) {
        if (date.length() == 5 && (date.contains(":") || date.contains(" ") || date.contains("-") || date.contains(
                "."))) {
            int clock;
            int minute;
            if (date.startsWith("0")) {
                clock = Integer.parseInt(date.substring(1, 2));
                if (date.substring(3).startsWith("0")) {
                    minute = Integer.parseInt(date.substring(4));
                } else {
                    minute = Integer.parseInt(date.substring(3));
                }
            } else {
                clock = Integer.parseInt(date.substring(0, 2));
                if (date.substring(3).startsWith("0")) {
                    minute = Integer.parseInt(date.substring(4));
                } else {
                    minute = Integer.parseInt(date.substring(3));
                }
            }
            if (clock >= 0 && clock < 25 && minute >= 0 && minute < 60) {
                Date s = orderMoreWorking.getDate().getTime();
                s.setHours(workingALot.getDateClock(date));
                s.setMinutes(workingALot.getDateMinute(date));
                return s;
            }
        }
        return null;
    }

    public Integer getDateClock(String date) {
        if (date.length() == 5 && (date.contains(":") || date.contains(" ") || date.contains("-") || date.contains(
                "."))) {
            int clock;
            if (date.startsWith("0")) {
                clock = Integer.parseInt(date.substring(1, 2));
            } else {
                clock = Integer.parseInt(date.substring(0, 2));
            }
            if (clock >= 0 && clock < 25) {
                return clock;
            }
        }
        return null;
    }

    public Integer getDateMinute(String date) {
        if (date.length() == 5 && (date.contains(":") || date.contains(" ") || date.contains("-") || date.contains(
                "."))) {
            int minute = 0;
            if (date.startsWith("0")) {
                if (date.substring(3).startsWith("0")) {
                    minute = Integer.parseInt(date.substring(4));
                } else {
                    minute = Integer.parseInt(date.substring(3));
                }
            }
            if (minute >= 0 && minute < 60) {
                return minute;
            }
        }
        return null;
    }

    /*
    income 09:30
    return 570
     */
    public Integer getHeightStadium(String date) {
        if (date.contains("X") || date.contains("x") || date.contains("Х") || date.contains("х") || date.contains(
                "×")) {
            try {
                if (date.contains("X")) {
                    return Integer.parseInt(date.substring(0, date.indexOf('X')));
                } else if (date.contains("x")) {
                    return Integer.parseInt(date.substring(0, date.indexOf('x')));
                } else if (date.contains("Х")) {
                    return Integer.parseInt(date.substring(0, date.indexOf('Х')));
                } else if (date.contains("х")) {
                    return Integer.parseInt(date.substring(0, date.indexOf('х')));
                } else if (date.contains("×")) {
                    return Integer.parseInt(date.substring(0, date.indexOf('×')));
                }
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }

    /*
    income 09:30
    return 570
     */
    public Integer getWidthStadium(String date) {
        if (date.contains("X") || date.contains("x") || date.contains("Х") || date.contains("×") || date.contains(
                "х")) {
            try {
                if (date.contains("X")) {
                    return Integer.parseInt(date.substring(date.indexOf('X') + 1));
                } else if (date.contains("x")) {
                    return Integer.parseInt(date.substring(date.indexOf('x') + 1));
                } else if (date.contains("Х")) {
                    return Integer.parseInt(date.substring(date.indexOf('Х') + 1));
                } else if (date.contains("х")) {
                    return Integer.parseInt(date.substring(date.indexOf('х') + 1));
                } else if (date.contains("×")) {
                    return Integer.parseInt(date.substring(date.indexOf('×') + 1));
                }
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }

    public String getParsDade(Date date) {
        try {
            Calendar calendar = orderMoreWorking.getDate();
            calendar.setTime(date);
            Integer clock = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);
            if (clock < 10) {
                if (minute < 10) {
                    return "0" + clock + ":" + "0" + minute;
                } else {
                    return "0" + clock + ":" + minute;
                }

            } else {
                if (minute < 10) {
                    return clock + ":" + "0" + minute;
                } else {
                    return clock + ":" + minute;
                }
            }
        } catch (Exception e) {
            return null;
        }
    }

    /*
       income String 12000
       return Integer 12000
    */
    public Integer getPrice(String price) {
        try {
            return Integer.parseInt(price.replace(" ", ""));
        } catch (Exception e) {
            return null;
        }
    }

    public List<StadiumDistance> getNearStadium(double longitude, double latitude) {
        List<Map<String, Object>> maps = jdbcTemplate.queryForList("select * from stadium s where s.active=true");
        List<StadiumDistance> distances = new ArrayList<>();
        for (Map<String, Object> map : maps) {
            distances.add(new StadiumDistance((long) map.get("id"),
                    distance(longitude, latitude, (double) map.get("longitude"), (double) map.get("latitude"), 0, 0),
                    (String) map.get("address"),
                    (String) map.get("name"),
                    (String) map.get("phone_number"),
                    (Date) map.get("opening_time"),
                    (Date) map.get("closing_time"),
                    (Integer) map.get("stadium_like"),
                    (Date) map.get("change_price_time"),
                    (Double) map.get("price_day_time"),
                    (Double) map.get("price_night_time"),
                    (Integer) map.get("width"),
                    (Integer) map.get("height"),
                    (Double) map.get("count_order"),
                    (Double) map.get("latitude"),
                    (Double) map.get("longitude"),
                    (Boolean) map.get("roof"),
                    (Double) map.get("money"),
                    (String) map.get("description")));
        }
        distances.sort((a, b) -> a.getDistance() < b.getDistance() ? -1 : 1);
        return distances;
    }

    public void generateListStadium(double longitude, double latitude, int prev, int next, String language, long chat_id) {

        try {
            List<StadiumDistance> nearStadium = getNearStadium(longitude, latitude);
            String text = "";
            List<String> listName = new ArrayList<>();
            if (nearStadium.size() >= next) {
                for (int i = prev; i < next; i++) {
                    listName.add(nearStadium.get(i).getName());
                    text = text.concat(getInfoStadiumForClient(nearStadium.get(i), language));
                }
                BRBTBot.execute(inlineClientList(chat_id, listName, text, prev, next));
            }
        } catch (Exception e) {
            exceptionSend.senException("generation stadium near => ", e, null);
            log.error("generation stadium near => ", e);
        }

    }

    public void generateListStadiumEdit(double longitude, double latitude, int prev, int next, String language,
                                        Integer msgId, long chat_id) {
        try {
            List<StadiumDistance> nearStadium = getNearStadium(longitude, latitude);
            String text = "";
            List<String> listName = new ArrayList<>();
            for (int i = prev; i < next; i++) {
                if (i < nearStadium.size()) {
                    listName.add(nearStadium.get(i).getName());
                    text = text.concat(getInfoStadiumForClient(nearStadium.get(i), language));
                }
            }
            if (!text.isEmpty()) {
                BRBTBot.execute(inlineClientListEdit(chat_id,
                        listName,
                        text,
                        prev,
                        next,
                        msgId,
                        (nearStadium.size() - next)));
            }
        } catch (Exception e) {
            exceptionSend.senException("generation list stadium edit  => ", e, null);
            log.error("generation list stadium edit  => ", e);
        }

    }

    public PhotoSize getPhoto(Update update) {
        // Check that the update contains a message and the message has a photo
        if (update.hasMessage() && update.getMessage().hasPhoto()) {
            // When receiving a photo, you usually get different sizes of it
            List<PhotoSize> photos = update.getMessage().getPhoto();

            // We fetch the bigger photo
            return photos.stream().max(Comparator.comparing(PhotoSize::getFileSize)).orElse(null);
        }

        // Return null if not found
        return null;
    }

    public String getFilePath(PhotoSize photo) {
        Objects.requireNonNull(photo);

        if (photo.hasFilePath()) { // If the file_path is already present, we are done!
            return photo.getFilePath();
        } else { // If not, let find it
            // We create a GetFile method and set the file_id from the photo
            GetFile getFileMethod = new GetFile();
            getFileMethod.setFileId(photo.getFileId());
            try {
                // We execute the method using AbsSender::execute method.
                File file = BRBTBot.execute(getFileMethod);
                // We now have the file_path
                return file.getFilePath();
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }

        return null; // Just in case
    }

    public String getExtension(PhotoSize photo) {
        Objects.requireNonNull(photo);

        if (photo.hasFilePath()) { // If the file_path is already present, we are done!
            return photo.getFilePath();
        } else { // If not, let find it
            // We create a GetFile method and set the file_id from the photo
            GetFile getFileMethod = new GetFile();
            getFileMethod.setFileId(photo.getFileId());
            try {
                // We execute the method using AbsSender::execute method.
                File file = BRBTBot.execute(getFileMethod);
                // We now have the file_path
                return getExt(file.getFilePath());
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }

        return null; // Just in case
    }

    public String getExt(String fileName) {
        String ext = null;
        if (fileName != null && !fileName.isEmpty()) {
            int dot = fileName.lastIndexOf(".");
            if (dot > 0 && dot <= fileName.length() - 2) {
                ext = fileName.substring(dot);
            }
        }
        return ext;
    }

    public java.io.File downloadPhoto(Update update) {
        try {
            // Download the file calling AbsSender::downloadFile method
            return BRBTBot.downloadFile(getFilePath(getPhoto(update)));
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void savePhoto(Update update, Stadium stadium, User user) throws IOException {
        try {
            StadiumPhoto stadiumPhoto = new StadiumPhoto();
            PhotoSize photoSize = getPhoto(update);
            stadiumPhoto.setStadium(stadium);
            stadiumPhoto.setUser(user);
            stadiumPhoto.setName(photoSize.getFileUniqueId());
            stadiumPhoto.setSize(photoSize.getFileSize());
            stadiumPhoto.setWhy(AttachmentTypeEnumWhy.STADIUM_PHOTO);
            stadiumPhoto.setPhoto(photoSize.getFileId());
            java.io.File file = workingALot.downloadPhoto(update);
            stadiumPhoto.setContentType(file.toURL().openConnection().getContentType());
            stadiumPhoto.setExtension(getExtension(getPhoto(update)));
            saveOnFolder(file, stadiumPhotoRepository.save(stadiumPhoto), stadium);
        } catch (Exception e) {
            exceptionSend.senException("save photo stadium => ", e, null);
            log.error("save photo stadium => ", e);
        }
    }

    public void saveOnFolder(java.io.File file, StadiumPhoto save, Stadium stadium) {
        java.io.File uploadFolder = new java.io.File(path + "/" + orderMoreWorking.getDate()
                .get(Calendar.YEAR) + "/" + orderMoreWorking.getDate()
                .get(Calendar.MONTH) + "/" + orderMoreWorking.getDate().get(Calendar.DAY_OF_MONTH));

        if (uploadFolder.mkdirs() && uploadFolder.exists()) {
            System.out.println("papkalar yaratildi  " + uploadFolder.getAbsolutePath());
        }

        uploadFolder = uploadFolder.getAbsoluteFile();
        java.io.File createFile = new java.io.File(uploadFolder + "/" + save.getId() + "_" + stadium.getName() + save.getExtension());
        save.setPath(createFile.getAbsolutePath());
        try {
            FileChannel src = new FileInputStream(file).getChannel();
            FileChannel dest = new FileOutputStream(createFile).getChannel();
            dest.transferFrom(src, 0, src.size());
            dest.close();
            src.close();
            stadiumPhotoRepository.save(save);
        } catch (Exception e) {
            exceptionSend.senException("save on folder => ", e, null);
            log.error("save on folder => ", e);
        }
    }

    public void sendStadiumPhoto(Stadium stadium, long chat_id) {
        try {
            SendChatAction sendChatAction = new SendChatAction();
            sendChatAction.setChatId(chat_id);
            sendChatAction.setAction(ActionType.UPLOADDOCUMENT);
            BRBTBot.execute(sendChatAction);
            List<StadiumPhoto> stadiumPhotos = stadiumPhotoRepository.findByStadiumIdAndWhyOrderByCreatedAtDesc(stadium.getId(),
                    AttachmentTypeEnumWhy.STADIUM_PHOTO);
            SendMediaGroup sendMediaGroup = new SendMediaGroup();
            sendMediaGroup.setChatId(chat_id);
            List<InputMedia> inputMediaList = new ArrayList<>();
            for (StadiumPhoto stadiumPhoto : stadiumPhotos) {
                InputMediaPhoto inputMediaPhoto = new InputMediaPhoto();
                if (stadiumPhoto.getPhoto() == null) {
                    String s = sendImageFromUrl(stadiumPhoto.getPath(),
                            String.valueOf(stadium.getUser().getTelegramId()));
                    if (s == null) {
                        System.err.println("");
                        continue;
                    }
                    stadiumPhoto.setPhoto(s);
                    stadiumPhotoRepository.save(stadiumPhoto);
                    inputMediaPhoto.setMedia(s);
                } else {
                    inputMediaPhoto.setMedia(stadiumPhoto.getPhoto());
                }
                inputMediaList.add(inputMediaPhoto);
                //            inputMediaPhoto.setCaption(inputMediaList.size()==1?"":answer);
            }
            if (stadiumPhotos.size() > 0) {
                sendMediaGroup.setMedia(inputMediaList);
                BRBTBot.execute(sendMediaGroup);
            }
        } catch (Exception e) {
            exceptionSend.senException("send stadium photo => ", e, null);
            log.error("send stadium photo => ", e);
        }

    }

    public String sendImageFromUrl(String filePath, String chatId) {
        // Create send method
        SendPhoto sendPhotoRequest = new SendPhoto();
        // Set destination chat id
        sendPhotoRequest.setChatId(chatId);
        // Set the photo file as a new photo (You can also use InputStream with a constructor overload)
        java.io.File file = new java.io.File(filePath);
        sendPhotoRequest.setPhoto(file);
        sendPhotoRequest.setCaption(BotAnswerString.upload_photo);
        try {
            // Execute the method
            return getPhotoTelegram(BRBTBot.execute(sendPhotoRequest)).getFileId();
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
        return null;
    }

    public PhotoSize getPhotoTelegram(Message message) {
        // Check that the update contains a message and the message has a photo
        if (message.hasPhoto()) {
            // When receiving a photo, you usually get different sizes of it
            List<PhotoSize> photos = message.getPhoto();

            // We fetch the bigger photo
            return photos.stream().max(Comparator.comparing(PhotoSize::getFileSize)).orElse(null);
        }

        // Return null if not found
        return null;
    }

    public RestMyStadium generation(Stadium stadium) {
        return new RestMyStadium(stadium.getId(),
                stadium.getName(),
                0,
                0,
                stadium.getLatitude(),
                stadium.getPhone_number(),
                stadium.getLongitude(),
                stadium.getAddress(),
                workingALot.getParsDade(stadium.getOpening_time()),
                workingALot.getParsDade(stadium.getClosing_time()),
                stadium.getStadium_like(),
                workingALot.getParsDade(stadium.getChange_price_time()),
                stadium.getPrice_day_time(),
                stadium.getPrice_night_time(),
                stadium.getWidth(),
                stadium.getHeight(),
                stadium.getCount_order(),
                stadium.isOpen(),
                stadium.isActive(),
                stadium.isVerify(),
                stadium.getMoney(),
                stadium.isRoof());
    }

    public void telegramMessageUser(long chat_id, double sumPrice, String language, OrdersActive ordersActive) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

            String text = (language.equals(BotAnswerString.uz) ? BotAnswerString.ORDER_NUMBER_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.ORDER_NUMBER_RU : language.equals(
                    BotAnswerString.uzl) ? BotAnswerString.ORDER_NUMBER_UZL : BotAnswerString.ORDER_NUMBER_EN) + ": " + ordersActive.getId() + System.lineSeparator()
                    + System.lineSeparator()
                    +
                    (language.equals(BotAnswerString.uz) ? BotAnswerString.PRICE_UZ : language.equals(
                            BotAnswerString.ru) ? BotAnswerString.PRICE_RU : language.equals(
                            BotAnswerString.uzl) ? BotAnswerString.PRICE_UZL : BotAnswerString.PRICE_EN) + ": " + sumPrice + System.lineSeparator() +
                    "✅" + (language.equals(
                    BotAnswerString.uz) ? BotAnswerString.ORDER_INSPECTION_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.ORDER_INSPECTION_RU : language.equals(
                    BotAnswerString.uzl) ? BotAnswerString.ORDER_INSPECTION_UZL : BotAnswerString.ORDER_INSPECTION_EN) + System.lineSeparator()
                    + (language.equals(
                    BotAnswerString.uz) ? BotAnswerString.ORDER_CONNECT_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.ORDER_CONNECT_RU : language.equals(
                    BotAnswerString.uzl) ? BotAnswerString.ORDER_CONNECT_UZL : BotAnswerString.ORDER_CONNECT_EN) + System.lineSeparator() + System.lineSeparator() +
                    (language.equals(BotAnswerString.uz) ? BotAnswerString.ORDER_CANCEL_FOR_USER_UZ : language.equals(
                            BotAnswerString.ru) ? BotAnswerString.ORDER_CANCEL_FOR_USER_RU : language.equals(
                            BotAnswerString.uzl) ? BotAnswerString.ORDER_CANCEL_FOR_USER_UZL : BotAnswerString.ORDER_CANCEL_FOR_USER_EN);


            SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                    .setText(text);
            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> listListRows = new ArrayList<>();
            List<InlineKeyboardButton> inlineKeyboardButton = new ArrayList<>();
            inlineKeyboardButton.add(new InlineKeyboardButton().setText(BotAnswerString.cancelOrderBTN)
                    .setCallbackData(BotAnswerString.cancelOrder + ordersActive.getId()));
            listListRows.add(inlineKeyboardButton);

            inlineKeyboardMarkup.setKeyboard(listListRows);
            sendMessage.setReplyMarkup(inlineKeyboardMarkup);
            BRBTBot.execute(sendMessage);
//            SendMessage sendMessage1 = new SendMessage().setChatId(chat_id)
//                    .setText(ordersActive.getStadium().getName() + System.lineSeparator() + formatter.format(
//                            ordersActive.getTime()) + "  ➡️      " + workingALot.getParsDade(ordersActive.getStartDate()) + " - " + workingALot.getParsDade(
//                            ordersActive.getEndDate()) + System.lineSeparator() + System.lineSeparator() + sumPrice + "  " + BotAnswerString.MANY + " " + System.lineSeparator() + (language.equals(
//                            BotAnswerString.uz) ? BotAnswerString.ORDER_INSPECTION_UZ : language.equals(
//                            BotAnswerString.ru) ? BotAnswerString.ORDER_INSPECTION_RU : language.equals(
//                            BotAnswerString.uzl) ? BotAnswerString.ORDER_INSPECTION_UZL : BotAnswerString.ORDER_INSPECTION_EN));
//            BRBTBot.execute(sendMessage1);
        } catch (Exception e) {
            exceptionSend.senException(" controller create order => ", e, ordersActive.getUser());
            log.error(" controller create order => ", e);
        }

    }

    public void saveIpAddress(User user, String ip) {
        try {
            if (!ipAddressRepository.existsByIpAndUserId(ip, user.getId())) {
                ipAddressRepository.save(new IpAddress(ip, true, user));
            }
            if (user.getIpAddress() != null && !user.getIpAddress().equals(ip)) {
                user.setIpAddress(ip);
                userRepository.save(user);
            }
        } catch (Exception e) {
            exceptionSend.senException(" saveIpAddress => ", e, user);
            log.error(" saveIpAddress => ", e);
        }
    }

    public double countOrder(User user, Stadium stadium) {
        try {
            return orderArchiveRepository.countAllByUserAndStadium(user, stadium);
        } catch (Exception e) {
            exceptionSend.senException(" countOrder for user => ", e, user);
            log.error(" countOrder for user => ", e);
            return 0;
        }
    }
}
