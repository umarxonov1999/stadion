package koinot.uz.backend.service;

import koinot.uz.backend.bot.BRBT_Bot;
import koinot.uz.backend.entity.OrdersActive;
import koinot.uz.backend.entity.OrdersBlack;
import koinot.uz.backend.entity.Stadium;
import koinot.uz.backend.entity.User;
import koinot.uz.backend.exception.ExceptionSend;
import koinot.uz.backend.payload.ApiResponseModel;
import koinot.uz.backend.payload.ReqAdminOrder;
import koinot.uz.backend.payload.ReqOrder;
import koinot.uz.backend.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

@Service
@Slf4j
public class OrderService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    BRBT_Bot BRBTBot;

    @Autowired
    koinot.uz.backend.service.WorkingALot workingALot;

    @Autowired
    koinot.uz.backend.service.OrderMoreWorking orderMoreWorking;

    @Autowired
    StadiumPhotoRepository stadiumPhotoRepository;

    @Autowired
    StadiumRepository stadiumRepository;

    @Autowired
    OrderActiveRepository activeRepository;

    @Autowired
    OrderArchiveRepository archiveRepository;

    @Autowired
    OrderBlackRepository blackRepository;

    @Autowired
    OrderActiveRepository orderActiveRepository;

    @Autowired
    ExceptionSend exceptionSend;

    @Autowired
    IpAddressRepository ipAddressRepository;


    public void getOrderStadium(Stadium stadium, User user, Update update, String language) {
        try {
            String text = update.getCallbackQuery().getMessage().getText();
            String order = System.lineSeparator();
            for (OrdersActive ordersActive : activeRepository.findAllByActiveAndStadiumId(true, stadium.getId())) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                formatter.setTimeZone(TimeZone.getTimeZone("Asia/Tashkent"));

                Date date = ordersActive.getTime();

                order = order + formatter.format(date) + "  ➡️      " + workingALot.getParsDade(ordersActive.getStartDate()) + " - " + workingALot.getParsDade(
                        ordersActive.getEndDate()) + System.lineSeparator();
            }
            text = text.concat(order);
            if (!(order.length() > 3)) {
                text = text.concat(BotAnswerString.NOT_FOUND);
            }

            orderMoreWorking.editInline(Long.valueOf(user.getTelegramId()),
                    update.getCallbackQuery().getMessage().getMessageId(),
                    stadium,
                    language,
                    text);
        } catch (Exception e) {
            exceptionSend.senException("change price nightlight => ", e, user);
            log.error("change price nightlight => ", e);
        }

    }

    public void getOrderDate(Update update, User user, String language, Stadium stadium) {
        try {
            String text = update.getCallbackQuery().getMessage().getText();
            EditMessageText sendMessage = new EditMessageText().setChatId(String.valueOf(user.getTelegramId()))
                    .setText(text)
                    .setMessageId(update.getCallbackQuery().getMessage().getMessageId());
            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> listListRows = new ArrayList<>();
            List<InlineKeyboardButton> inlineKeyboardButton = new ArrayList<>();
            inlineKeyboardButton.add(new InlineKeyboardButton().setText(BotAnswerString.CANCEL)
                    .setCallbackData(BotAnswerString.cancel_client + stadium.getName()));

            List<InlineKeyboardButton> info = new ArrayList<>();
            Calendar calendar = orderMoreWorking.getDate();

            Calendar calendar1 = orderMoreWorking.getDate();
            calendar1.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1);
            DateFormat df = new SimpleDateFormat("MMMMMMMMMM",
                    (language.equals(BotAnswerString.uz) ? Locale.forLanguageTag("uz") : language.equals(
                            BotAnswerString.ru) ? Locale.forLanguageTag("ru") : language.equals(BotAnswerString.uzl) ? Locale.forLanguageTag(
                            "uz") : Locale.forLanguageTag("en")));
            df.setTimeZone(TimeZone.getTimeZone("Asia/Tashkent"));

            String k = !((calendar.getMaximum(Calendar.DAY_OF_MONTH) - calendar.get(Calendar.DAY_OF_MONTH) - 8) >= 0) ? df.format(
                    calendar1.getTime()) : "";

            info.add(new InlineKeyboardButton().setText(df.format(calendar.getTime()) + " " + k + System.lineSeparator() + (language.equals(
                            BotAnswerString.uz) ? BotAnswerString.SELECT_DAY_UZ : language.equals(BotAnswerString.ru) ? BotAnswerString.SELECT_DAY_RU : language.equals(
                            BotAnswerString.uzl) ? BotAnswerString.SELECT_DAY_UZL : BotAnswerString.SELECT_DAY_EN))
                    .setCallbackData("Qudratjon"));
            List<InlineKeyboardButton> inlineKeyboardButton1 = new ArrayList<>();
            List<InlineKeyboardButton> inlineKeyboardButton2 = new ArrayList<>();
            calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) - 1);

            for (int i = -1; i < 7; i++) {
                int n = calendar.get(Calendar.DAY_OF_MONTH);
                calendar.set(Calendar.DAY_OF_MONTH, n + 1);
                if (i < 3) {
                    inlineKeyboardButton1.add(new InlineKeyboardButton().setText(String.valueOf(calendar.get(
                                    Calendar.DAY_OF_MONTH)))
                            .setCallbackData(BotAnswerString.get_time + calendar.get(Calendar.DAY_OF_MONTH) + BotAnswerString.get_month + calendar.get(
                                    Calendar.MONTH) + BotAnswerString.get_stadium + stadium.getName()));
                } else {
                    inlineKeyboardButton2.add(new InlineKeyboardButton().setText(String.valueOf(calendar.get(
                                    Calendar.DAY_OF_MONTH)))
                            .setCallbackData(BotAnswerString.get_time + calendar.get(Calendar.DAY_OF_MONTH) + BotAnswerString.get_month + calendar.get(
                                    Calendar.MONTH) + BotAnswerString.get_stadium + stadium.getName()));
                }
            }
            listListRows.add(info);
            listListRows.add(inlineKeyboardButton1);
            listListRows.add(inlineKeyboardButton2);
            listListRows.add(inlineKeyboardButton);
            inlineKeyboardMarkup.setKeyboard(listListRows);
            sendMessage.setReplyMarkup(inlineKeyboardMarkup);
            BRBTBot.execute(sendMessage);
        } catch (Exception e) {
            exceptionSend.senException("get order info buys or free => ", e, user);
            log.error("get order info buys or free => ", e);
        }

    }

    public void createOrder(Stadium stadium, User user, Update update, String language, int day, int month) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            formatter.setTimeZone(TimeZone.getTimeZone("Asia/Tashkent"));
            long chat_id = user.getTelegramId();
            Calendar calendar = orderMoreWorking.getDate();
            calendar.set(Calendar.DAY_OF_MONTH, day);
            calendar.set(Calendar.MONTH, month);
            Optional<OrdersBlack> allByUser = blackRepository.findAllByUser(user);
            if (!allByUser.isPresent()) {
                blackRepository.save(new OrdersBlack(user,
                        stadium,
                        user.getLatitude(),
                        user.getLongitude(),
                        0,
                        null,
                        null,
                        calendar.getTime(),
                        false,
                        false));

            } else {
                blackRepository.delete(allByUser.get());
                blackRepository.save(new OrdersBlack(user,
                        stadium,
                        user.getLatitude(),
                        user.getLongitude(),
                        0,
                        null,
                        null,
                        calendar.getTime(),
                        false,
                        false));
            }
            SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                    .setReplyToMessageId(update.getCallbackQuery().getMessage().getMessageId())
                    .setText((language.equals(
                            BotAnswerString.uz) ? BotAnswerString.NAME_UZ : language.equals(BotAnswerString.ru) ? BotAnswerString.NAME_UZ : language.equals(
                            BotAnswerString.uzl) ? BotAnswerString.NAME_UZL : BotAnswerString.NAME_EN) + ": " + stadium.getName() + System.lineSeparator() +

                            (language.equals(
                                    BotAnswerString.uz) ? BotAnswerString.DAY_UZ : language.equals(BotAnswerString.ru) ? BotAnswerString.DAY_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.DAY_UZL : BotAnswerString.DAY_EN) + ": " + orderMoreWorking.getWeek(calendar.get(
                                    Calendar.DAY_OF_WEEK),
                            language) + "  " + formatter.format(calendar.getTime()) + System.lineSeparator() + "❓" + (language.equals(
                            BotAnswerString.uz) ? BotAnswerString.WHAT_TIME_TO_GO_UZ : language.equals(BotAnswerString.ru) ? BotAnswerString.WHAT_TIME_TO_GO_RU : language.equals(
                            BotAnswerString.uzl) ? BotAnswerString.WHAT_TIME_TO_GO_UZL : BotAnswerString.WHAT_TIME_TO_GO_EN));
            BRBTBot.execute(sendMessage);
        } catch (Exception e) {
            exceptionSend.senException("client create order with telegram bot => ", e, user);
            log.error("client create order with telegram bot => ", e);
        }

    }

    public void getOrderClock(Update update, User user, String language) {
        try {
            long chat_id = user.getTelegramId();
            String text = update.getMessage().getText();
            Optional<OrdersBlack> allByUser = blackRepository.findAllByUser(user);
            if (allByUser.isPresent()) {
                OrdersBlack ordersBlack = allByUser.get();
                if (workingALot.getDate(text) != null) {
                    List<OrdersActive> oa = new ArrayList<>();
                    SimpleDateFormat h = new SimpleDateFormat("yyyy-MM-dd");
                    h.setTimeZone(TimeZone.getTimeZone("Asia/Tashkent"));

                    for (OrdersActive ordersActive : activeRepository.findAllByActiveAndStadiumId(true,
                            ordersBlack.getStadium().getId())) {
                        if (h.format(ordersActive.getTime()).equals(h.format(ordersBlack.getTime()))) {
                            oa.add(ordersActive);
                        }
                    }

                    if (ordersBlack.getStartDate() == null) {
                        Calendar s = orderMoreWorking.getDate();
                        Calendar c = orderMoreWorking.getDate();
                        s.setTime(ordersBlack.getTime());
                        c.setTime(workingALot.getDate(text));
                        s.set(Calendar.HOUR_OF_DAY, c.get(Calendar.HOUR_OF_DAY));
                        s.set(Calendar.MINUTE, c.get(Calendar.MINUTE));

                        if (orderMoreWorking.startFreeTime(oa, s.getTime(), language, chat_id)) {
                            ordersBlack.setStartDate(s.getTime());
                            SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                                    .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.WHAT_TIME_PLAY_UZ : language.equals(
                                            BotAnswerString.ru) ? BotAnswerString.WHAT_TIME_PLAY_RU : language.equals(
                                            BotAnswerString.uzl) ? BotAnswerString.WHAT_TIME_PLAY_UZL : BotAnswerString.WHAT_TIME_PLAY_EN);
                            BRBTBot.execute(sendMessage);
                            blackRepository.save(ordersBlack);
                        }
                    } else {
                        if (orderMoreWorking.endDateFree(oa,
                                ordersBlack.getStartDate(),
                                workingALot.getDate(text),
                                language,
                                chat_id)) {
                            Calendar s = orderMoreWorking.getDate();
                            Calendar c = orderMoreWorking.getDate();
                            s.setTime(ordersBlack.getTime());
                            c.setTime(workingALot.getDate(text));
                            s.set(Calendar.HOUR_OF_DAY, c.get(Calendar.HOUR_OF_DAY));
                            s.set(Calendar.MINUTE, c.get(Calendar.MINUTE));
                            double sumPrice = orderMoreWorking.getPriceStadium(ordersBlack.getStadium(),
                                    ordersBlack.getStartDate(),
                                    s.getTime());
                            OrdersActive save = activeRepository.save(new OrdersActive(ordersBlack.getUser(),
                                    ordersBlack.getStadium(),
                                    ordersBlack.getLatitude(),
                                    ordersBlack.getLongitude(),
                                    sumPrice,
                                    ordersBlack.getStartDate(),
                                    s.getTime(),
                                    ordersBlack.getTime(),
                                    false,
                                    false,
                                    workingALot.countOrder(ordersBlack.getUser(), ordersBlack.getStadium())));

                            String text1=(language.equals(BotAnswerString.uz) ? BotAnswerString.ORDER_NUMBER_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.ORDER_NUMBER_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.ORDER_NUMBER_UZL : BotAnswerString.ORDER_NUMBER_EN)+": "+save.getId()+ System.lineSeparator()
                                    +  System.lineSeparator()
                                    +
                                    (language.equals(BotAnswerString.uz) ? BotAnswerString.PRICE_UZ : language.equals(
                                            BotAnswerString.ru) ? BotAnswerString.PRICE_RU : language.equals(
                                            BotAnswerString.uzl) ? BotAnswerString.PRICE_UZL : BotAnswerString.PRICE_EN) +": "+sumPrice+ System.lineSeparator()+
                                    "✅"+(language.equals(
                                    BotAnswerString.uz) ? BotAnswerString.ORDER_INSPECTION_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.ORDER_INSPECTION_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.ORDER_INSPECTION_UZL : BotAnswerString.ORDER_INSPECTION_EN)+System.lineSeparator()
                                    +(language.equals(
                                    BotAnswerString.uz) ? BotAnswerString.ORDER_CONNECT_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.ORDER_CONNECT_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.ORDER_CONNECT_UZL : BotAnswerString.ORDER_CONNECT_EN)+System.lineSeparator()+System.lineSeparator()+
                                    (language.equals(BotAnswerString.uz) ? BotAnswerString.ORDER_CANCEL_FOR_USER_UZ : language.equals(
                                            BotAnswerString.ru) ? BotAnswerString.ORDER_CANCEL_FOR_USER_RU : language.equals(
                                            BotAnswerString.uzl) ? BotAnswerString.ORDER_CANCEL_FOR_USER_UZL : BotAnswerString.ORDER_CANCEL_FOR_USER_EN);


                            SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                                    .setText(text1);
                            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
                            List<List<InlineKeyboardButton>> listListRows = new ArrayList<>();
                            List<InlineKeyboardButton> inlineKeyboardButton = new ArrayList<>();
                            inlineKeyboardButton.add(new InlineKeyboardButton().setText(BotAnswerString.cancelOrderBTN)
                                    .setCallbackData(BotAnswerString.cancelOrder + save.getId()));
                            listListRows.add(inlineKeyboardButton);

                            inlineKeyboardMarkup.setKeyboard(listListRows);
                            sendMessage.setReplyMarkup(inlineKeyboardMarkup);
                            BRBTBot.execute(sendMessage);

//                            orderMoreWorking.getRegionBtn(chat_id,
//                                    language,
//                                    sumPrice + "  " + BotAnswerString.MANY + " " + System.lineSeparator() + (language.equals(
//                                            BotAnswerString.uz) ? BotAnswerString.ORDER_INSPECTION_UZ : language.equals(
//                                            BotAnswerString.ru) ? BotAnswerString.ORDER_INSPECTION_RU : language.equals(
//                                            BotAnswerString.uzl) ? BotAnswerString.ORDER_INSPECTION_UZL : BotAnswerString.ORDER_INSPECTION_EN));

                            orderMoreWorking.stadiumBossAlert(ordersBlack.getStadium(), save);
                            blackRepository.deleteById(ordersBlack.getId());
                        }
                    }

                } else {
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.DATE_FORMAT_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.DATE_FORMAT_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.DATE_FORMAT_UZL : BotAnswerString.DATE_FORMAT_EN);
                    BRBTBot.execute(sendMessage);
                }
            }
        } catch (Exception e) {
            exceptionSend.senException("get order clock => ", e, user);
            log.error("get order clock => ", e);
        }

    }

    public void verifyOrder(OrdersActive order, Update update, User user) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            formatter.setTimeZone(TimeZone.getTimeZone("Asia/Tashkent"));

            Date date = order.getTime();
            order.setActive(true);
            OrdersActive save = activeRepository.save(order);
            orderMoreWorking.editOrderBoss(user, order, update, BotAnswerString.SUCCESSFUL);

            if (save.getUser().getTelegramId() != null) {

                String language = order.getUser().getLanguage();
                long chat_id = order.getUser().getTelegramId();

                SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                        .setText(order.getStadium()
                                .getName() + System.lineSeparator() + formatter.format(date) + "  ➡️      " + workingALot.getParsDade(
                                order.getStartDate()) + " - " + workingALot.getParsDade(order.getEndDate()) + System.lineSeparator() + System.lineSeparator() + (language.equals(
                                BotAnswerString.uz) ? BotAnswerString.ORDER_ACTIVE_UZ : language.equals(
                                BotAnswerString.ru) ? BotAnswerString.ORDER_ACTIVE_RU : language.equals(
                                BotAnswerString.uzl) ? BotAnswerString.ORDER_ACTIVE_UZL : BotAnswerString.ORDER_ACTIVE_EN));
                BRBTBot.execute(sendMessage);
            }
        } catch (Exception e) {
            exceptionSend.senException("verify order => ", e, user);
            log.error("verify order => ", e);
        }

    }

    public void cancelOrder(OrdersActive order, Update update, User user) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            formatter.setTimeZone(TimeZone.getTimeZone("Asia/Tashkent"));

            Date date = order.getTime();
            order.setActive(false);
            order.setCancelOrder(true);
            OrdersActive save = activeRepository.save(order);
            orderMoreWorking.editOrderBoss(user, order, update, BotAnswerString.CANCEL);

            if (save.getUser().getTelegramId() != null) {

                String language = order.getUser().getLanguage();
                long chat_id = order.getUser().getTelegramId();
                SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                        .setText(order.getStadium()
                                .getName() + System.lineSeparator() + formatter.format(date) + "  ➡️      " + workingALot.getParsDade(
                                order.getStartDate()) + " - " + workingALot.getParsDade(order.getEndDate()) + System.lineSeparator() + System.lineSeparator() + (language.equals(
                                BotAnswerString.uz) ? BotAnswerString.ORDER_CANCEL_UZ : language.equals(
                                BotAnswerString.ru) ? BotAnswerString.ORDER_CANCEL_RU : language.equals(
                                BotAnswerString.uzl) ? BotAnswerString.ORDER_CANCEL_UZL : BotAnswerString.ORDER_CANCEL_EN));
                BRBTBot.execute(sendMessage);
            }
        } catch (Exception e) {
            exceptionSend.senException("cancel order admin => ", e, user);
            log.error("cancel order admin => ", e);
        }

    }

    public void like(Update update, User user, Stadium stadium) {
        try {
            String language = user.getLanguage();
            long chat_id = user.getTelegramId();

            stadium.setStadium_like(stadium.getStadium_like() == null ? 1 : stadium.getStadium_like() + 1);
            stadiumRepository.save(stadium);

            EditMessageText sendMessage = new EditMessageText().setChatId(chat_id)
                    .setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                    .setText(stadium.getName() + System.lineSeparator() + (language.equals(BotAnswerString.uz) ? BotAnswerString.STADIUM_LIKE_UZ : language.equals(
                            BotAnswerString.ru) ? BotAnswerString.STADIUM_LIKE_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.STADIUM_LIKE_UZL : BotAnswerString.STADIUM_LIKE_EN));
            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> listListRows = new ArrayList<>();
            List<InlineKeyboardButton> inlineKeyboardButton = new ArrayList<>();
            inlineKeyboardButton.add(new InlineKeyboardButton().setText(BotAnswerString.like + BotAnswerString.SUCCESSFUL)
                    .setCallbackData(BotAnswerString.SUCCESSFUL + BotAnswerString.like + stadium.getName()));
            inlineKeyboardButton.add(new InlineKeyboardButton().setText(BotAnswerString.CANCEL)
                    .setCallbackData("cancel" + BotAnswerString.CANCEL));
            inlineKeyboardButton.add(new InlineKeyboardButton().setText(BotAnswerString.disLice)
                    .setCallbackData(BotAnswerString.disLice + stadium.getName()));
            listListRows.add(inlineKeyboardButton);
            inlineKeyboardMarkup.setKeyboard(listListRows);
            sendMessage.setReplyMarkup(inlineKeyboardMarkup);
            BRBTBot.execute(sendMessage);
        } catch (Exception e) {
            exceptionSend.senException("like for stadium => ", e, user);
            log.error("like for stadium => ", e);
        }

    }

    public void disLice(Update update, User user, Stadium stadium) {
        try {
            String language = user.getLanguage();
            long chat_id = user.getTelegramId();

            stadium.setStadium_like(stadium.getStadium_like() == null ? -1 : stadium.getStadium_like() - 1);
            stadiumRepository.save(stadium);

            EditMessageText sendMessage = new EditMessageText().setChatId(chat_id)
                    .setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                    .setText(stadium.getName() + System.lineSeparator() + (language.equals(BotAnswerString.uz) ? BotAnswerString.STADIUM_LIKE_UZ : language.equals(
                            BotAnswerString.ru) ? BotAnswerString.STADIUM_LIKE_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.STADIUM_LIKE_UZL : BotAnswerString.STADIUM_LIKE_EN));
            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> listListRows = new ArrayList<>();
            List<InlineKeyboardButton> inlineKeyboardButton = new ArrayList<>();
            inlineKeyboardButton.add(new InlineKeyboardButton().setText(BotAnswerString.like)
                    .setCallbackData(BotAnswerString.like + stadium.getName()));
            inlineKeyboardButton.add(new InlineKeyboardButton().setText(BotAnswerString.CANCEL)
                    .setCallbackData("cancel" + BotAnswerString.CANCEL));
            inlineKeyboardButton.add(new InlineKeyboardButton().setText(BotAnswerString.disLice + BotAnswerString.SUCCESSFUL)
                    .setCallbackData(BotAnswerString.SUCCESSFUL + BotAnswerString.disLice + stadium.getName()));
            listListRows.add(inlineKeyboardButton);
            inlineKeyboardMarkup.setKeyboard(listListRows);
            sendMessage.setReplyMarkup(inlineKeyboardMarkup);
            BRBTBot.execute(sendMessage);
        } catch (Exception e) {
            exceptionSend.senException("dislike => ", e, user);
            log.error("dislike => ", e);
        }

    }

    public void cancelOrderClient(Integer chat_id, Long activeId, User user, Update update) {
        try {
            String language = user.getLanguage();
            String s = (language.equals(BotAnswerString.uz) ? BotAnswerString.CLIENT_CANCEL_ORDER_UZ : language.equals(
                    BotAnswerString.ru) ? BotAnswerString.CLIENT_CANCEL_ORDER_RU : language.equals(BotAnswerString.uzl) ? BotAnswerString.CLIENT_CANCEL_ORDER_UZL : BotAnswerString.CLIENT_CANCEL_ORDER_EN) + System.lineSeparator() + new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm").format(new Date());

            EditMessageText sendMessage = new EditMessageText().setChatId(String.valueOf(chat_id))
                    .setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                    .setText(update.getCallbackQuery().getMessage().getText() + System.lineSeparator() + s);
            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> listListRows = new ArrayList<>();
            List<InlineKeyboardButton> inlineKeyboardButton = new ArrayList<>();
            inlineKeyboardButton.add(new InlineKeyboardButton().setText(BotAnswerString.cancelOrderBTN)
                    .setCallbackData(BotAnswerString.cancelOrder + activeId));
            listListRows.add(inlineKeyboardButton);
            inlineKeyboardMarkup.setKeyboard(listListRows);
            sendMessage.setReplyMarkup(inlineKeyboardMarkup);
            BRBTBot.execute(sendMessage);


            OrdersActive ordersActive = activeRepository.getById(activeId);
            orderMoreWorking.stadiumBossAlertCancelClient(ordersActive.getStadium(), ordersActive);
            ordersActive.setActive(false);
            ordersActive.setCancelOrder(true);
            activeRepository.save(ordersActive);
        } catch (Exception e) {
            exceptionSend.senException("cancel order client => ", e, user);
            log.error("cancel order client => ", e);
        }
    }

    public HttpEntity<ApiResponseModel> getPrice(String start, String end, Long id) {
        try {
            Optional<Stadium> byId = stadiumRepository.findById(id);
            if (byId.isPresent()) {
                Stadium stadium = byId.get();
                return ResponseEntity.status(HttpStatus.OK)
                        .body(new ApiResponseModel(HttpStatus.OK.value(),
                                "price",
                                orderMoreWorking.getPriceStadium(stadium, getDate(start), getDate(end))));
            }
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(new ApiResponseModel(HttpStatus.NOT_FOUND.value(), "stadium not found"));

        } catch (Exception e) {
            exceptionSend.senException("get Price client => ", e, null);
            log.error("get Price client => ", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new ApiResponseModel(HttpStatus.BAD_REQUEST.value(), e.getMessage()));
        }
    }

    private Calendar getDate() {
        Calendar calendar = new GregorianCalendar();
        try {
            calendar.setTimeZone(TimeZone.getTimeZone("Asia/Tashkent"));
            return calendar;
        } catch (Exception e) {
            return calendar;
        }
    }

    private Date getDate(String date) {
        if (date.length() == 5 && (date.contains(":") || date.contains(" ") || date.contains("-") || date.contains(
                "."))) {
            int clock;
            int minute;
            if (date.startsWith("0")) {
                clock = Integer.parseInt(date.substring(1, 2));
                if (date.substring(3).startsWith("0")) {
                    minute = Integer.parseInt(date.substring(4));
                } else {
                    minute = Integer.parseInt(date.substring(3));
                }
            } else {
                clock = Integer.parseInt(date.substring(0, 2));
                if (date.substring(3).startsWith("0")) {
                    minute = Integer.parseInt(date.substring(4));
                } else {
                    minute = Integer.parseInt(date.substring(3));
                }
            }
            if (clock >= 0 && clock < 25 && minute >= 0 && minute < 61) {
                Calendar calendar = getDate();
                calendar.set(Calendar.HOUR_OF_DAY, clock);
                calendar.set(Calendar.MINUTE, minute);
                return calendar.getTime();
            }
        }
        return null;
    }

    public HttpEntity<ApiResponseModel> searchNumber(String phoneNumber) {
        try {
            if (phoneNumber.length() > 4) {
                return ResponseEntity.status(HttpStatus.OK)
                        .body(new ApiResponseModel(HttpStatus.OK.value(),
                                "here it is",
                                userRepository.findAllByPhoneNumberContains(phoneNumber)));
            }
            return ResponseEntity.status(HttpStatus.ACCEPTED)
                    .body(new ApiResponseModel(HttpStatus.ACCEPTED.value(), "let there be at least 5 numbers"));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new ApiResponseModel(HttpStatus.BAD_REQUEST.value(), e.getMessage()));
        }
    }

    public HttpEntity<ApiResponseModel> adminCreateOrder(ReqAdminOrder order, User user) {

        try {
            if (order.getPhoneNumber() == null || order.getPhoneNumber().length() < 6) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body(new ApiResponseModel(HttpStatus.BAD_REQUEST.value(),
                                "phone number Example: +998917797278"));
            }

            Optional<Stadium> stadiumOptional = stadiumRepository.findById(order.getStadiumId());
            if (stadiumOptional.isEmpty()) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body(new ApiResponseModel(HttpStatus.NOT_FOUND.value(), "stadium not found"));
            }
            Stadium stadium = stadiumOptional.get();
            if (!stadium.getUser().getId().equals(user.getId())) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN)
                        .body(new ApiResponseModel(HttpStatus.FORBIDDEN.value(),
                                "you are not the owner of this stadium"));
            }

            User user1 = userRepository.findByPhoneNumber(order.getPhoneNumber())
                    .orElseGet(() -> userRepository.save(new User(order.getPhoneNumber(), "null", "null")));

            double countOrder = workingALot.countOrder(user1, stadium);


            List<OrdersActive> ordersActiveList = new ArrayList<>();
            for (String date : order.getTime()) {

                if (!Pattern.compile("^((19|2[0-9])[0-9]{2})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$")
                        .matcher(date)
                        .matches()) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                            .body(new ApiResponseModel(HttpStatus.BAD_REQUEST.value(),
                                    "time pattern yyyy-MM-dd example:1999-03-03 "));
                }

                OrdersActive active = new OrdersActive();
                active.setActive(true);

                Calendar s = orderMoreWorking.getDate();
                Calendar c = orderMoreWorking.getDate();
                s.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(date));
                c.setTime(workingALot.getDate(order.getEndDate()));
                s.set(Calendar.HOUR_OF_DAY, c.get(Calendar.HOUR_OF_DAY));
                s.set(Calendar.MINUTE, c.get(Calendar.MINUTE));
                active.setEndDate(s.getTime());
                Calendar s1 = orderMoreWorking.getDate();
                Calendar c1 = orderMoreWorking.getDate();
                s1.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(date));
                c1.setTime(workingALot.getDate(order.getStartDate()));
                s1.set(Calendar.HOUR_OF_DAY, c1.get(Calendar.HOUR_OF_DAY));
                s1.set(Calendar.MINUTE, c1.get(Calendar.MINUTE));
                active.setStartDate(s1.getTime());
                active.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(date));


                active.setStadium(stadium);
                active.setSum(orderMoreWorking.getPriceStadium(stadium, s1.getTime(), s.getTime()));

                active.setUser(user1);

                active.setCountOrder(countOrder);


                ordersActiveList.add(active);

            }

            new Thread(() -> {
                for (OrdersActive save : orderActiveRepository.saveAll(ordersActiveList)) {
                    if (user1.getTelegramId() != null) {
                        if (user1.getLanguage() != null) {
                            workingALot.telegramMessageUser(user1.getTelegramId(),
                                    save.getSum(),
                                    user1.getLanguage(),
                                    save);
                        } else {
                            workingALot.telegramMessageUser(user1.getTelegramId(),
                                    save.getSum(),
                                    "uz_lotin\uD83C\uDDFA\uD83C\uDDFF",
                                    save);
                        }
                    }

                }
            }).start();


            return ResponseEntity.status(HttpStatus.OK)
                    .body(new ApiResponseModel(HttpStatus.OK.value(), "create order"));

        } catch (Exception e) {
            exceptionSend.senException("adminCreateOrder => ", e, user);
            log.error("adminCreateOrder => ", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new ApiResponseModel(HttpStatus.BAD_REQUEST.value(), e.getMessage()));
        }
    }

    public HttpEntity<ApiResponseModel> clientCreateOrder(ReqOrder order) {
        try {
            Optional<Stadium> byId = stadiumRepository.findById(order.getStadiumId());
            if (!byId.isPresent()) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body(new ApiResponseModel(HttpStatus.NOT_FOUND.value(), "stadium not found"));
            }
            if (order.getId() != null) {
                if (!orderActiveRepository.existsById(order.getId())) {
                    return ResponseEntity.status(HttpStatus.NOT_FOUND)
                            .body(new ApiResponseModel(HttpStatus.NOT_FOUND.value(), "order not found"));
                }
            }
            Stadium stadium = byId.get();
            List<OrdersActive> oa = new ArrayList<>();
            SimpleDateFormat h = new SimpleDateFormat("yyyy-MM-dd");
            for (OrdersActive ordersActive : activeRepository.findAllByActiveAndStadiumId(true, stadium.getId())) {
                if (h.format(ordersActive.getTime())
                        .equals(h.format(new SimpleDateFormat("yyyy-MM-dd").parse(order.getTime())))) {
                    oa.add(ordersActive);
                }
            }
            if (orderMoreWorking.freeTimeForWeb(oa,
                    workingALot.getDate(order.getStartDate()),
                    workingALot.getDate(order.getEndDate()))) {
                return ResponseEntity.status(HttpStatus.CONFLICT)
                        .body(new ApiResponseModel(HttpStatus.CONFLICT.value(), "this time is buys"));
            }
            HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
            String ip = request.getRemoteAddr();
            if (ip.length() > 0) {
                double sumPrice = orderMoreWorking.getPriceStadium(stadium,
                        workingALot.getDate(order.getStartDate()),
                        workingALot.getDate(order.getEndDate()));
                if (!userRepository.existsAllByPhoneNumber(order.getPhoneNumber())) {
                    User save1 = userRepository.save(new User(order.getPhoneNumber()
                            .startsWith("+") ? order.getPhoneNumber() : "+" + order.getPhoneNumber(),
                            "null",
                            "null"));
                    OrdersActive save = saveActiveOrder(null,
                            save1,
                            stadium,
                            stadium.getLatitude(),
                            stadium.getLongitude(),
                            sumPrice,
                            workingALot.getDate(order.getStartDate()),
                            workingALot.getDate(order.getEndDate()),
                            new SimpleDateFormat("yyyy-MM-dd").parse(order.getTime()),
                            false,
                            false,
                            workingALot.countOrder(save1, stadium));
                    orderMoreWorking.stadiumBossAlert(stadium, save);
                    workingALot.saveIpAddress(save1, ip);
                    return ResponseEntity.status(HttpStatus.OK)
                            .body(new ApiResponseModel(HttpStatus.OK.value(),
                                    "the order was accepted",
                                    new ReqOrder(save.getId(),
                                            save.getStadium().getId(),
                                            save.getUser().getPhoneNumber(),
                                            workingALot.getParsDade(save.getStartDate()),
                                            workingALot.getParsDade(save.getEndDate()),
                                            new SimpleDateFormat("yyyy-MM-dd").format(save.getTime()
                                                    .getTime()))));

                } else {
                    User user = userRepository.findAllByPhoneNumber(order.getPhoneNumber());

                    OrdersActive ordersActive = saveActiveOrder(order.getId(),
                            user,
                            stadium,
                            stadium.getLatitude(),
                            stadium.getLongitude(),
                            sumPrice,
                            workingALot.getDate(order.getStartDate()),
                            workingALot.getDate(order.getEndDate()),
                            new SimpleDateFormat("yyyy-MM-dd").parse(order.getTime()),
                            false,
                            false,
                            workingALot.countOrder(user, stadium));

                    orderMoreWorking.stadiumBossAlert(stadium, ordersActive);
                    if (user.getTelegramId() != null) {
                        if (user.getLanguage() != null) {
                            workingALot.telegramMessageUser(user.getTelegramId(),
                                    sumPrice,
                                    user.getLanguage(),
                                    ordersActive);
                        } else {
                            workingALot.telegramMessageUser(user.getTelegramId(),
                                    sumPrice,
                                    "uz_lotin\uD83C\uDDFA\uD83C\uDDFF",
                                    ordersActive);
                        }
                    }
                    workingALot.saveIpAddress(user, ip);


                    return ResponseEntity.status(HttpStatus.OK)
                            .body(new ApiResponseModel(HttpStatus.OK.value(),
                                    "the order was accepted",
                                    new ReqOrder(ordersActive.getId(),
                                            ordersActive.getStadium().getId(),
                                            ordersActive.getUser().getPhoneNumber(),
                                            workingALot.getParsDade(ordersActive.getStartDate()),
                                            workingALot.getParsDade(ordersActive.getEndDate()),
                                            new SimpleDateFormat("yyyy-MM-dd").format(ordersActive.getTime()
                                                    .getTime()))));
                }

            }
            return ResponseEntity.status(HttpStatus.CONFLICT)
                    .body(new ApiResponseModel(HttpStatus.CONFLICT.value(), "ip address is invalid"));

        } catch (Exception e) {
            exceptionSend.senException("clientCreateOrder => " + order.getPhoneNumber(), e, null);
            log.error("clientCreateOrder => ", e);
            return ResponseEntity.status(HttpStatus.CONFLICT)
                    .body(new ApiResponseModel(HttpStatus.CONFLICT.value(), "the order was not accepted"));
        }
    }

    public OrdersActive saveActiveOrder(Long orderId, User user, Stadium stadium, double latitude, double longitude,
                                        double sum, Date start, Date end, Date time, boolean active, boolean cancel,
                                        double countOrder) {
        if (orderId == null) {
            return activeRepository.save(new OrdersActive(user,
                    stadium,
                    latitude,
                    longitude,
                    sum,
                    start,
                    end,
                    time,
                    active,
                    cancel,
                    countOrder));
        } else {
            OrdersActive ordersActive = orderActiveRepository.getById(orderId);
            if (!ordersActive.getUser().getId().equals(user.getId())) {
                return null;
            }
            ordersActive.setLatitude(latitude);
            ordersActive.setLongitude(longitude);
            ordersActive.setSum(sum);
            ordersActive.setStartDate(start);
            ordersActive.setEndDate(end);
            ordersActive.setTime(time);
            ordersActive.setActive(active);
            ordersActive.setCancelOrder(cancel);
            ordersActive.setCountOrder(countOrder);
            return activeRepository.save(ordersActive);
        }
    }


}
