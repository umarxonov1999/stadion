package koinot.uz.backend.service;

import koinot.uz.backend.bot.BRBT_Bot;
import koinot.uz.backend.entity.Stadium;
import koinot.uz.backend.entity.User;
import koinot.uz.backend.entity.enums.UserState;
import koinot.uz.backend.entity.enums.UserType;
import koinot.uz.backend.exception.ExceptionSend;
import koinot.uz.backend.repository.StadiumRepository;
import koinot.uz.backend.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendVideo;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class BotService {

    @Autowired
    BRBT_Bot BRBTBot;

    @Autowired
    koinot.uz.backend.service.WorkingALot workingALot;

    @Autowired
    UserRepository userRepository;

    @Autowired
    StadiumRepository stadiumRepository;

    @Autowired
    koinot.uz.backend.service.OrderMoreWorking orderMoreWorking;

    @Autowired
    ExceptionSend exceptionSend;


    public void userLanguage(Update update) {
        try{
            int message_id = update.getMessage().getMessageId();

            long chat_id = update.getMessage().getChatId();
            Integer id = update.getMessage().getFrom().getId();
            if(userRepository.existsByTelegramId( id )){
                User user = userRepository.findAllByTelegramId( id );
                if(user.getLanguage() == null){
                    workingALot.getLanguage( update.getMessage().getFrom().getLanguageCode(),chat_id );
                }
                if(user.getLanguage() != null){
                    if(user.getType() != null){
                        if(user.getType().equals( UserType.ADMIN )){
                            List<String> list = new ArrayList<>();
                            String info = "";
                            List<Stadium> allStadium = stadiumRepository.findAllByUserAndDelete( user,false );
                            for (Stadium stadium1 : allStadium) {
                                list.add( stadium1.getName() );
                                info = info.concat( workingALot.getInfoStadium( stadium1,user.getLanguage() ) );
                            }
                            list.add( BotAnswerString.CREATE );

                            BRBTBot.execute( workingALot.inline( chat_id,list,info ) );
                            workingALot.getStadiumDashButton( user.getLanguage(),chat_id );

                        }else if(user.getType().equals( UserType.USER )){
                            workingALot.getClientLocation( user.getLanguage(),chat_id,(int) message_id,(user.getFirstName().equals("null")?user.getLastName():user.getFirstName()) );
                        }
                    }else{
                        workingALot.getClientLocation( user.getLanguage(),chat_id,message_id,(user.getFirstName().equals("null")?user.getLastName():user.getFirstName()) );
                    }
                }
            }else{
                userRepository.save( new User( id,
                        update.getMessage().getFrom().getFirstName() == null ? "null" : update.getMessage()
                                .getFrom()
                                .getFirstName(),
                        update.getMessage().getFrom().getLastName() == null ? "null" : update.getMessage()
                                .getFrom()
                                .getLastName(),
                        UserState.CHOOSE_LANGUAGE ) );
                workingALot.getLanguage( update.getMessage().getFrom().getLanguageCode(),chat_id );
            }
        }catch(TelegramApiException e){
            exceptionSend.senException( "user language => ",e,null );
            log.error( "user language => ",e );
        }
    }

    public void sendVideo(Update update) {
//        try{
//            BRBTBot.execute( new SendVideo().setChatId( update.getMessage().getChatId() )
//                    .setVideo( BotAnswerString.VIDEO_FOR_EXPLAINING_BOT ) );
//        }catch(TelegramApiException e){
//            e.printStackTrace();
//        }
    }

    public void editLanguage(Update update) {
        int message_id = update.getCallbackQuery().getMessage().getMessageId();
        long chat_id = update.getCallbackQuery().getMessage().getChatId();
        try{
            Integer id = update.getCallbackQuery().getFrom().getId();
            User user = userRepository.findAllByTelegramId( id );
            String s = update.getCallbackQuery().getData();
            user.setLanguage( s );
            user.setState( UserState.DEFAULT );
            userRepository.save( user );
            List<String> strings = new ArrayList<>();
            CallbackQuery data = update.getCallbackQuery();
            if(user.getType() != null){
                if(user.getType().equals( UserType.ADMIN )){
                    List<String> list = new ArrayList<>();
                    String info = "";
                    List<Stadium> allStadium = stadiumRepository.findAllByUserAndDelete( user,false );
                    for (Stadium stadium1 : allStadium) {
                        list.add( stadium1.getName() );
                        info = info.concat( workingALot.getInfoStadium( stadium1,user.getLanguage() ) );
                    }
                    list.add( BotAnswerString.CREATE );
                    BRBTBot.execute( workingALot.editInline( chat_id,message_id,list,info ) );
                    workingALot.getStadiumDashButton( user.getLanguage(),chat_id );
                }else if(user.getType().equals( UserType.USER )){
                    workingALot.getClientLocation( user.getLanguage(),chat_id,message_id,(user.getFirstName().equals("null")?user.getLastName():user.getFirstName()) );
                }
            }else{
                strings.add( data.getData().equals( BotAnswerString.uzl ) ? BotAnswerString.yes_uzl : data.getData()
                        .equals( BotAnswerString.uz ) ? BotAnswerString.yes_uz : data.getData()
                        .equals( BotAnswerString.en ) ? BotAnswerString.yes_en : BotAnswerString.yes_ru );
                strings.add( data.getData().equals( BotAnswerString.uzl ) ? BotAnswerString.nope_uzl : data.getData()
                        .equals( BotAnswerString.uz ) ? BotAnswerString.nope_uz : data.getData()
                        .equals( BotAnswerString.en ) ? BotAnswerString.nope_en : BotAnswerString.nope_ru );
                BRBTBot.execute( workingALot.editInline( chat_id,
                        message_id,
                        strings,
                        data.getData()
                                .equals( BotAnswerString.uzl ) ? BotAnswerString.HAVE_YOU_STADIUM_UZL : data.getData()
                                .equals( BotAnswerString.uz ) ? BotAnswerString.HAVE_YOU_STADIUM_UZ : data.getData()
                                .equals( BotAnswerString.en ) ? BotAnswerString.HAVE_YOU_STADIUM_EN : BotAnswerString.HAVE_YOU_STADIUM_RU ) );
            }
        }catch(TelegramApiException e){
            exceptionSend.senException( "edit language => ",e,null );
            log.error( "edit language => ",e );
        }
    }

    public void getStadiumForAdmin(User user,Update update) {
        try{
            long chat_id = update.getMessage().getChatId();
            List<String> list = new ArrayList<>();
            String info = "";
            List<Stadium> allStadium = stadiumRepository.findAllByUserAndDelete( user,false );
            for (Stadium stadium1 : allStadium) {
                list.add( stadium1.getName() );
                info = info.concat( workingALot.getInfoStadium( stadium1,user.getLanguage() ) );
            }
            list.add( BotAnswerString.CREATE );
            if(allStadium.size() > 0) BRBTBot.execute( workingALot.inline( chat_id,list,info ) );
            workingALot.getStadiumDashButton( user.getLanguage(),chat_id );
        }catch(Exception e){
            exceptionSend.senException( "get stadium for admin => ",e,user );
            log.error( "get stadium for admin => ",e );
        }

    }

    public void changeLanguage(User user,Update update) {
        try{
            user.setState( UserState.CHOOSE_LANGUAGE );
            userRepository.save( user );
            workingALot.getLanguage( update.getMessage().getFrom().getLanguageCode(),update.getMessage().getChatId() );
        }catch(Exception e){
            exceptionSend.senException( "change language => ",e,user );
            log.error( "change language => ",e );
        }

    }
}
