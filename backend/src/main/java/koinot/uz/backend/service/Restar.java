package koinot.uz.backend.service;

import koinot.uz.backend.bot.BRBT_Bot;
import koinot.uz.backend.entity.Stadium;
import koinot.uz.backend.entity.StadiumPhoto;
import koinot.uz.backend.entity.User;
import koinot.uz.backend.entity.enums.AttachmentTypeEnumWhy;
import koinot.uz.backend.entity.enums.UserState;
import koinot.uz.backend.exception.ExceptionSend;
import koinot.uz.backend.repository.OrderActiveRepository;
import koinot.uz.backend.repository.StadiumPhotoRepository;
import koinot.uz.backend.repository.StadiumRepository;
import koinot.uz.backend.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendSticker;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class Restar {

    @Autowired
    BRBT_Bot BRBTBot;

    @Autowired
    BotService botService;

    @Autowired
    WorkingALot workingALot;

    @Autowired
    OrderActiveRepository activeRepository;

    @Autowired
    StadiumPhotoRepository stadiumPhotoRepository;

    @Autowired
    StadiumRepository stadiumRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    OrderMoreWorking orderMoreWorking;
    @Autowired
    ExceptionSend exceptionSend;


    public void registerBoss(User user,Update update) {
        try{
            if(! stadiumRepository.existsByUser( user )){
                botService.userLanguage( update );
            }else{
                Stadium stadium = user.getActiveStadium();
                if(! stadium.isActive()){
                    user.setActiveStadium( null );
                    user.setType( null );
                    user.setState( UserState.CHOOSE_LANGUAGE );
                    userRepository.save( user );
                    List<StadiumPhoto> byStadium = stadiumPhotoRepository.findByStadiumIdAndWhyOrderByCreatedAtDesc(
                            stadium.getId(),
                            AttachmentTypeEnumWhy.STADIUM_PHOTO );
                    for (StadiumPhoto stadiumPhoto : byStadium) {
                        stadiumPhotoRepository.delete( stadiumPhoto );
                    }
                    stadiumRepository.delete( stadium );
                    workingALot.getLanguage( update.getMessage().getFrom().getLanguageCode(),user.getTelegramId() );
                }
            }
        }catch(Exception e){
            exceptionSend.senException( "restart register boss => ",e,user );
            log.error( "restart register boss => ",e );
        }

    }

    public void getLocationButton(String language,long chat_id) {
        try{
            SendMessage sendMessage = new SendMessage().setChatId( chat_id )
                    .setText( language.equals( BotAnswerString.uz ) ? BotAnswerString.GET_ADDRESS_STADIUM_UZ : language.equals(
                            BotAnswerString.ru ) ? BotAnswerString.GET_ADDRESS_STADIUM_RU : language.equals(
                            BotAnswerString.uzl ) ? BotAnswerString.GET_ADDRESS_STADIUM_UZL : BotAnswerString.GET_ADDRESS_STADIUM_EN );
            ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
            sendMessage.setReplyMarkup( replyKeyboardMarkup );
            replyKeyboardMarkup.setSelective( true );
            replyKeyboardMarkup.setResizeKeyboard( true );
            replyKeyboardMarkup.setOneTimeKeyboard( true );
            List<KeyboardRow> keyboard = new ArrayList<>();
            KeyboardRow keyboardFirstRow = new KeyboardRow();
            KeyboardButton keyboardButton = new KeyboardButton();
            keyboardButton.setText( language.equals( BotAnswerString.uz ) ? BotAnswerString.SEND_MY_CURRENT_LOCATION_UZ : language.equals(
                            BotAnswerString.ru ) ? BotAnswerString.SEND_MY_CURRENT_LOCATION_RU : language.equals(
                            BotAnswerString.uzl ) ? BotAnswerString.SEND_MY_CURRENT_LOCATION_UZL : BotAnswerString.SEND_MY_CURRENT_LOCATION_EN )
                    .setRequestLocation( true );
            keyboardFirstRow.add( keyboardButton );
            keyboard.add( keyboardFirstRow );

            KeyboardRow keyboardSecondRow = new KeyboardRow();
            KeyboardButton keyboardButtonCancel = new KeyboardButton();
            keyboardButtonCancel.setText( language.equals( BotAnswerString.uz ) ? BotAnswerString.RESTART_UZ : language.equals(
                    BotAnswerString.ru ) ? BotAnswerString.RESTART_RU : language.equals( BotAnswerString.uzl ) ? BotAnswerString.RESTART_UZL : BotAnswerString.RESTART_EN );
            keyboardSecondRow.add( keyboardButtonCancel );
            keyboard.add( keyboardSecondRow );
            replyKeyboardMarkup.setKeyboard( keyboard );
            SendSticker sendSticker = new SendSticker().setChatId( chat_id )
                    .setSticker( BotAnswerString.GET_LOCATION_STICKER );
            BRBTBot.execute( sendSticker );
            BRBTBot.execute( sendMessage );
        }catch(Exception e){
            exceptionSend.senException( "restart get location btn => ",e,null );
            log.error( "restart get location btn => ",e );
        }

    }

    public void getPhoNumberButton(String language,long chat_id) {
        SendMessage sendMessage = new SendMessage().setChatId( chat_id )
                .setText( language.equals( BotAnswerString.uz ) ? BotAnswerString.GET_PHONE_NUMBER_STADIUM_UZ : language.equals(
                        BotAnswerString.ru ) ? BotAnswerString.GET_PHONE_NUMBER_STADIUM_RU : language.equals(
                        BotAnswerString.uzl ) ? BotAnswerString.GET_PHONE_NUMBER_STADIUM_UZL : BotAnswerString.GET_PHONE_NUMBER_STADIUM_EN );
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup( replyKeyboardMarkup );
        replyKeyboardMarkup.setSelective( true );
        replyKeyboardMarkup.setResizeKeyboard( true );
        replyKeyboardMarkup.setOneTimeKeyboard( true );
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow keyboardFirstRow = new KeyboardRow();
        KeyboardButton keyboardButton = new KeyboardButton();
        keyboardButton.setText( language.equals( BotAnswerString.uz ) ? BotAnswerString.SEND_THIS_PHONE_NUMBER_UZ : language.equals(
                        BotAnswerString.ru ) ? BotAnswerString.SEND_THIS_PHONE_NUMBER_RU : language.equals( BotAnswerString.uzl ) ? BotAnswerString.SEND_THIS_PHONE_NUMBER_UZL : BotAnswerString.SEND_THIS_PHONE_NUMBER_EN )
                .setRequestContact( true );
        keyboardFirstRow.add( keyboardButton );
        keyboard.add( keyboardFirstRow );

        KeyboardRow keyboardSecondRow = new KeyboardRow();
        KeyboardButton keyboardButtonCancel = new KeyboardButton();
        keyboardButtonCancel.setText( language.equals( BotAnswerString.uz ) ? BotAnswerString.RESTART_UZ : language.equals(
                BotAnswerString.ru ) ? BotAnswerString.RESTART_RU : language.equals( BotAnswerString.uzl ) ? BotAnswerString.RESTART_UZL : BotAnswerString.RESTART_EN );
        keyboardSecondRow.add( keyboardButtonCancel );
        keyboard.add( keyboardSecondRow );
        replyKeyboardMarkup.setKeyboard( keyboard );
        try{
            SendSticker sendSticker = new SendSticker().setChatId( chat_id )
                    .setSticker( BotAnswerString.GET_PHONE_STICKER );
            BRBTBot.execute( sendSticker );
            BRBTBot.execute( sendMessage );
        }catch(TelegramApiException e){
            exceptionSend.senException( "get phone number btn => ",e,null );
            log.error( "get phone number btn => ",e );
        }
    }
}
