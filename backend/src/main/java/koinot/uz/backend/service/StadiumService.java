package koinot.uz.backend.service;

import koinot.uz.backend.entity.OrdersActive;
import koinot.uz.backend.entity.Stadium;
import koinot.uz.backend.entity.StadiumPhoto;
import koinot.uz.backend.entity.User;
import koinot.uz.backend.entity.enums.AttachmentTypeEnumWhy;
import koinot.uz.backend.exception.ExceptionSend;
import koinot.uz.backend.payload.*;
import koinot.uz.backend.repository.*;
import koinot.uz.backend.security.AuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class StadiumService {

    @Autowired
    StadiumRepository stadiumRepository;
    @Autowired
    OrderActiveRepository orderActiveRepository;
    @Autowired
    OrderArchiveRepository orderArchiveRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    StadiumPhotoRepository stadiumPhotoRepository;
    @Autowired
    AuthService authService;
    @Autowired
    WorkingALot workingALot;
    @Autowired
    SupperAdmin supperAdmin;
    @Autowired
    ExceptionSend exceptionSend;
    @Autowired
    OrderActiveRepository activeRepository;
    @Autowired
    OrderMoreWorking orderMoreWorking;
    @Value ( "${bot.token}" )
    private String botToken;
    @Value ( "${upload.folder}" )
    private String path;

    public RestWebInfo getStadium() {
        List<RestStadium> stadiums = new ArrayList<>();
        for (Stadium stadium : stadiumRepository.findAllByActiveAndDelete(true , false)) {
            stadiums.add(new RestStadium(stadium.getId() ,
                    stadium.getLatitude() ,
                    stadium.getLongitude() ,
                    stadium.getStadium_like() ,
                    0 ,
                    stadium.getName() ,
                    stadium.isRoof()));
        }
        return new RestWebInfo(userRepository.count() ,
                (orderActiveRepository.count() + orderArchiveRepository.count()) ,
                stadiums);
    }

    public RestStadiumInfo getStadiumOrder(Long id) {
        Optional<Stadium> byId = stadiumRepository.findById(id);
        SimpleDateFormat min = new SimpleDateFormat("HH:mm");
        if (byId.isPresent()) {
            Stadium stadium = byId.get();

            return new RestStadiumInfo(stadium.getId() ,
                    stadium.getPhone_number() ,
                    stadium.getStadium_like() ,
                    min.format(stadium.getChange_price_time()) ,
                    stadium.getPrice_day_time() ,
                    stadium.getPrice_night_time() ,
                    stadium.getWidth() ,
                    stadium.getHeight() ,
                    stadium.getCount_order() ,
                    stadium.getOpening_time() == null ? null : min.format(stadium.getOpening_time()) ,
                    stadium.getClosing_time() == null ? null : min.format(stadium.getClosing_time()) ,
                    generationOrder(stadium.getId()) ,
                    generationPhotoLink(stadiumPhotoRepository.findByStadiumIdAndWhyOrderByCreatedAtDesc(stadium.getId() ,
                            AttachmentTypeEnumWhy.STADIUM_PHOTO)) ,
                    stadium.isRoof() ,
                    stadium.getDescription());
        }
        return null;
    }

    public List<ReqStadiumOrder> generationOrder(Long id) {
        SimpleDateFormat day = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat min = new SimpleDateFormat("HH:mm");
        return orderActiveRepository.findAllByActiveAndStadiumId(true , id)
                .stream()
                .map(o -> new ReqStadiumOrder(min.format(o.getStartDate()) ,
                        min.format(o.getEndDate()) ,
                        day.format(o.getTime())))
                .collect(Collectors.toList());
    }

    private List<RestMyStadium> generation(List<Stadium> list) {
        List<RestMyStadium> myStadiums = new ArrayList<>();
        for (Stadium stadium : list) {
            int active = 0;
            int notActive = 0;
            for (OrdersActive ordersActive : orderActiveRepository.findAllByStadiumAndCancelOrderOrderByTimeDesc(
                    stadium ,
                    false)) {
                if (ordersActive.isActive()) {
                    active++;
                } else {
                    notActive++;
                }
            }
            myStadiums.add(new RestMyStadium(stadium.getId() ,
                    stadium.getName() ,
                    active ,
                    notActive ,
                    stadium.getLatitude() ,
                    stadium.getPhone_number() ,
                    stadium.getLongitude() ,
                    stadium.getAddress() ,
                    workingALot.getParsDade(stadium.getOpening_time()) ,
                    workingALot.getParsDade(stadium.getClosing_time()) ,
                    stadium.getStadium_like() ,
                    workingALot.getParsDade(stadium.getChange_price_time()) ,
                    stadium.getPrice_day_time() ,
                    stadium.getPrice_night_time() ,
                    stadium.getWidth() ,
                    stadium.getHeight() ,
                    stadium.getCount_order() ,
                    stadium.isOpen() ,
                    stadium.isActive() ,
                    stadium.isVerify() ,
                    stadium.isRoof() ,
                    stadium.getMoney() ,
                    generationPhotoLink(stadiumPhotoRepository.findByStadiumIdAndWhyOrderByCreatedAtDesc(stadium.getId() ,
                            AttachmentTypeEnumWhy.STADIUM_PHOTO))));
        }
        return myStadiums;
    }

    public List<ResUploadFile> generationPhotoLink(List<StadiumPhoto> photo) {
//        Gson gson = new Gson( );
//        List<String> url = new ArrayList<>( );
//        for (StadiumPhoto stadiumPhoto : photo) {
//            url.add( ServletUriComponentsBuilder.fromCurrentContextPath( )
//                    .path( "/koinot/stadium/byteFile/" )
//                    .path( stadiumPhoto.getId( ).toString( ) )
//                    .toUriString( ) );
//        }
//
//        return gson.toJson( url );

        return photo.stream()
                .map(stadiumPhoto -> new ResUploadFile(stadiumPhoto.getId() ,
                        stadiumPhoto.getName() ,
                        stadiumPhoto.getContentType() ,
                        stadiumPhoto.getWhy() ,
                        stadiumPhoto.getSize() ,
                        ServletUriComponentsBuilder.fromCurrentContextPath()
                                .path("/koinot/stadium/byteFile/")
                                .path(stadiumPhoto.getId().toString())
                                .toUriString()))
                .collect(Collectors.toList());
    }

    public List<RestMyStadiumOrder> generationOrder(Stadium stadium) {
        SimpleDateFormat day = new SimpleDateFormat("yyyy-MM-dd");
        day.setTimeZone(TimeZone.getTimeZone("Asia/Tashkent"));

        return orderActiveRepository.findAllByStadiumAndCancelOrderOrderByTimeDesc(stadium , false)
                .stream()
                .map(ordersActive -> new RestMyStadiumOrder(ordersActive.getCreatedAt().getTime() ,
                        ordersActive.getId() ,
                        ordersActive.getUser().getPhoneNumber() ,
                        ordersActive.getUser().getFirstName() ,
                        ordersActive.getUser().getLastName() ,
                        ordersActive.getUser().getLanguage() ,
                        ordersActive.getLatitude() ,
                        ordersActive.getLongitude() ,
                        ordersActive.getSum() ,
                        getParsDade(ordersActive.getStartDate()) ,
                        getParsDade(ordersActive.getEndDate()) ,
                        day.format(ordersActive.getTime()) ,
                        ordersActive.isActive() ,
                        ordersActive.isCancelOrder() ,
                        ordersActive.getCountOrder()))
                .collect(Collectors.toList());
    }

    public HttpEntity<ApiResponseModel> myStadium(User user) {
        try {
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new ApiResponseModel(HttpStatus.OK.value() ,
                            "here it is" ,
                            generation(stadiumRepository.findAllByUserAndDeleteOrderById(user , false))));
        } catch (Exception e) {
            exceptionSend.senException(" controller myStadium => stadium not found for you =>" , e , user);
            log.error(" controller myStadium => stadium not found for you =>" , e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(new ApiResponseModel(HttpStatus.NOT_FOUND.value() , "stadium not found for you"));
        }
    }

    public HttpEntity<ApiResponseModel> myStadiumOrder(User user , Long id) {
        try {
            Optional<Stadium> byId = stadiumRepository.findById(id);
            if (byId.isPresent()) {
                Stadium stadium = byId.get();
                if (stadium.getUser().getId().equals(user.getId())) {
                    return ResponseEntity.status(HttpStatus.OK)
                            .body(new ApiResponseModel(HttpStatus.OK.value() ,
                                    "here it is" ,
                                    generationOrder(stadium)));
                }
                return ResponseEntity.status(HttpStatus.FORBIDDEN)
                        .body(new ApiResponseModel(HttpStatus.FORBIDDEN.value() ,
                                "you are not the owner of this stadium"));
            }
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(new ApiResponseModel(HttpStatus.NOT_FOUND.value() , "stadium is not available"));
        } catch (Exception e) {
            exceptionSend.senException("controller my stadium order => " , e , user);
            log.error("controller my stadium order => " , e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(new ApiResponseModel(HttpStatus.NOT_FOUND.value() , e.getMessage()));
        }
    }

    public HttpEntity<ApiResponseModel> activeOrder(User user , Long id) {
        Optional<OrdersActive> byId = orderActiveRepository.findById(id);
        if (byId.isPresent()) {
            OrdersActive ordersActive = byId.get();
            if (ordersActive.getStadium().getUser().getId().equals(user.getId())) {
                ordersActive.setActive(!ordersActive.isActive());
                OrdersActive save = orderActiveRepository.save(ordersActive);
                Integer telegramId = ordersActive.getUser().getTelegramId();
                if (telegramId != null) {
                    authService.sendTelegramCode(telegramId , generationTextSuccessful(ordersActive));
                }
                return ResponseEntity.status(HttpStatus.OK)
                        .body(new ApiResponseModel(HttpStatus.OK.value() ,
                                save.isActive() ? "active" : "not active"));
            }
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body(new ApiResponseModel(HttpStatus.FORBIDDEN.value() ,
                            "you are not the owner of this stadium"));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new ApiResponseModel(HttpStatus.NOT_FOUND.value() , "this id= " + id + " not found"));
    }

    public HttpEntity<ApiResponseModel> cancelOrder(User user , Long id) {
        Optional<OrdersActive> byId = orderActiveRepository.findById(id);
        if (byId.isPresent()) {
            OrdersActive ordersActive = byId.get();
            if (ordersActive.getStadium().getUser().getId().equals(user.getId())) {
                ordersActive.setActive(false);
                ordersActive.setCancelOrder(!ordersActive.isCancelOrder());
                OrdersActive save = orderActiveRepository.save(ordersActive);
                Integer telegramId = ordersActive.getUser().getTelegramId();
                if (telegramId != null) {
                    authService.sendTelegramCode(telegramId , generationTextWarning(ordersActive));
                }
                return ResponseEntity.status(HttpStatus.OK)
                        .body(new ApiResponseModel(HttpStatus.OK.value() ,
                                save.isCancelOrder() ? "cancel" : "not cancel"));
            }
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body(new ApiResponseModel(HttpStatus.FORBIDDEN.value() ,
                            "you are not the owner of this stadium"));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new ApiResponseModel(HttpStatus.NOT_FOUND.value() , "this id= " + id + " not found"));
    }

    private Calendar getDate() {
        Calendar calendar = new GregorianCalendar();
        try {
            calendar.setTimeZone(TimeZone.getTimeZone("Asia/Tashkent"));
            return calendar;
        } catch (Exception e) {
            return calendar;
        }
    }

    public ApiResponseModel photoIds(Long id) {
        try {
            return new ApiResponseModel(HttpStatus.OK.value() ,
                    "here it is" ,
                    generationPhotoLink(stadiumPhotoRepository.findByStadiumIdAndWhyOrderByCreatedAtDesc(id ,
                            AttachmentTypeEnumWhy.STADIUM_PHOTO)));
        } catch (Exception e) {
            return new ApiResponseModel(HttpStatus.NOT_FOUND.value() , "stadium not found");
        }
    }

    public HttpEntity<ApiResponseModel> uploadPhotoFileList(MultipartHttpServletRequest request , Long id , User user) {
        try {
            Iterator<String> iterator = request.getFileNames();
            MultipartFile multipartFile;
            List<ResUploadFile> resUploadFiles = new ArrayList<>();
            while (iterator.hasNext()) {
                multipartFile = request.getFile(iterator.next());
                StadiumPhoto stadiumPhoto = new StadiumPhoto();
                Stadium stadiumById = getStadiumById(id);
                if (stadiumById == null) {
                    return ResponseEntity.status(HttpStatus.CONFLICT)
                            .body(new ApiResponseModel(HttpStatus.CONFLICT.value() , "stadium is not available"));
                }
                if (!stadiumById.getUser().getId().equals(user.getId())) {
                    return ResponseEntity.status(HttpStatus.FORBIDDEN)
                            .body(new ApiResponseModel(HttpStatus.FORBIDDEN.value() ,
                                    "you are not the owner of this stadium"));
                }
                if (stadiumPhotoRepository.countStadiumPhotoByStadiumAndWhy(stadiumById ,
                        AttachmentTypeEnumWhy.STADIUM_PHOTO) > 9) {
                    return ResponseEntity.status(HttpStatus.CONFLICT)
                            .body(new ApiResponseModel(HttpStatus.CONFLICT.value() ,
                                    "the number of images may not exceed 10"));
                }
                stadiumPhoto.setStadium(stadiumById);
                stadiumPhoto.setSize(multipartFile.getSize());
                stadiumPhoto.setUser(user);
                stadiumPhoto.setWhy(AttachmentTypeEnumWhy.STADIUM_PHOTO);
                stadiumPhoto.setName(multipartFile.getOriginalFilename());
                stadiumPhoto.setContentType(multipartFile.getContentType());
                if (getExt(multipartFile.getOriginalFilename()) == null || multipartFile.getSize() == 0 || !multipartFile.getContentType()
                        .startsWith("image")) {
                    return ResponseEntity.status(HttpStatus.CONFLICT)
                            .body(new ApiResponseModel(HttpStatus.CONFLICT.value() ,
                                    "file not confirmed ... send picture"));
                }
                stadiumPhoto.setExtension(getExt(multipartFile.getOriginalFilename()));

                StadiumPhoto save = stadiumPhotoRepository.save(stadiumPhoto);

                File uploadFolder = new File(path + "/" + getDate().get(Calendar.YEAR) + "/" + getDate().get(
                        Calendar.MONTH) + "/" + getDate().get(Calendar.DAY_OF_MONTH));

                if (uploadFolder.mkdirs() && uploadFolder.exists()) {
                    System.out.println("papkalar yaratildi  " + uploadFolder.getAbsolutePath());
                }

                uploadFolder = uploadFolder.getAbsoluteFile();
                File file = new File(uploadFolder + "/" + save.getId() + "_" + stadiumById.getName() + save.getExtension());
                save.setPath(file.getAbsolutePath());

                try {
                    multipartFile.transferTo(file);
                    StadiumPhoto save1 = stadiumPhotoRepository.save(save);
                    resUploadFiles.add(new ResUploadFile(save1.getId() ,
                            save1.getName() ,
                            save1.getContentType() ,
                            save1.getWhy() ,
                            save1.getSize() ,
                            ServletUriComponentsBuilder.fromCurrentContextPath()
                                    .path("/koinot/stadium/byteFile/")
                                    .path(stadiumPhoto.getId().toString())
                                    .toUriString()));

                } catch (Exception e) {
                    stadiumPhotoRepository.delete(save);
                    exceptionSend.senException(" controller uploadPhotoFileList => " , e , user);
                    log.error(" controller uploadPhotoFileList => " , e);
                    return ResponseEntity.status(HttpStatus.CONFLICT)
                            .body(new ApiResponseModel(HttpStatus.CONFLICT.value() , e.getMessage() , null));
                }
            }
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new ApiResponseModel(HttpStatus.OK.value() , "saved" , resUploadFiles));
        } catch (Exception e) {
            exceptionSend.senException(" controller uploadPhotoFileList => " , e , user);
            log.error(" controller uploadPhotoFileList => " , e);
            return ResponseEntity.status(HttpStatus.CONFLICT)
                    .body(new ApiResponseModel(HttpStatus.CONFLICT.value() , e.getMessage() , null));
        }

    }

    public String getExt(String fileName) {
        String ext = null;
        if (fileName != null && !fileName.isEmpty()) {
            int dot = fileName.lastIndexOf(".");
            if (dot > 0 && dot <= fileName.length() - 2) {
                ext = fileName.substring(dot);
            }
        }
        return ext;
    }

    public Stadium getStadiumById(Long id) {
        try {
            return stadiumRepository.getById(id);
        } catch (Exception e) {
            return null;
        }
    }

    public String generationTextSuccessful(OrdersActive ordersActive) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        formatter.setTimeZone(TimeZone.getTimeZone("Asia/Tashkent"));

        String language = ordersActive.getUser().getLanguage() == null ? BotAnswerString.uz : ordersActive.getUser()
                .getLanguage();
        return ordersActive.getStadium()
                .getName() + System.lineSeparator() + formatter.format(ordersActive.getTime()) + "  ➡️      " + getParsDade(
                ordersActive.getStartDate()) + " - " + getParsDade(ordersActive.getEndDate()) + System.lineSeparator() + System.lineSeparator() + (language.equals(
                BotAnswerString.uz) ? BotAnswerString.ORDER_ACTIVE_UZ : language.equals(BotAnswerString.ru) ? BotAnswerString.ORDER_ACTIVE_RU : language.equals(
                BotAnswerString.uzl) ? BotAnswerString.ORDER_ACTIVE_UZL : BotAnswerString.ORDER_ACTIVE_EN);
    }

    public String generationTextWarning(OrdersActive ordersActive) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        formatter.setTimeZone(TimeZone.getTimeZone("Asia/Tashkent"));
        String language = ordersActive.getUser().getLanguage() == null ? BotAnswerString.uz : ordersActive.getUser()
                .getLanguage();
        return ordersActive.getStadium()
                .getName() + System.lineSeparator() + formatter.format(ordersActive.getTime()) + "  ➡️      " + getParsDade(
                ordersActive.getStartDate()) + " - " + getParsDade(ordersActive.getEndDate()) + System.lineSeparator() + System.lineSeparator() + (language.equals(
                BotAnswerString.uz) ? BotAnswerString.ORDER_CANCEL_UZ : language.equals(BotAnswerString.ru) ? BotAnswerString.ORDER_CANCEL_RU : language.equals(
                BotAnswerString.uzl) ? BotAnswerString.ORDER_CANCEL_UZL : BotAnswerString.ORDER_CANCEL_EN);
    }

    public String getParsDade(Date date) {
        try {
            Calendar calendar = getDate();
            calendar.setTime(date);
            Integer clock = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);
            if (clock < 10) {
                if (minute < 10) {
                    return "0" + clock + ":" + "0" + minute;
                } else {
                    return "0" + clock + ":" + minute;
                }

            } else {
                if (minute < 10) {
                    return clock + ":" + "0" + minute;
                } else {
                    return clock + ":" + minute;
                }
            }
        } catch (Exception e) {
            return null;
        }
    }

    public HttpEntity<?> byteFile(Long id) throws IOException {
        try {
            StadiumPhoto one = stadiumPhotoRepository.findAllByIdAndWhy(id , AttachmentTypeEnumWhy.STADIUM_PHOTO);
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(one.getContentType()))
                    .header(HttpHeaders.CONTENT_DISPOSITION , "attachment; filename=\"" + one.getName() + "\"")
                    .body(Files.readAllBytes(Paths.get(one.getPath())));

        } catch (Exception e) {
            return ResponseEntity.ok()
                    .contentType(MediaType.IMAGE_JPEG)
                    .body(StreamUtils.copyToByteArray(new ClassPathResource("image/Logo.png").getInputStream()));
        }
    }

    public HttpEntity<ApiResponseModel> deletePhoto(Long id , User user) {
        try {
            StadiumPhoto stadiumPhoto = stadiumPhotoRepository.findAllByIdAndWhy(id ,
                    AttachmentTypeEnumWhy.STADIUM_PHOTO);
            if (stadiumPhoto != null && stadiumPhoto.getStadium().getUser().getId().equals(user.getId())) {
                stadiumPhoto.setWhy(AttachmentTypeEnumWhy.DELETE_STADIUM_PHOTO);
                stadiumPhotoRepository.save(stadiumPhoto);
                return ResponseEntity.status(HttpStatus.OK)
                        .body(new ApiResponseModel(HttpStatus.OK.value() , "deleted"));
            }
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body(new ApiResponseModel(HttpStatus.FORBIDDEN.value() ,
                            "you are not the owner of this stadium"));
        } catch (Exception e) {
            exceptionSend.senException(" controller deletePhoto => " , e , user);
            log.error(" controller deletePhoto => " , e);
            return ResponseEntity.status(HttpStatus.CONFLICT)
                    .body(new ApiResponseModel(HttpStatus.CONFLICT.value() , "not delete"));
        }
    }

    public HttpEntity<ApiResponseModel> saveOrEditStadium(User user , ReqStadium reqStadium) {
        try {
            ApiResponseModel apiResponseModel = new ApiResponseModel();
            Stadium stadium = new Stadium();
            apiResponseModel.setMessage("save");

            if (reqStadium.getId() != null) {
                Optional<Stadium> stadiumOptional = stadiumRepository.findById(reqStadium.getId());
                if (stadiumOptional.isEmpty()) {
                    return ResponseEntity.status(HttpStatus.NOT_FOUND)
                            .body(new ApiResponseModel(HttpStatus.NOT_FOUND.value() , "stadium not found"));
                }
                stadium = stadiumOptional.get();
                if (!stadium.getUser().getId().equals(user.getId())) {
                    return ResponseEntity.status(HttpStatus.FORBIDDEN)
                            .body(new ApiResponseModel(HttpStatus.FORBIDDEN.value() ,
                                    "you are not the owner of this stadium"));
                }
                apiResponseModel.setMessage("edit");
                if (stadiumRepository.existsByNameAndIdNot(reqStadium.getName() , stadium.getId())) {
                    return ResponseEntity.status(HttpStatus.CONFLICT)
                            .body(new ApiResponseModel(HttpStatus.CONFLICT.value() ,
                                    "The " + reqStadium.getName() + " name is busy"));
                }
                stadium.setName(reqStadium.getName());
            }
            if (apiResponseModel.getMessage().equals("save")) {
                if (stadiumRepository.existsAllByName(reqStadium.getName())) {
                    return ResponseEntity.status(HttpStatus.CONFLICT)
                            .body(new ApiResponseModel(HttpStatus.CONFLICT.value() ,
                                    "The " + reqStadium.getName() + " name is busy"));
                }
                stadium.setName(reqStadium.getName());
            }
            stadium.setPhone_number(reqStadium.getPhone_number());
            stadium.setLatitude(reqStadium.getLatitude());
            stadium.setLongitude(reqStadium.getLongitude());
            stadium.setAddress(reqStadium.getAddress());
            stadium.setOpening_time(workingALot.getDate(reqStadium.getOpening_time()));
            stadium.setClosing_time(workingALot.getDate(reqStadium.getClosing_time()));
            stadium.setChange_price_time(workingALot.getDate(reqStadium.getChange_price_time()));
            stadium.setPrice_day_time(reqStadium.getPrice_day_time());
            stadium.setPrice_night_time(reqStadium.getPrice_night_time());
            stadium.setWidth(reqStadium.getWidth());
            stadium.setHeight(reqStadium.getHeight());
            stadium.setRoof(reqStadium.isRoof());
            stadium.setDescription(reqStadium.getDescription());
            stadium.setUser(user);
            supperAdmin.alertSupperAdmin(stadiumRepository.save(stadium));
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new ApiResponseModel(HttpStatus.OK.value() , apiResponseModel.getMessage()));
        } catch (Exception e) {
            exceptionSend.senException(" controller saveOrEditStadium => " , e , user);
            log.error(" controller saveOrEditStadium => " , e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ApiResponseModel(HttpStatus.INTERNAL_SERVER_ERROR.value() , "failed... use brbtbot"));
        }
    }

    public HttpEntity<ApiResponseModel> delete(User user , Long id) {
        Optional<Stadium> byId = stadiumRepository.findById(id);
        if (byId.isPresent()) {
            Stadium stadium = byId.get();
            if (stadium.getUser().getId().equals(user.getId())) {
                deleteStadium(stadium);
                return ResponseEntity.status(HttpStatus.OK)
                        .body(new ApiResponseModel(HttpStatus.OK.value() , "this stadion delete"));
            }
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body(new ApiResponseModel(HttpStatus.FORBIDDEN.value() ,
                            "you are not the owner of this stadium"));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new ApiResponseModel(HttpStatus.NOT_FOUND.value() , "stadium fot found"));
    }

    public HttpEntity<ApiResponseModel> getCancel(User user , Long id) {
        Optional<Stadium> byId = stadiumRepository.findById(id);
        if (byId.isPresent()) {
            Stadium stadium = byId.get();
            if (stadium.getUser().getId().equals(user.getId())) {
                SimpleDateFormat day = new SimpleDateFormat("yyyy-MM-dd");

                return ResponseEntity.status(HttpStatus.OK)
                        .body(new ApiResponseModel(HttpStatus.OK.value() ,
                                "here it is" ,
                                orderActiveRepository.findAllByStadiumAndCancelOrderOrderByTimeDesc(stadium , true)
                                        .stream()
                                        .map(active -> new RestMyStadiumOrder(active.getCreatedAt().getTime() ,
                                                active.getId() ,
                                                active.getUser().getPhoneNumber() ,
                                                active.getUser().getFirstName() ,
                                                active.getUser().getLastName() ,
                                                active.getUser().getLanguage() ,
                                                active.getLatitude() ,
                                                active.getLongitude() ,
                                                active.getSum() ,
                                                getParsDade(active.getStartDate()) ,
                                                getParsDade(active.getEndDate()) ,
                                                day.format(active.getTime()) ,
                                                active.isActive() ,
                                                active.isCancelOrder() ,
                                                active.getCountOrder()))
                                        .collect(Collectors.toList())));
            }
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body(new ApiResponseModel(HttpStatus.FORBIDDEN.value() ,
                            "you are not the owner of this stadium"));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new ApiResponseModel(HttpStatus.NOT_FOUND.value() , "stadium fot found"));
    }

    public HttpEntity<ApiResponseModel> deleteCancel(User user , Long id) {
        Optional<OrdersActive> byId = orderActiveRepository.findById(id);
        if (byId.isPresent()) {
            OrdersActive order = byId.get();
            if (order.getStadium().getUser().getId().equals(user.getId())) {
                orderActiveRepository.delete(order);
                return ResponseEntity.status(HttpStatus.OK)
                        .body(new ApiResponseModel(HttpStatus.OK.value() , "delete"));
            }
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body(new ApiResponseModel(HttpStatus.FORBIDDEN.value() ,
                            "you are not the owner of this stadium"));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new ApiResponseModel(HttpStatus.NOT_FOUND.value() , "this order has expired"));
    }

    public void deleteStadium(Stadium stadium) {
        stadium.setDelete(true);
        stadium.setName(stadium.getName() + stadium.getName() + stadium.getName());
        stadiumRepository.save(stadium);
    }

    public HttpEntity<?> searchStadiumComfort(Double lat , Double lng , Optional<Integer> r0 , Optional<Integer> r) {
        Integer r1 = r0.orElse(0);
        Integer r2 = r.orElse(10000);
        if (r2 - r1 > 30000) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new ApiResponseModel(HttpStatus.BAD_REQUEST.value() ,
                            "the search sector should not exceed 30,000 m  => if(r-r0>30000)"));
        }
        List<StadiumDistance> nearStadium = workingALot.getNearStadium(lng , lat)
                .stream()
                .filter(s -> s.getDistance() >= r1 && s.getDistance() <= r2)
                .collect(Collectors.toList());
        SimpleDateFormat min = new SimpleDateFormat("HH:mm");

        return ResponseEntity.status(HttpStatus.OK)
                .body(new ApiResponseModel(HttpStatus.OK.value() ,
                        "here it is" ,
                        nearStadium.stream()
                                .map(stadium -> new RestStadiumInfo(stadium.getId() ,
                                        stadium.getPhoneNumber() ,
                                        stadium.getStadiumLike() ,
                                        min.format(stadium.getChangePriceTime()) ,
                                        stadium.getPriceDayTime() ,
                                        stadium.getPriceNightTime() ,
                                        stadium.getWidth() ,
                                        stadium.getHeight() ,
                                        stadium.getCountOrder() ,
                                        stadium.getOpeningTime() == null ? null : min.format(stadium.getOpeningTime()) ,
                                        stadium.getClosingTime() == null ? null : min.format(stadium.getClosingTime()) ,
                                        generationOrder(stadium.getId()) ,
                                        generationPhotoLink(stadiumPhotoRepository.findByStadiumIdAndWhyOrderByCreatedAtDesc(
                                                stadium.getId() ,
                                                AttachmentTypeEnumWhy.STADIUM_PHOTO)) ,
                                        stadium.isRoof() ,
                                        stadium.getDescription()))
                                .collect(Collectors.toList())));
    }

    public boolean stadiumIsFree(Long id , String startDate , String endDate , String time) {
        List<OrdersActive> oa = new ArrayList<>();
        SimpleDateFormat h = new SimpleDateFormat("yyyy-MM-dd");
        for (OrdersActive ordersActive : activeRepository.findAllByActiveAndStadiumId(true , id)) {
            if (h.format(ordersActive.getTime()).equals(h.format(time))) {
                oa.add(ordersActive);
            }
        }
        return orderMoreWorking.freeTimeForWeb(oa , workingALot.getDate(startDate) , workingALot.getDate(endDate));
    }

    public HttpEntity<?> image(String name) throws IOException {
        if (name.contains("marker.png")) {
            return ResponseEntity.ok()
                    .contentType(MediaType.IMAGE_PNG)
                    .body(StreamUtils.copyToByteArray(new ClassPathResource("image/marker.png").getInputStream()));
        }

        if (name.contains("nav.png")) {
            return ResponseEntity.ok()
                    .contentType(MediaType.IMAGE_PNG)
                    .body(StreamUtils.copyToByteArray(new ClassPathResource("image/nav.png").getInputStream()));
        }

        return ResponseEntity.ok()
                .contentType(MediaType.IMAGE_PNG)
                .body(StreamUtils.copyToByteArray(new ClassPathResource("image/Logo.png").getInputStream()));


    }

}
