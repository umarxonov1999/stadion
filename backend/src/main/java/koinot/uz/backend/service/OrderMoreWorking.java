package koinot.uz.backend.service;

import com.google.gson.Gson;
import koinot.uz.backend.bot.BRBT_Bot;
import koinot.uz.backend.entity.OrdersActive;
import koinot.uz.backend.entity.OrdersArchive;
import koinot.uz.backend.entity.Stadium;
import koinot.uz.backend.entity.User;
import koinot.uz.backend.entity.enums.NatificationType;
import koinot.uz.backend.exception.ExceptionSend;
import koinot.uz.backend.payload.RegionLocation;
import koinot.uz.backend.payload.ReqOrder;
import koinot.uz.backend.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendSticker;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Slf4j
public class OrderMoreWorking {

    @Autowired
    BRBT_Bot BRBTBot;

    @Autowired
    StadiumPhotoRepository stadiumPhotoRepository;

    @Autowired
    StadiumRepository stadiumRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    koinot.uz.backend.service.WorkingALot workingALot;

    @Autowired
    OrderMoreWorking orderMoreWorking;

    @Autowired
    OrderActiveRepository orderActiveRepository;

    @Autowired
    OrderArchiveRepository archiveRepository;

    @Autowired
    SystemVariableRepository systemVariableRepository;

    @Autowired
    koinot.uz.backend.service.NotificationService notificationService;

    @Autowired
    ExceptionSend exceptionSend;


    public void getRegionBtn(long chat_id,String language,String text) {
        try{
            SendMessage sendMessage = new SendMessage().setChatId( chat_id ).setText( text );
            ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
            sendMessage.setReplyMarkup( replyKeyboardMarkup );
            replyKeyboardMarkup.setSelective( true );
            replyKeyboardMarkup.setResizeKeyboard( true );
            replyKeyboardMarkup.setOneTimeKeyboard( true );

            List<KeyboardRow> keyboard = new ArrayList<>();
            KeyboardRow keyboardFirstRow = new KeyboardRow();
            KeyboardButton keyboardButton = new KeyboardButton();
            keyboardButton.setText( language.equals( BotAnswerString.uz ) ? BotAnswerString.SEND_MY_CURRENT_LOCATION_UZ : language.equals(
                            BotAnswerString.ru ) ? BotAnswerString.SEND_MY_CURRENT_LOCATION_RU : language.equals(
                            BotAnswerString.uzl ) ? BotAnswerString.SEND_MY_CURRENT_LOCATION_UZL : BotAnswerString.SEND_MY_CURRENT_LOCATION_EN )
                    .setRequestLocation( true );

            keyboardFirstRow.add( keyboardButton );
            keyboard.add( keyboardFirstRow );

            HashMap<String, RegionLocation> map = workingALot.getRegion();
            for (String key : map.keySet()) {
                KeyboardButton keyboardButton1 = new KeyboardButton();
                KeyboardRow forRow = new KeyboardRow();
                keyboardButton1.setText( key );
                forRow.add( keyboardButton1 );
                keyboard.add( forRow );
            }
            KeyboardRow keyboardFirstRow0 = new KeyboardRow();
            KeyboardButton keyboardButton0 = new KeyboardButton();
            keyboardButton0.setText( language.equals( BotAnswerString.uz ) ? BotAnswerString.CHANGE_CLIENT_PHONE_NUMBER_UZ : language.equals(
                    BotAnswerString.ru ) ? BotAnswerString.CHANGE_CLIENT_PHONE_NUMBER_RU : language.equals(
                    BotAnswerString.uzl ) ? BotAnswerString.CHANGE_CLIENT_PHONE_NUMBER_UZL : BotAnswerString.CHANGE_CLIENT_PHONE_NUMBER_EN );

            keyboardFirstRow0.add( keyboardButton0 );
            keyboard.add( keyboardFirstRow0 );
            replyKeyboardMarkup.setKeyboard( keyboard );
            BRBTBot.execute( sendMessage );
        }catch(Exception e){
            exceptionSend.senException( "get Region Btn => ",e,null );
            log.error( "get Region Btn => ",e );
        }

    }

    public void editInline(Long chatId,Integer msgId,Stadium stadium,String language,String text) {
        try{
            EditMessageText editMessageText = new EditMessageText().setChatId( chatId )
                    .setText( text )
                    .setMessageId( msgId );
            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> listListRows = new ArrayList<>();
            List<InlineKeyboardButton> inlineKeyboardButton = new ArrayList<>();
            String order = language.equals( BotAnswerString.uz ) ? BotAnswerString.ORDER_UZ : language.equals(
                    BotAnswerString.ru ) ? BotAnswerString.ORDER_RU : language.equals( BotAnswerString.uzl ) ? BotAnswerString.ORDER_UZL : BotAnswerString.ORDER_EN;
            inlineKeyboardButton.add( new InlineKeyboardButton().setText( BotAnswerString.CANCEL )
                    .setCallbackData( BotAnswerString.cancel_client + stadium.getName() ) );
            inlineKeyboardButton.add( new InlineKeyboardButton().setText( order )
                    .setCallbackData( BotAnswerString.get_order + stadium.getName() ) );

            listListRows.add( inlineKeyboardButton );

            inlineKeyboardMarkup.setKeyboard( listListRows );
            editMessageText.setReplyMarkup( inlineKeyboardMarkup );

            BRBTBot.execute( editMessageText );
        }catch(Exception e){
            exceptionSend.senException( " Telegram bot editInline => ",e,stadium.getUser() );
            log.error( " Telegram bot editInline => ",e );
        }

    }

    public boolean freeTime(Date startActive,Date endActive,Date startBlack,Date endBlack) {
        SimpleDateFormat h = new SimpleDateFormat( "HH" );
        SimpleDateFormat m = new SimpleDateFormat( "mm" );
        int sa = ( Integer.parseInt( h.format( startActive ) ) * 60 ) + Integer.parseInt( m.format( startActive ) );
        int ea = ( Integer.parseInt( h.format( endActive ) ) * 60 ) + Integer.parseInt( m.format( endActive ) );
        int sb = ( Integer.parseInt( h.format( startBlack ) ) * 60 ) + Integer.parseInt( m.format( startBlack ) );
        int eb = ( Integer.parseInt( h.format( endBlack ) ) * 60 ) + Integer.parseInt( m.format( endBlack ) );
        return ( sa < sb && ea > sb ) || ( sa < eb && ea > eb ) || ( sb < sa && eb > sa ) || ( sb < ea && eb > ea );
    }

    public boolean startFreeTime(List<OrdersActive> ordersActives,Date startBlack,String language,long chat_id) {
        try{
            SimpleDateFormat h = new SimpleDateFormat( "HH" );
            SimpleDateFormat m = new SimpleDateFormat( "mm" );
            h.setTimeZone( TimeZone.getTimeZone( "Asia/Tashkent" ) );

            if(! ordersActives.isEmpty()){
                for (OrdersActive ordersActive : ordersActives) {
                    if(( Integer.parseInt( h.format( ordersActive.getStartDate() ) ) + ( Integer.parseInt( m.format(
                            ordersActive.getStartDate() ) ) ) == ( ( ( Integer.parseInt( h.format( startBlack ) ) ) ) + ( ( Integer.parseInt(
                            m.format( startBlack ) ) ) ) ) )){
                        SendMessage sendMessage = new SendMessage().setChatId( chat_id )
                                .setText( language.equals( BotAnswerString.uz ) ? BotAnswerString.BUSY_TIME_UZ : language.equals(
                                        BotAnswerString.ru ) ? BotAnswerString.BUSY_TIME_RU : language.equals(
                                        BotAnswerString.uzl ) ? BotAnswerString.BUSY_TIME_UZL : BotAnswerString.BUSY_TIME_EN );
                        BRBTBot.execute( sendMessage );
                        return false;
                    }
                }
            }
            if(getDate().getTime().after( startBlack )){
                SendMessage sendMessage = new SendMessage().setChatId( chat_id )
                        .setText( language.equals( BotAnswerString.uz ) ? BotAnswerString.ERROR_STADIUM_UZ : language.equals(
                                BotAnswerString.ru ) ? BotAnswerString.ERROR_STADIUM_RU : language.equals(
                                BotAnswerString.uzl ) ? BotAnswerString.ERROR_STADIUM_UZL : BotAnswerString.ERROR_STADIUM_EN );
                BRBTBot.execute( sendMessage );
                return false;
            }
            return true;
        }catch(Exception e){
            exceptionSend.senException( " Telegram bot startFreeTime => ",e,null );
            log.error( " Telegram bot startFreeTime => ",e );
            return false;
        }

    }

    public boolean startFreeTimeForWeb(List<OrdersActive> ordersActives,Date startBlack) {
        SimpleDateFormat h = new SimpleDateFormat( "HH" );
        if(! ordersActives.isEmpty()){
            for (OrdersActive ordersActive : ordersActives) {
                if(( Integer.parseInt( h.format( ordersActive.getStartDate() ) ) == ( ( Integer.parseInt( h.format(
                        startBlack ) ) ) ) )){
                    return false;
                }
            }
        }
        return ! getDate().getTime().after( startBlack );
    }

    public boolean freeTimeForWeb(List<OrdersActive> ordersActives,Date startBlack,Date endBlack) {
        SimpleDateFormat h = new SimpleDateFormat( "HH" );
        if(! ordersActives.isEmpty()){
            for (OrdersActive ordersActive : ordersActives) {
                if(freeTime( ordersActive.getStartDate(),ordersActive.getEndDate(),startBlack,endBlack )){
                    return true;
                }
            }
        }
        return false;
    }

    public boolean endDateFree(List<OrdersActive> ordersActives,Date startBlack,Date endBlack,String language,
                               long chat_id) {
        try{
            SimpleDateFormat h = new SimpleDateFormat( "HH" );
            SimpleDateFormat m = new SimpleDateFormat( "mm" );
            h.setTimeZone( TimeZone.getTimeZone( "Asia/Tashkent" ) );
            m.setTimeZone( TimeZone.getTimeZone( "Asia/Tashkent" ) );
            if(! ordersActives.isEmpty()){
                for (OrdersActive ordersActive : ordersActives) {
                    if(freeTime( ordersActive.getStartDate(),ordersActive.getEndDate(),startBlack,endBlack )){
                        SendMessage sendMessage = new SendMessage().setChatId( chat_id )
                                .setText( language.equals( BotAnswerString.uz ) ? BotAnswerString.BUSY_TIME_UZ : language.equals(
                                        BotAnswerString.ru ) ? BotAnswerString.BUSY_TIME_RU : language.equals(
                                        BotAnswerString.uzl ) ? BotAnswerString.BUSY_TIME_UZL : BotAnswerString.BUSY_TIME_EN );
                        BRBTBot.execute( sendMessage );
                        return false;
                    }
                }
            }
            int sb = ( Integer.parseInt( h.format( startBlack ) ) * 60 ) + Integer.parseInt( m.format( startBlack ) );
            int eb = ( Integer.parseInt( h.format( endBlack ) ) * 60 ) + Integer.parseInt( m.format( endBlack ) );
            if(sb >= eb){
                SendMessage sendMessage = new SendMessage().setChatId( chat_id )
                        .setText( language.equals( BotAnswerString.uz ) ? BotAnswerString.ERROR_STADIUM_UZ : language.equals(
                                BotAnswerString.ru ) ? BotAnswerString.ERROR_STADIUM_RU : language.equals(
                                BotAnswerString.uzl ) ? BotAnswerString.ERROR_STADIUM_UZL : BotAnswerString.ERROR_STADIUM_EN );
                BRBTBot.execute( sendMessage );
                return false;
            }
            return true;
        }catch(Exception e){
            exceptionSend.senException( " Telegram bot endDateFree => ",e,null );
            log.error( " Telegram bot endDateFree => ",e );
            return false;
        }

    }

    public boolean endDateFreeForWeb(List<OrdersActive> ordersActives,Date startBlack,Date endBlack) {
        SimpleDateFormat h = new SimpleDateFormat( "HH" );
        SimpleDateFormat m = new SimpleDateFormat( "mm" );
        if(! ordersActives.isEmpty()){
            for (OrdersActive ordersActive : ordersActives) {
                if(freeTime( ordersActive.getStartDate(),ordersActive.getEndDate(),startBlack,endBlack )){

                    return false;
                }
            }
        }
        int sb = ( Integer.parseInt( h.format( startBlack ) ) * 60 ) + Integer.parseInt( m.format( startBlack ) );
        int eb = ( Integer.parseInt( h.format( endBlack ) ) * 60 ) + Integer.parseInt( m.format( endBlack ) );
        if(sb >= eb){

            return false;
        }
        return true;
    }

    public void stadiumBossAlert(Stadium stadium,OrdersActive order) {
        try{
            long chat_id = stadium.getUser().getTelegramId();
            String language = stadium.getUser().getLanguage();
            SimpleDateFormat formatter = new SimpleDateFormat( "yyyy-MM-dd" );
            Date date = order.getTime();


            int d = (int) workingALot.distance( stadium.getLongitude(),
                    stadium.getLatitude(),
                    order.getLongitude(),
                    order.getLatitude(),
                    0,
                    0 );

            String countOrder = language.equals( BotAnswerString.uz ) ? BotAnswerString.ORDER_COUNT_UZ : language.equals(
                    BotAnswerString.ru ) ? BotAnswerString.ORDER_COUNT_RU : language.equals( BotAnswerString.uzl ) ? BotAnswerString.ORDER_COUNT_UZL : BotAnswerString.ORDER_COUNT_EN;

            SendMessage sendMessage = new SendMessage().setChatId( chat_id )
                    .setText( stadium.getName() + System.lineSeparator() + ( order.getUser()
                            .getPhoneNumber() == null ? "" : order.getUser()
                            .getPhoneNumber() ) + System.lineSeparator() + ( order.getUser()
                            .getLastName()
                            .equals( "null" ) ? "" : order.getUser().getLastName() ) + " " + ( order.getUser()
                            .getFirstName()
                            .equals( "null" ) ? "" : order.getUser()
                            .getFirstName() ) + System.lineSeparator() + formatter.format( date ) + "  ➡️      " + workingALot.getParsDade(
                            order.getStartDate() ) + " - " + workingALot.getParsDade( order.getEndDate() ) + System.lineSeparator() + d / 1000 + " Km (" + d % 1000 + " m)" + System.lineSeparator() + orderMoreWorking.getPriceStadium(
                            stadium,
                            order.getStartDate(),
                            order.getEndDate() ) + BotAnswerString.MANY + System.lineSeparator() + countOrder + ": " + order.getCountOrder() );
            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> listListRows = new ArrayList<>();

            String verify = language.equals( BotAnswerString.uz ) ? BotAnswerString.VERiFY_UZ : language.equals(
                    BotAnswerString.ru ) ? BotAnswerString.VERiFY_RU : language.equals( BotAnswerString.uzl ) ? BotAnswerString.VERiFY_UZL : BotAnswerString.VERiFY_EN;

            String cancel = BotAnswerString.CANCEL;

            List<InlineKeyboardButton> inlineKeyboardButton = new ArrayList<>();
            inlineKeyboardButton.add( new InlineKeyboardButton().setText( verify )
                    .setCallbackData( BotAnswerString.verify + order.getId() ) );
            inlineKeyboardButton.add( new InlineKeyboardButton().setText( cancel )
                    .setCallbackData( BotAnswerString.cancelBoss + order.getId() ) );
            listListRows.add( inlineKeyboardButton );

            inlineKeyboardMarkup.setKeyboard( listListRows );
            sendMessage.setReplyMarkup( inlineKeyboardMarkup );
            BRBTBot.execute( sendMessage );
            User user = order.getUser();
            notificationService.notification( countOrder,
                    user.getFirstName() + " " + user.getLastName() + " " + user.getPhoneNumber(),
                    order.getId(),
                    NatificationType.ORDER,
                    stadium.getUser(),
                    order.getStadium(),
                    "" );
        }catch(Exception e){
            exceptionSend.senException( " Telegram bot stadiumBossAlert => ",e,order.getUser() );
            log.error( " Telegram bot stadiumBossAlert => ",e );
        }

    }

    public void stadiumBossAlertCancelClient(Stadium stadium,OrdersActive order) {
        try{
            long chat_id = stadium.getUser().getTelegramId();
            String language = stadium.getUser().getLanguage();
            SimpleDateFormat formatter = new SimpleDateFormat( "yyyy-MM-dd" );
            Date date = order.getTime();


            int d = (int) workingALot.distance( stadium.getLongitude(),
                    stadium.getLatitude(),
                    order.getLongitude(),
                    order.getLatitude(),
                    0,
                    0 );

            String countOrder = language.equals( BotAnswerString.uz ) ? BotAnswerString.ORDER_CANCEL_BOSS_UZ : language.equals(
                    BotAnswerString.ru ) ? BotAnswerString.ORDER_CANCEL_BOSS_RU : language.equals( BotAnswerString.uzl ) ? BotAnswerString.ORDER_CANCEL_BOSS_UZL : BotAnswerString.ORDER_CANCEL_BOSS_EN;

            String cancel = language.equals( BotAnswerString.uz ) ? BotAnswerString.ORDER_CANCEL_CLIENT_UZ : language.equals(
                    BotAnswerString.ru ) ? BotAnswerString.ORDER_CANCEL_CLIENT_RU : language.equals( BotAnswerString.uzl ) ? BotAnswerString.ORDER_CANCEL_CLIENT_UZL : BotAnswerString.ORDER_CANCEL_CLIENT_EN;

            SendMessage sendMessage = new SendMessage().setChatId( chat_id )
                    .setText( stadium.getName() + System.lineSeparator() + ( order.getUser()
                            .getPhoneNumber() == null ? "" : order.getUser()
                            .getPhoneNumber() ) + System.lineSeparator() + ( order.getUser()
                            .getLastName()
                            .equals( "null" ) ? "" : order.getUser().getLastName() ) + " " + ( order.getUser()
                            .getFirstName()
                            .equals( "null" ) ? "" : order.getUser()
                            .getFirstName() ) + System.lineSeparator() + formatter.format( date ) + "  ➡️      " + workingALot.getParsDade(
                            order.getStartDate() ) + " - " + workingALot.getParsDade( order.getEndDate() ) + System.lineSeparator() + d / 1000 + " Km (" + d % 1000 + " m)" + System.lineSeparator() + orderMoreWorking.getPriceStadium(
                            stadium,
                            order.getStartDate(),
                            order.getEndDate() ) + BotAnswerString.MANY + System.lineSeparator() + countOrder + ": " + order.getCountOrder() + System.lineSeparator() + cancel );

            BRBTBot.execute( sendMessage );
            User user = order.getUser();
            Gson gson = new Gson();
            notificationService.notification( countOrder,
                    user.getFirstName() + " " + user.getLastName() + " " + user.getPhoneNumber(),
                    order.getId(),
                    NatificationType.CANCEL,
                    stadium.getUser(),
                    order.getStadium(),
                    gson.toJson( new ReqOrder( order.getId(),
                            order.getStadium().getId(),
                            workingALot.getParsDade( order.getStartDate() ),
                            order.getUser().getPhoneNumber(),
                            workingALot.getParsDade( order.getEndDate() ),
                            String.valueOf( order.getTime() ) ) ) );
        }catch(Exception e){
            exceptionSend.senException( " Telegram bot stadiumBossAlertCancelClient => ",e,order.getUser() );
            log.error( " Telegram bot stadiumBossAlertCancelClient => ",e );
        }

    }

    public void getLocationButtonForClient(String language,long chat_id) {
        try{
            SendMessage sendMessage = new SendMessage().setChatId( chat_id )
                    .setText( language.equals( BotAnswerString.uz ) ? BotAnswerString.GET_CLIENT_LOCATION_UZ : language.equals(
                            BotAnswerString.ru ) ? BotAnswerString.GET_CLIENT_LOCATION_RU : language.equals(
                            BotAnswerString.uzl ) ? BotAnswerString.GET_CLIENT_LOCATION_UZL : BotAnswerString.GET_CLIENT_LOCATION_EN );
            ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
            sendMessage.setReplyMarkup( replyKeyboardMarkup );
            replyKeyboardMarkup.setSelective( true );
            replyKeyboardMarkup.setResizeKeyboard( true );
            replyKeyboardMarkup.setOneTimeKeyboard( true );
            List<KeyboardRow> keyboard = new ArrayList<>();
            KeyboardRow keyboardFirstRow = new KeyboardRow();
            KeyboardButton keyboardButton = new KeyboardButton();
            keyboardButton.setText( language.equals( BotAnswerString.uz ) ? BotAnswerString.SEND_MY_CURRENT_LOCATION_UZ : language.equals(
                            BotAnswerString.ru ) ? BotAnswerString.SEND_MY_CURRENT_LOCATION_RU : language.equals(
                            BotAnswerString.uzl ) ? BotAnswerString.SEND_MY_CURRENT_LOCATION_UZL : BotAnswerString.SEND_MY_CURRENT_LOCATION_EN )
                    .setRequestLocation( true );
            keyboardFirstRow.add( keyboardButton );
            keyboard.add( keyboardFirstRow );
            replyKeyboardMarkup.setKeyboard( keyboard );
            SendSticker sendSticker = new SendSticker().setChatId( chat_id )
                    .setSticker( BotAnswerString.GET_LOCATION_STICKER );
            BRBTBot.execute( sendSticker );
            BRBTBot.execute( sendMessage );
        }catch(Exception e){
            exceptionSend.senException( " Telegram bot getLocationButtonForClient => ",e,null );
            log.error( " Telegram bot getLocationButtonForClient => ",e );
        }

    }

    public List<String> getDStadiumFreeTime(Stadium stadium,List<OrdersActive> orders) {
        List<String> time = new ArrayList<>();
        Calendar calendar = orderMoreWorking.getDate();
        calendar.set( Calendar.HOUR,0 );
        calendar.set( Calendar.MINUTE,0 );
        for (int i = 0; i < 24; i++) {
            int n = calendar.get( Calendar.HOUR );
            calendar.set( Calendar.HOUR,n + 1 );
            if(! orders.isEmpty()){
                if(( stadium.getOpening_time()
                        .getHours() <= calendar.get( Calendar.HOUR_OF_DAY ) ) && ( stadium.getClosing_time()
                        .getHours() >= calendar.get( Calendar.HOUR_OF_DAY ) )){
                    for (OrdersActive order : orders) {
                        if(! ( order.getStartDate()
                                .getHours() == calendar.get( Calendar.HOUR_OF_DAY ) || ( order.getStartDate()
                                .getHours() < calendar.get( Calendar.HOUR_OF_DAY ) && order.getEndDate()
                                .getHours() > calendar.get( Calendar.HOUR_OF_DAY ) ) )){
                            time.add( workingALot.getParsDade( calendar.getTime() ) );
                        }
                    }
                }
            }else{
                if(( stadium.getOpening_time()
                        .getHours() <= calendar.get( Calendar.HOUR_OF_DAY ) ) && ( stadium.getClosing_time()
                        .getHours() >= calendar.get( Calendar.HOUR_OF_DAY ) )){
                    time.add( workingALot.getParsDade( calendar.getTime() ) );
                }
            }
        }
        return time;
    }

    public Calendar getDate() {
        Calendar calendar = new GregorianCalendar();
        try{
            calendar.setTimeZone( TimeZone.getTimeZone( "Asia/Tashkent" ) );
            return calendar;
        }catch(Exception e){
            return calendar;
        }
    }

    public String getWeek(int week,String language) {
        String s;
        switch (week) {
            case 1:
                s = language.equals( BotAnswerString.uz ) ? BotAnswerString.SUNDAY_UZ : language.equals( BotAnswerString.ru ) ? BotAnswerString.SUNDAY_RU : language.equals(
                        BotAnswerString.uzl ) ? BotAnswerString.SUNDAY_UZL : BotAnswerString.SUNDAY_EN;
                break;
            case 2:
                s = language.equals( BotAnswerString.uz ) ? BotAnswerString.MONDAY_UZ : language.equals( BotAnswerString.ru ) ? BotAnswerString.MONDAY_RU : language.equals(
                        BotAnswerString.uzl ) ? BotAnswerString.MONDAY_UZL : BotAnswerString.MONDAY_EN;
                break;
            case 3:
                s = language.equals( BotAnswerString.uz ) ? BotAnswerString.TUESDAY_UZ : language.equals(
                        BotAnswerString.ru ) ? BotAnswerString.TUESDAY_RU : language.equals( BotAnswerString.uzl ) ? BotAnswerString.TUESDAY_UZL : BotAnswerString.TUESDAY_EN;
                break;
            case 4:
                s = language.equals( BotAnswerString.uz ) ? BotAnswerString.WEDNESDAY_UZ : language.equals(
                        BotAnswerString.ru ) ? BotAnswerString.WEDNESDAY_RU : language.equals( BotAnswerString.uzl ) ? BotAnswerString.WEDNESDAY_UZL : BotAnswerString.WEDNESDAY_EN;
                break;
            case 5:
                s = language.equals( BotAnswerString.uz ) ? BotAnswerString.THURSDAY_UZ : language.equals(
                        BotAnswerString.ru ) ? BotAnswerString.THURSDAY_RU : language.equals( BotAnswerString.uzl ) ? BotAnswerString.THURSDAY_UZL : BotAnswerString.THURSDAY_EN;
                break;
            case 6:
                s = language.equals( BotAnswerString.uz ) ? BotAnswerString.FRIDAY_UZ : language.equals( BotAnswerString.ru ) ? BotAnswerString.FRIDAY_RU : language.equals(
                        BotAnswerString.uzl ) ? BotAnswerString.FRIDAY_UZL : BotAnswerString.FRIDAY_EN;
                break;
            case 7:
                s = language.equals( BotAnswerString.uz ) ? BotAnswerString.SATURDAY_UZ : language.equals(
                        BotAnswerString.ru ) ? BotAnswerString.SATURDAY_RU : language.equals( BotAnswerString.uzl ) ? BotAnswerString.SATURDAY_UZL : BotAnswerString.SATURDAY_EN;
                break;
            default:
                s = "null";
                break;
        }
        return s;
    }

    public void editOrderBoss(User user,OrdersActive order,Update update,String s) {
        try{
            String language = user.getLanguage();
            EditMessageText sendMessage = new EditMessageText().setChatId( String.valueOf( user.getTelegramId() ) )
                    .setMessageId( update.getCallbackQuery().getMessage().getMessageId() )
                    .setText( update.getCallbackQuery().getMessage().getText() + System.lineSeparator() + s );
            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> listListRows = new ArrayList<>();

            String verify = language.equals( BotAnswerString.uz ) ? BotAnswerString.VERiFY_UZ : language.equals(
                    BotAnswerString.ru ) ? BotAnswerString.VERiFY_RU : language.equals( BotAnswerString.uzl ) ? BotAnswerString.VERiFY_UZL : BotAnswerString.VERiFY_EN;

            String cancel = BotAnswerString.CANCEL;

            List<InlineKeyboardButton> inlineKeyboardButton = new ArrayList<>();
            inlineKeyboardButton.add( new InlineKeyboardButton().setText( verify )
                    .setCallbackData( BotAnswerString.verify + order.getId() ) );
            inlineKeyboardButton.add( new InlineKeyboardButton().setText( cancel )
                    .setCallbackData( BotAnswerString.cancelBoss + order.getId() ) );
            listListRows.add( inlineKeyboardButton );

            inlineKeyboardMarkup.setKeyboard( listListRows );
            sendMessage.setReplyMarkup( inlineKeyboardMarkup );
            BRBTBot.execute( sendMessage );
        }catch(Exception e){
            exceptionSend.senException( " Telegram bot editOrderBoss => ",e,user );
            log.error( " Telegram bot editOrderBoss => ",e );
        }

    }

    public void afterPlay() {
        Calendar calendar = getDate();
        for (OrdersActive ordersActive : orderActiveRepository.findAll()) {
            if(ordersActive.getTime().before( calendar.getTime() )){
                if(ordersActive.getEndDate().before( calendar.getTime() )){
                    if(ordersActive.isActive()){
                        sendFeedback( ordersActive );
                        User user = ordersActive.getStadium().getUser();

                        String language = user.getLanguage();

                        String countOrder = language.equals( BotAnswerString.uz ) ? BotAnswerString.PLAY_FINISHED_UZ : language.equals(
                                BotAnswerString.ru ) ? BotAnswerString.PLAY_FINISHED_RU : language.equals(
                                BotAnswerString.uzl ) ? BotAnswerString.PLAY_FINISHED_UZL : BotAnswerString.PLAY_FINISHED_EN;
                        notificationService.notification( countOrder,
                                ordersActive.getUser().getFirstName() + " " + ordersActive.getUser()
                                        .getLastName() + " " + ordersActive.getUser().getPhoneNumber(),
                                ordersActive.getId(),
                                NatificationType.FINISH,
                                user,
                                ordersActive.getStadium(),
                                "" );
                    }
                    archiveRepository.save( new OrdersArchive( ordersActive.getUser(),
                            ordersActive.getStadium(),
                            ordersActive.getLatitude(),
                            ordersActive.getLongitude(),
                            ordersActive.getSum(),
                            ordersActive.getStartDate(),
                            ordersActive.getEndDate(),
                            ordersActive.getTime(),
                            ordersActive.isActive(),
                            ordersActive.isCancelOrder(),
                            ordersActive.getCountOrder() ) );
                    orderActiveRepository.delete( ordersActive );
                    Stadium stadium = ordersActive.getStadium();
                    stadium.setCount_order( stadium.getCount_order() + 1 );
                    stadium.setMoney( stadium.getMoney() + ordersActive.getSum() );
                    stadiumRepository.save( stadium );
                }
            }
        }
    }

    public void sendFeedback(OrdersActive ordersActive) {
        try{
            User user = ordersActive.getUser();
            Stadium stadium = ordersActive.getStadium();
            if(user.getTelegramId() != null){
                if(! stadium.getUser().getId().equals( user.getId() )){
                    long chat_id = user.getTelegramId();
                    String language = user.getLanguage() == null ? BotAnswerString.uz : user.getLanguage();
                    SendMessage sendMessage = new SendMessage().setChatId( chat_id )
                            .setText( stadium.getName() + System.lineSeparator() + ( language.equals( BotAnswerString.uz ) ? BotAnswerString.STADIUM_LIKE_UZ : language.equals(
                                    BotAnswerString.ru ) ? BotAnswerString.STADIUM_LIKE_RU : language.equals(
                                    BotAnswerString.uzl ) ? BotAnswerString.STADIUM_LIKE_UZL : BotAnswerString.STADIUM_LIKE_EN ) );
                    InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
                    List<List<InlineKeyboardButton>> listListRows = new ArrayList<>();
                    List<InlineKeyboardButton> inlineKeyboardButton = new ArrayList<>();
                    inlineKeyboardButton.add( new InlineKeyboardButton().setText( BotAnswerString.like )
                            .setCallbackData( BotAnswerString.like + stadium.getName() ) );
                    inlineKeyboardButton.add( new InlineKeyboardButton().setText( BotAnswerString.CANCEL )
                            .setCallbackData( "cancel" + BotAnswerString.CANCEL ) );
                    inlineKeyboardButton.add( new InlineKeyboardButton().setText( BotAnswerString.disLice )
                            .setCallbackData( BotAnswerString.disLice + stadium.getName() ) );
                    listListRows.add( inlineKeyboardButton );
                    inlineKeyboardMarkup.setKeyboard( listListRows );
                    sendMessage.setReplyMarkup( inlineKeyboardMarkup );
                    BRBTBot.execute( sendMessage );
                }
            }
        }catch(Exception e){
            exceptionSend.senException( " Telegram bot sendFeedback => ",e,ordersActive.getUser() );
            log.error( " Telegram bot sendFeedback => ",e );
        }

    }

    public double getPriceStadium(Stadium stadium,Date start,Date end) {
        DecimalFormat df = new DecimalFormat( "#.##" );
        df.setRoundingMode( RoundingMode.CEILING );
        SimpleDateFormat h = new SimpleDateFormat( "HH" );
        SimpleDateFormat m = new SimpleDateFormat( "mm" );
        int st = ( Integer.parseInt( h.format( start ) ) * 60 ) + Integer.parseInt( m.format( start ) );
        int et = ( Integer.parseInt( h.format( end ) ) * 60 ) + Integer.parseInt( m.format( end ) );
        int ss = ( Integer.parseInt( h.format( stadium.getChange_price_time() ) ) * 60 ) + Integer.parseInt( m.format(
                stadium.getChange_price_time() ) );
        double price_day_time = stadium.getPrice_day_time() / 60;
        double price_night_time = stadium.getPrice_night_time() / 60;
        double better = getBetter( start,end );
        if(st < ss && et < ss){
            return Double.parseDouble( df.format( price_day_time * better ) );
        }else if(st > ss && et > ss){
            return Double.parseDouble( df.format( price_night_time * better ) );
        }else{
            return Double.parseDouble( df.format( getBetter( start,
                    stadium.getChange_price_time() ) * price_day_time + getBetter( stadium.getChange_price_time(),
                    end ) * price_night_time ) );
        }
    }

    public double getBetter(Date start,Date end) {
        Calendar s = getDate();
        Calendar e = getDate();
        s.setTime( start );
        e.setTime( end );
        Calendar ss = getDate();
        Calendar ee = getDate();
        ss.set( Calendar.HOUR_OF_DAY,s.get( Calendar.HOUR_OF_DAY ) );
        ss.set( Calendar.MINUTE,s.get( Calendar.MINUTE ) );
        ee.set( Calendar.HOUR_OF_DAY,e.get( Calendar.HOUR_OF_DAY ) );
        ee.set( Calendar.MINUTE,e.get( Calendar.MINUTE ) );

        return (double) ( ee.getTimeInMillis() - ss.getTimeInMillis() ) / 60000;
    }
}
