package koinot.uz.backend.service;

import com.google.gson.Gson;
import koinot.uz.backend.entity.Firebase;
import koinot.uz.backend.entity.Stadium;
import koinot.uz.backend.entity.User;
import koinot.uz.backend.entity.enums.NatificationType;
import koinot.uz.backend.exception.ExceptionSend;
import koinot.uz.backend.payload.DataNatification;
import koinot.uz.backend.payload.RestNotification;
import koinot.uz.backend.repository.FirebaseRepository;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class NotificationService {

    @Autowired
    FirebaseRepository firebaseRepository;

    @Value("${firebase.fcm.server.key}")
    private String firebaseKey;

    @Autowired
    koinot.uz.backend.service.WorkingALot workingALot;
    @Autowired
    ExceptionSend exceptionSend;


    public void notification(String title,String message,Long id,NatificationType natificationType,User user,
                             Stadium stadium,String order) {
        new Thread( () -> {
            try{
                for (Firebase firebase : firebaseRepository.findAllByUser( user )) {
                    Gson gson = new Gson();
                    RestNotification bodyNode = new RestNotification( new DataNatification( title,
                            message.replace( "null","" ),
                            natificationType,
                            gson.toJson( workingALot.generation( stadium ) ),
                            true,
                            id,
                            order ),"/" + firebase.getFirebaseKey() );
                    HttpHeaders headers = new HttpHeaders();
                    headers.setContentType( MediaType.APPLICATION_JSON );
                    headers.set( "Authorization","key=" + firebaseKey );
                    String url = "https://fcm.googleapis.com/fcm/send";
                    RestTemplate template = new RestTemplate();
                    HttpEntity<RestNotification> entity = new HttpEntity<>( bodyNode,headers );
                    template.postForLocation( url,entity );
                }
            }catch(Exception e){
                exceptionSend.senException( "notification => ",e,user );
                log.error( "notification => ",e );
            }
        } ).start();
    }

    public void notificationStadiumBoss(String title,String message,Long id,NatificationType natificationType,User user,
                                        Stadium stadium,boolean status,String order) {
        new Thread( () -> {
            try{
                for (Firebase firebase : firebaseRepository.findAllByUser( user )) {
                    Gson gson = new Gson();
                    RestNotification bodyNode = new RestNotification( new DataNatification( title,
                            message.replace( "null","" ),
                            natificationType,
                            gson.toJson( workingALot.generation( stadium ) ),
                            status,
                            id,
                            order ),"/" + firebase.getFirebaseKey() );
                    HttpHeaders headers = new HttpHeaders();
                    headers.setContentType( MediaType.APPLICATION_JSON );
                    headers.set( "Authorization","key=" + firebaseKey );
                    String url = "https://fcm.googleapis.com/fcm/send";
                    RestTemplate template = new RestTemplate();
                    HttpEntity<RestNotification> entity = new HttpEntity<>( bodyNode,headers );
                    template.postForLocation( url,entity );
                }
            }catch(Exception e){
                exceptionSend.senException( "notification => ",e,user );
                log.error( "notification => ",e );
            }
        } ).start();
    }

    public void allNotification(String title,String message,Long id,NatificationType natificationType) {
        RestNotification bodyNode = new RestNotification( new DataNatification( title,
                message,
                natificationType,
                "koinot",
                true,
                id,
                "" ),"/topics/koinot" );
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType( MediaType.APPLICATION_JSON );
        headers.set( "Authorization","key=" + firebaseKey );
        String url = "https://fcm.googleapis.com/fcm/send";
        RestTemplate template = new RestTemplate();
        HttpEntity<RestNotification> entity = new HttpEntity<>( bodyNode,headers );
        template.postForLocation( url,entity );
        System.out.println( "send mew message" );
    }
}
