package koinot.uz.backend.service;

import koinot.uz.backend.entity.OrdersArchive;
import koinot.uz.backend.entity.Stadium;
import koinot.uz.backend.entity.User;
import koinot.uz.backend.exception.ExceptionSend;
import koinot.uz.backend.payload.ApiResponseModel;
import koinot.uz.backend.payload.Graph;
import koinot.uz.backend.payload.RestMyStadiumOrder;
import koinot.uz.backend.repository.OrderActiveRepository;
import koinot.uz.backend.repository.OrderArchiveRepository;
import koinot.uz.backend.repository.StadiumRepository;
import koinot.uz.backend.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Slf4j
public class StadiumDashService {

    @Autowired
    StadiumRepository stadiumRepository;

    @Autowired
    OrderActiveRepository orderActiveRepository;

    @Autowired
    OrderArchiveRepository orderArchiveRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    StadiumService stadiumService;
    @Autowired
    ExceptionSend exceptionSend;


    private List<Graph> generationGraphs(Stadium stadium,Timestamp startDate,Timestamp endDate) {
        SimpleDateFormat set = new SimpleDateFormat( "MM.dd" );
        SimpleDateFormat e = new SimpleDateFormat( "yyyy.MM.dd" );

        List<OrdersArchive> all = orderArchiveRepository.findAllByStadiumAndCancelOrderAndTimeBetweenOrderByTimeAsc(
                stadium,
                false,
                startDate,
                endDate );
        List<Graph> graphs = new ArrayList<>();
        double sum = 0.0;
        for (int i = 1; i < all.size(); i++) {
            sum = sum + all.get( i - 1 ).getSum();

            if(! e.format( all.get( i - 1 ).getTime() ).equals( e.format( all.get( i ).getTime() ) )){
                graphs.add( new Graph( set.format( all.get( i - 1 ).getTime() ),sum ) );
                sum = 0;
            }
            if(i + 1 == all.size()){
                sum = sum + all.get( i ).getSum();
                graphs.add( new Graph( set.format( all.get( i ).getTime() ),sum ) );
            }

        }
        return graphs;
    }

    private List<RestMyStadiumOrder> generationOrder(List<OrdersArchive> archiveList) {

        SimpleDateFormat day = new SimpleDateFormat( "yyyy-MM-dd" );

        List<RestMyStadiumOrder> list = new ArrayList<>();
        for (OrdersArchive ordersArchive : archiveList) {
            list.add( new RestMyStadiumOrder( ordersArchive.getCreatedAt().getTime(),
                    ordersArchive.getId(),
                    ordersArchive.getUser().getPhoneNumber(),
                    ordersArchive.getUser().getFirstName(),
                    ordersArchive.getUser().getLastName(),
                    ordersArchive.getUser().getLanguage(),
                    ordersArchive.getLatitude(),
                    ordersArchive.getLongitude(),
                    ordersArchive.getSum(),
                    stadiumService.getParsDade( ordersArchive.getStartDate() ),
                    stadiumService.getParsDade( ordersArchive.getEndDate() ),
                    day.format( ordersArchive.getTime() ),
                    ordersArchive.isActive(),
                    ordersArchive.isCancelOrder(),
                    ordersArchive.getCountOrder() ) );
        }
        return list;
    }

    public HttpEntity<ApiResponseModel> graph(User user,Long id,Timestamp startDate,Timestamp endDate) {
        try{
            Optional<Stadium> byId = stadiumRepository.findById( id );
            if(byId.isPresent()){
                Stadium stadium = byId.get();
                if(stadium.getUser().getId().equals( user.getId() )){
                    return ResponseEntity.status( HttpStatus.OK )
                            .body( new ApiResponseModel( HttpStatus.OK.value(),
                                    "here it is",
                                    generationGraphs( stadium,startDate,endDate ) ) );
                }
                return ResponseEntity.status( HttpStatus.FORBIDDEN )
                        .body( new ApiResponseModel( HttpStatus.FORBIDDEN.value(),
                                "you are not the owner of this stadium" ) );
            }
            return ResponseEntity.status( HttpStatus.NOT_FOUND )
                    .body( new ApiResponseModel( HttpStatus.NOT_FOUND.value(),"stadium not found" ) );
        }catch(Exception e){
            exceptionSend.senException( " controller graph => ",e,user );
            log.error( " controller graph => ",e );
            return ResponseEntity.status( HttpStatus.BAD_REQUEST )
                    .body( new ApiResponseModel( HttpStatus.BAD_REQUEST.value(),e.getMessage() ) );
        }
    }

    private Calendar getDate() {
        Calendar calendar = new GregorianCalendar();
        try{
            calendar.setTimeZone( TimeZone.getTimeZone( "Asia/Tashkent" ) );
            return calendar;
        }catch(Exception e){
            return calendar;
        }
    }

    public HttpEntity<ApiResponseModel> archiveAll(User user,Long id) {
        try{
            if(stadiumRepository.existsByIdAndUser( id,user )){
                return ResponseEntity.status( HttpStatus.OK )
                        .body( new ApiResponseModel( HttpStatus.OK.value(),
                                "here it is",
                                generationOrder( orderArchiveRepository.findAllByStadiumIdAndCancelOrderOrderByTimeAsc(
                                        id,
                                        false ) ) ) );
            }
            return ResponseEntity.status( HttpStatus.FORBIDDEN )
                    .body( new ApiResponseModel( HttpStatus.FORBIDDEN.value(),
                            "you are not the owner of this stadium" ) );
        }catch(Exception e){
            exceptionSend.senException( " controller archiveAll => ",e,user );
            log.error( " controller archiveAll => ",e );
            return ResponseEntity.status( HttpStatus.BAD_REQUEST )
                    .body( new ApiResponseModel( HttpStatus.BAD_REQUEST.value(),e.getMessage() ) );
        }
    }

    public HttpEntity<ApiResponseModel> archiveAfterCreateTime(User user,Long id,Long time) {
        try{
            if(stadiumRepository.existsByIdAndUser( id,user )){
                return ResponseEntity.status( HttpStatus.OK )
                        .body( new ApiResponseModel( HttpStatus.OK.value(),
                                "here it is",
                                generationOrder( orderArchiveRepository.findAllByStadiumIdAndCreatedAtIsAfterAndCancelOrderAndCreatedAtNotOrderByCreatedAtAsc(
                                        id,
                                        new Timestamp( time ),
                                        false,
                                        new Timestamp( time ) ) ) ) );
            }
            return ResponseEntity.status( HttpStatus.FORBIDDEN )
                    .body( new ApiResponseModel( HttpStatus.FORBIDDEN.value(),
                            "you are not the owner of this stadium" ) );
        }catch(Exception e){
            exceptionSend.senException( " controller archiveAfterCreateTime => ",e,user );
            log.error( " controller archiveAfterCreateTime => ",e );
            return ResponseEntity.status( HttpStatus.BAD_REQUEST )
                    .body( new ApiResponseModel( HttpStatus.BAD_REQUEST.value(),e.getMessage() ) );
        }
    }
}
