package koinot.uz.backend.service;

import koinot.uz.backend.bot.BRBT_Bot;
import koinot.uz.backend.entity.Stadium;
import koinot.uz.backend.entity.User;
import koinot.uz.backend.entity.enums.AttachmentTypeEnumWhy;
import koinot.uz.backend.entity.enums.UserState;
import koinot.uz.backend.entity.enums.UserType;
import koinot.uz.backend.exception.ExceptionSend;
import koinot.uz.backend.payload.RegionLocation;
import koinot.uz.backend.repository.StadiumPhotoRepository;
import koinot.uz.backend.repository.StadiumRepository;
import koinot.uz.backend.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.HashMap;
import java.util.regex.Pattern;

@Service
@Slf4j
public class GetStadium {

    @Autowired
    UserRepository userRepository;

    @Autowired
    BRBT_Bot BRBTBot;

    @Autowired
    koinot.uz.backend.service.WorkingALot workingALot;

    @Autowired
    StadiumPhotoRepository stadiumPhotoRepository;

    @Autowired
    StadiumRepository stadiumRepository;

    @Autowired
    ExceptionSend exceptionSend;

    @Autowired
    OrderMoreWorking orderMoreWorking;


    public void getStadium(Update update, User user, String data, String language) {
        try {
            user.setState(UserState.CLIENT_SEND_LOCATION);
            userRepository.save(user);
            workingALot.getClientLocation(language,
                    update.getCallbackQuery().getMessage().getChatId(),
                    update.getCallbackQuery().getMessage().getMessageId(), (user.getFirstName().equals("null") ? user.getLastName() : user.getFirstName()));
            SendMessage sendMessage = new SendMessage().setChatId(String.valueOf(user.getTelegramId()))
                    .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.ALERT_CLIENT_LOCATION_UZ : language.equals(
                            BotAnswerString.ru) ? BotAnswerString.ALERT_CLIENT_LOCATION_RU : language.equals(
                            BotAnswerString.uzl) ? BotAnswerString.ALERT_CLIENT_LOCATION_UZL : BotAnswerString.ALERT_CLIENT_LOCATION_EN);
            BRBTBot.execute(sendMessage);
        } catch (Exception e) {
            exceptionSend.senException("change get stadium => ", e, user);
            log.error("change get stadium => ", e);
        }

    }

    public void getLocation(Update update, User user, String language) {
        try {
            Message message = update.getMessage();
            double longitude = message.getLocation().getLongitude();
            double latitude = message.getLocation().getLatitude();
            user.setLatitude(latitude);
            user.setLongitude(longitude);
            userRepository.save(user);
            workingALot.generateListStadium(longitude, latitude, 0, 1, user.getLanguage(), user.getTelegramId());
        } catch (Exception e) {
            exceptionSend.senException("client get location => ", e, user);
            log.error("client get location => ", e);
        }

    }

    public void prevSelectStadium(int value, Integer chat_id, User user, Update update) {
        try {
            if (value != 0) {
                workingALot.generateListStadiumEdit(user.getLongitude(),
                        user.getLatitude(),
                        value - 1,
                        value,
                        user.getLanguage(),
                        update.getCallbackQuery().getMessage().getMessageId(),
                        chat_id);
            }
        } catch (Exception e) {
            exceptionSend.senException("prev select stadium => ", e, user);
            log.error("prev select stadium => ", e);
        }
    }

    public void nextSelectStadium(int value, Integer chat_id, User user, Update update) {
        try {
            workingALot.generateListStadiumEdit(user.getLongitude(),
                    user.getLatitude(),
                    value,
                    value + 1,
                    user.getLanguage(),
                    update.getCallbackQuery().getMessage().getMessageId(),
                    chat_id);
        } catch (Exception e) {
            exceptionSend.senException("next select stadium => ", e, user);
            log.error("next select stadium => ", e);
        }

    }

    public void getClientStadium(Stadium stadium, User user, Update update, String language) {
        try {
            workingALot.getStadiumInfoClient(language, user, stadium);
        } catch (Exception e) {
            exceptionSend.senException("change price nightlight => ", e, user);
            log.error("change price nightlight => ", e);
        }
    }

    public void cancelClient(Stadium stadium, User user, Update update, String language) {
        try {
            int message_id = update.getCallbackQuery().getMessage().getMessageId();
            int countPhoto = stadiumPhotoRepository.countStadiumPhotoByStadiumAndWhy(stadium,
                    AttachmentTypeEnumWhy.STADIUM_PHOTO);
            for (int i = 0; i < countPhoto + 2; i++) {
                workingALot.deleteMsg(Long.valueOf(user.getTelegramId()), message_id - i);
            }
        } catch (Exception e) {
            exceptionSend.senException("cancel client stadium delete message => ", e, user);
            log.error("cancel client stadium delete message => ", e);
        }

    }

    public void getPhoneClient(User user, Update update, String language) {
        try {
            user.setState(UserState.CLIENT_SEND_PHONE_NUMBER);
            userRepository.save(user);
            workingALot.getClientPhoNumberButton(language, update.getCallbackQuery().getMessage().getChatId());
        } catch (Exception e) {
            exceptionSend.senException("get client phone number btn => ", e, user);
            log.error("get client phone number btn => ", e);
        }

    }

    public void getClientPhone(Update update, User user, String language) {
        try {
            User user1 = user;
            long chat_id = user.getTelegramId();
            if ((update.getMessage().hasContact() || (update.getMessage().hasText()) && Pattern.compile(
                            "^[+][9][9][8][0-9]{9}$")
                    .matcher(update.getMessage().hasContact() ? update.getMessage()
                            .getContact()
                            .getPhoneNumber() : update.getMessage().getText())
                    .matches())) {
                if (update.getMessage().hasContact()) {
                    String tel = update.getMessage().getContact().getPhoneNumber();
                    if (userRepository.existsAllByPhoneNumber(tel.startsWith("+") ? tel : "+" + tel)) {
                        User u = userRepository.findAllByPhoneNumber(tel.startsWith("+") ? tel : "+" + tel);
                        if (u.getTelegramId() == null && u.getFirstName().equals("null") && u.getLastName()
                                .equals("null")) {
                            user = u;
                            user.setTelegramId(user1.getTelegramId());
                            userRepository.delete(user1);
                        }
                    }
                    user.setLastName(update.getMessage()
                            .getFrom()
                            .getFirstName() == null ? "null" : update.getMessage().getFrom().getFirstName());
                    user.setFirstName(update.getMessage()
                            .getFrom()
                            .getLastName() == null ? "null" : update.getMessage().getFrom().getLastName());
                    user.setPhoneNumber(tel.startsWith("+") ? tel : "+" + tel);
                } else {
                    String tel = update.getMessage().getText();
                    if (userRepository.existsAllByPhoneNumber(tel.startsWith("+") ? tel : "+" + tel)) {
                        User u = userRepository.findAllByPhoneNumber(tel.startsWith("+") ? tel : "+" + tel);
                        if (u.getTelegramId() == null) {
                            user = u;
                            user.setTelegramId(user1.getTelegramId());
                            userRepository.delete(user1);
                        }
                    }
                    user.setPhoneNumber(tel.startsWith("+") ? tel : "+" + tel);
                }
                workingALot.deleteMsg(chat_id, update.getMessage().getMessageId());
                workingALot.deleteMsg(chat_id, update.getMessage().getMessageId() - 1);
                user.setType(UserType.USER);
                user.setState(UserState.CLIENT_SEND_LOCATION);
                userRepository.save(user);
                if (user.getLongitude() != 0) {
                    SendMessage sendMessage = new SendMessage().setChatId(chat_id)
                            .setText(language.equals(BotAnswerString.uz) ? BotAnswerString.SEND_CLIENT_CAN_ORDER_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.SEND_CLIENT_CAN_ORDER_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.SEND_CLIENT_CAN_ORDER_UZL : BotAnswerString.SEND_CLIENT_CAN_ORDER_EN);
                    BRBTBot.execute(sendMessage);
                } else {
                    orderMoreWorking.getRegionBtn(chat_id,
                            language,
                            (language.equals(BotAnswerString.uz) ? BotAnswerString.HELLO_MR_UZ : language.equals(
                                    BotAnswerString.ru) ? BotAnswerString.HELLO_MR_RU : language.equals(
                                    BotAnswerString.uzl) ? BotAnswerString.HELLO_MR_UZL : BotAnswerString.HELLO_MR_EN) + (user.getFirstName().equals("null") ? user.getLastName() : user.getFirstName()));
                }
            } else {
                workingALot.getClientPhoNumberButton(language, chat_id);
            }

        } catch (TelegramApiException e) {
            exceptionSend.senException("get client phone number and save => ", e, user);
            log.error("get client phone number and save => ", e);
        }
    }

    public void selectRegion(Update update, User user, String language) {
        try {
            String text = update.getMessage().getText();
            HashMap<String, RegionLocation> region = workingALot.getRegion();
            double longitude = region.get(text).getLongitude();
            double latitude = region.get(text).getLatitude();
            SendMessage sendMessage = new SendMessage().setChatId(String.valueOf(user.getTelegramId()))
                    .setText(language.equals(BotAnswerString.uz) ? text + BotAnswerString.REGION_NAME_UZ : language.equals(
                            BotAnswerString.ru) ? BotAnswerString.REGION_NAME_RU + text : language.equals(
                            BotAnswerString.uzl) ? text + BotAnswerString.REGION_NAME_UZL : BotAnswerString.REGION_NAME_EN + text);
            BRBTBot.execute(sendMessage);
            user.setLatitude(latitude);
            user.setLongitude(longitude);
            userRepository.save(user);
            workingALot.generateListStadium(longitude, latitude, 0, 1, user.getLanguage(), user.getTelegramId());
        } catch (Exception e) {
            exceptionSend.senException("select region => ", e, user);
            log.error("select region => ", e);
        }

    }
}
