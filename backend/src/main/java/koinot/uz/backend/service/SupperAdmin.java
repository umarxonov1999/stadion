package koinot.uz.backend.service;

import koinot.uz.backend.bot.BRBT_Bot;
import koinot.uz.backend.entity.Stadium;
import koinot.uz.backend.entity.StadiumPhoto;
import koinot.uz.backend.entity.User;
import koinot.uz.backend.entity.enums.AttachmentTypeEnumWhy;
import koinot.uz.backend.entity.enums.NatificationType;
import koinot.uz.backend.entity.enums.UserType;
import koinot.uz.backend.exception.ExceptionSend;
import koinot.uz.backend.repository.StadiumPhotoRepository;
import koinot.uz.backend.repository.StadiumRepository;
import koinot.uz.backend.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.ActionType;
import org.telegram.telegrambots.meta.api.methods.send.SendChatAction;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.File;
import java.util.List;

@Service
@Slf4j
public class SupperAdmin {

    @Autowired
    UserRepository userRepository;

    @Autowired
    BRBT_Bot BRBTBot;

    @Autowired
    koinot.uz.backend.service.WorkingALot workingALot;

    @Autowired
    StadiumPhotoRepository stadiumPhotoRepository;

    @Autowired
    StadiumRepository stadiumRepository;


    @Autowired
    NotificationService notificationService;

    @Autowired
    ExceptionSend exceptionSend;


    public void activeStadium(Update update,User user,Stadium stadium) {
        try{
            stadium.setActive( true );
            String language = stadium.getUser().getLanguage();
            String text = language.equals( BotAnswerString.uz ) ? BotAnswerString.ADMIN_VERIFY_UZ : language.equals(
                    BotAnswerString.ru ) ? BotAnswerString.ADMIN_VERIFY_RU : language.equals( BotAnswerString.uzl ) ? BotAnswerString.ADMIN_VERIFY_UZL : BotAnswerString.ADMIN_VERIFY_EN;
            sendMessageAdmin( stadium.getUser().getTelegramId(),text );
            stadiumRepository.save( stadium );
            notificationService.notificationStadiumBoss( BotAnswerString.KOINOT,
                    text,
                    stadium.getId(),
                    NatificationType.ALL,
                    stadium.getUser(),
                    stadium,
                    true,
                    null );
            workingALot.sendAdminsAlert( user,text );
        }catch(Exception e){
            exceptionSend.senException( "supper admin active stadium => ",e,user );
            log.error( "supper admin active stadium => ",e );
        }
    }

    public void deleteStadium(Update update,User user,Stadium stadium) {
        try{
            User userStadium = stadium.getUser();
            userStadium.setActiveStadium( null );
            for (StadiumPhoto stadiumPhoto : stadiumPhotoRepository.findByStadiumIdAndWhyOrderByCreatedAtDesc( stadium.getId(),
                    AttachmentTypeEnumWhy.STADIUM_PHOTO )) {
                stadiumPhotoRepository.delete( stadiumPhoto );
            }
            stadiumRepository.delete( stadium );

            if(! stadiumRepository.existsByUser( userStadium )){
                user.setType( UserType.USER );
            }
            userRepository.save( userStadium );

            String language = userStadium.getLanguage();
            String text = language.equals( BotAnswerString.uz ) ? BotAnswerString.ADMIN_DELETE_UZ : language.equals(
                    BotAnswerString.ru ) ? BotAnswerString.ADMIN_DELETE_RU : language.equals( BotAnswerString.uzl ) ? BotAnswerString.ADMIN_DELETE_UZL : BotAnswerString.ADMIN_DELETE_EN;
            sendMessageAdmin( stadium.getUser().getTelegramId(),text );
            notificationService.notificationStadiumBoss( BotAnswerString.KOINOT,
                    text,
                    stadium.getId(),
                    NatificationType.ALL,
                    stadium.getUser(),
                    stadium,
                    false,
                    null );
            workingALot.sendAdminsAlert( user,text );
        }catch(Exception e){
            exceptionSend.senException( "supper admin delete stadium => ",e,user );
            log.error( "supper admin delete stadium => ",e );
        }


    }

    public void warningWorkingHours(Update update,User user,Stadium stadium) {
        try{
            String language = stadium.getUser().getLanguage();
            String text = language.equals( BotAnswerString.uz ) ? BotAnswerString.ADMIN_WORKING_HOURS_UZ : language.equals(
                    BotAnswerString.ru ) ? BotAnswerString.ADMIN_WORKING_HOURS_RU : language.equals( BotAnswerString.uzl ) ? BotAnswerString.ADMIN_WORKING_HOURS_UZL : BotAnswerString.ADMIN_WORKING_HOURS_EN;
            sendMessageAdmin( stadium.getUser().getTelegramId(),text );
            notificationService.notificationStadiumBoss( BotAnswerString.KOINOT,
                    text,
                    stadium.getId(),
                    NatificationType.ALL,
                    stadium.getUser(),
                    stadium,
                    false,
                    null );
            workingALot.sendAdminsAlert( user,text );
        }catch(Exception e){
            exceptionSend.senException( "supper admin warningWorkingHours => ",e,user );
            log.error( "supper admin warningWorkingHours => ",e );
        }


    }

    public void warningPrice(Update update,User user,Stadium stadium) {
        try{
            String language = stadium.getUser().getLanguage();
            String text = language.equals( BotAnswerString.uz ) ? BotAnswerString.ADMIN_PRICE_UZ : language.equals(
                    BotAnswerString.ru ) ? BotAnswerString.ADMIN_PRICE_RU : language.equals( BotAnswerString.uzl ) ? BotAnswerString.ADMIN_PRICE_UZL : BotAnswerString.ADMIN_PRICE_EN;
            sendMessageAdmin( stadium.getUser().getTelegramId(),text );
            notificationService.notificationStadiumBoss( BotAnswerString.KOINOT,
                    text,
                    stadium.getId(),
                    NatificationType.ALL,
                    stadium.getUser(),
                    stadium,
                    false,
                    null );
            workingALot.sendAdminsAlert( user,text );
        }catch(Exception e){
            exceptionSend.senException( "supper admin price => ",e,user );
            log.error( "supper admin price => ",e );
        }


    }

    public void warningLocation(Update update,User user,Stadium stadium) {
        try{
            String language = stadium.getUser().getLanguage();
            String text = language.equals( BotAnswerString.uz ) ? BotAnswerString.ADMIN_LOCATION_UZ : language.equals(
                    BotAnswerString.ru ) ? BotAnswerString.ADMIN_LOCATION_RU : language.equals( BotAnswerString.uzl ) ? BotAnswerString.ADMIN_LOCATION_UZL : BotAnswerString.ADMIN_LOCATION_EN;
            sendMessageAdmin( stadium.getUser().getTelegramId(),text );
            notificationService.notificationStadiumBoss( BotAnswerString.KOINOT,
                    text,
                    stadium.getId(),
                    NatificationType.ALL,
                    stadium.getUser(),
                    stadium,
                    false,
                    null );
            workingALot.sendAdminsAlert( user,text );
        }catch(Exception e){
            exceptionSend.senException( "supper admin location => ",e,user );
            log.error( "supper admin location => ",e );
        }


    }

    public void warningName(Update update,User user,Stadium stadium) {
        try{
            String language = stadium.getUser().getLanguage();
            String text = language.equals( BotAnswerString.uz ) ? BotAnswerString.ADMIN_NAME_UZ : language.equals(
                    BotAnswerString.ru ) ? BotAnswerString.ADMIN_NAME_RU : language.equals( BotAnswerString.uzl ) ? BotAnswerString.ADMIN_NAME_UZL : BotAnswerString.ADMIN_NAME_EN;
            sendMessageAdmin( stadium.getUser().getTelegramId(),text );
            notificationService.notificationStadiumBoss( BotAnswerString.KOINOT,
                    text,
                    stadium.getId(),
                    NatificationType.ALL,
                    stadium.getUser(),
                    stadium,
                    false,
                    null );
            workingALot.sendAdminsAlert( user,text );
        }catch(Exception e){
            exceptionSend.senException( "supper admin  name => ",e,user );
            log.error( "supper admin  name => ",e );
        }


    }

    public void warningChangePrice(Update update,User user,Stadium stadium) {
        try{
            String language = stadium.getUser().getLanguage();
            String text = language.equals( BotAnswerString.uz ) ? BotAnswerString.ADMIN_TIME_FOR_CHANGE_PRICE_UZ : language.equals(
                    BotAnswerString.ru ) ? BotAnswerString.ADMIN_TIME_FOR_CHANGE_PRICE_RU : language.equals(
                    BotAnswerString.uzl ) ? BotAnswerString.ADMIN_TIME_FOR_CHANGE_PRICE_UZL : BotAnswerString.ADMIN_TIME_FOR_CHANGE_PRICE_EN;
            sendMessageAdmin( stadium.getUser().getTelegramId(),text );
            notificationService.notificationStadiumBoss( BotAnswerString.KOINOT,
                    text,
                    stadium.getId(),
                    NatificationType.ALL,
                    stadium.getUser(),
                    stadium,
                    false,
                    null );
            workingALot.sendAdminsAlert( user,text );
        }catch(Exception e){
            exceptionSend.senException( "supper admin change price => ",e,user );
            log.error( "supper admin change price => ",e );
        }


    }

    public void warningPhoto(Update update,User user,Stadium stadium) {
        try{
            String language = stadium.getUser().getLanguage();
            String text = language.equals( BotAnswerString.uz ) ? BotAnswerString.ADMIN_PICTURE_UZ : language.equals(
                    BotAnswerString.ru ) ? BotAnswerString.ADMIN_PICTURE_RU : language.equals( BotAnswerString.uzl ) ? BotAnswerString.ADMIN_PICTURE_UZL : BotAnswerString.ADMIN_PICTURE_EN;
            sendMessageAdmin( stadium.getUser().getTelegramId(),text );
            notificationService.notificationStadiumBoss( BotAnswerString.KOINOT,
                    text,
                    stadium.getId(),
                    NatificationType.ALL,
                    stadium.getUser(),
                    stadium,
                    false,
                    null );
            workingALot.sendAdminsAlert( user,text );
        }catch(Exception e){
            exceptionSend.senException( "supper admin photo => ",e,user );
            log.error( "supper admin photo => ",e );
        }


    }

    public void warningAddress(Update update,User user,Stadium stadium) {
        try{
            String language = stadium.getUser().getLanguage();
            String text = language.equals( BotAnswerString.uz ) ? BotAnswerString.ADMIN_ADDRESS_UZ : language.equals(
                    BotAnswerString.ru ) ? BotAnswerString.ADMIN_ADDRESS_RU : language.equals( BotAnswerString.uzl ) ? BotAnswerString.ADMIN_ADDRESS_UZL : BotAnswerString.ADMIN_ADDRESS_EN;
            sendMessageAdmin( stadium.getUser().getTelegramId(),text );
            notificationService.notificationStadiumBoss( BotAnswerString.KOINOT,
                    text,
                    stadium.getId(),
                    NatificationType.ALL,
                    stadium.getUser(),
                    stadium,
                    false,
                    null );
            workingALot.sendAdminsAlert( user,text );
        }catch(Exception e){
            exceptionSend.senException( "supper admin address => ",e,user );
            log.error( "supper admin address => ",e );
        }


    }

    public void warningPhoneNumber(Update update,User user,Stadium stadium) {
        try{
            String language = stadium.getUser().getLanguage();
            String text = language.equals( BotAnswerString.uz ) ? BotAnswerString.ADMIN_PHONE_UZ : language.equals(
                    BotAnswerString.ru ) ? BotAnswerString.ADMIN_PHONE_RU : language.equals( BotAnswerString.uzl ) ? BotAnswerString.ADMIN_PHONE_UZL : BotAnswerString.ADMIN_PHONE_EN;
            sendMessageAdmin( stadium.getUser().getTelegramId(),text );
            notificationService.notificationStadiumBoss( BotAnswerString.KOINOT,
                    text,
                    stadium.getId(),
                    NatificationType.ALL,
                    stadium.getUser(),
                    stadium,
                    false,
                    null );
            workingALot.sendAdminsAlert( user,text );
        }catch(Exception e){
            exceptionSend.senException( "supper admin phone number => ",e,user );
            log.error( "supper admin phone number => ",e );
        }
    }

    public void warningSizeStadium(Update update,User user,Stadium stadium) {
        try{
            String language = stadium.getUser().getLanguage();
            String text = language.equals( BotAnswerString.uz ) ? BotAnswerString.ADMIN_SIZE_UZ : language.equals(
                    BotAnswerString.ru ) ? BotAnswerString.ADMIN_SIZE_RU : language.equals( BotAnswerString.uzl ) ? BotAnswerString.ADMIN_SIZE_UZL : BotAnswerString.ADMIN_SIZE_EN;
            sendMessageAdmin( stadium.getUser().getTelegramId(),text );
            notificationService.notificationStadiumBoss( BotAnswerString.KOINOT,
                    text,
                    stadium.getId(),
                    NatificationType.ALL,
                    stadium.getUser(),
                    stadium,
                    false,
                    null );
            workingALot.sendAdminsAlert( user,text );
        }catch(Exception e){
            exceptionSend.senException( "supper admin size stadium => ",e,user );
            log.error( "supper admin size stadium => ",e );
        }


    }

    public void sendStadiumPhoto(long chat_id) {
        try{
            SendChatAction sendChatAction = new SendChatAction();
            sendChatAction.setChatId( chat_id );
            sendChatAction.setAction( ActionType.UPLOADDOCUMENT );
            BRBTBot.execute( sendChatAction );
            List<StadiumPhoto> stadiumPhotos = stadiumPhotoRepository.findAllByWhy( AttachmentTypeEnumWhy.DELETE_STADIUM_PHOTO );
            for (StadiumPhoto stadiumPhoto : stadiumPhotos) {
                String id;
                if(stadiumPhoto.getPhoto() == null){
                    id = workingALot.sendImageFromUrl( stadiumPhoto.getPath(),String.valueOf( chat_id ) );
                    if(id == null){
                        System.err.println( "" );
                        continue;
                    }
                }else{
                    id = stadiumPhoto.getPhoto();
                }
                sendImageFromFileId( id,
                        String.valueOf( chat_id ),
                        "user " + stadiumPhoto.getUser()
                                .getPhoneNumber() + System.lineSeparator() + "stadium name " + stadiumPhoto.getStadium()
                                .getName() );
                File myObj = new File( stadiumPhoto.getPath() );
                if(myObj.delete()){
                    System.out.println( "Deleted the folder: " + myObj.getName() );
                }else{
                    System.out.println( "Failed to delete the folder." );
                }
            }
            stadiumPhotoRepository.deleteAll( stadiumPhotos );
        }catch(Exception e){
            exceptionSend.senException( "supper admin de => ",e,null );
            log.error( "supper admin de => ",e );
        }

    }

    public void sendImageFromFileId(String fileId,String chatId,String cap) {
        // Create send method
        SendPhoto sendPhotoRequest = new SendPhoto();
        // Set destination chat id
        sendPhotoRequest.setChatId( chatId );
        // Set the photo url as a simple photo
        sendPhotoRequest.setPhoto( new InputFile( fileId ) );
        sendPhotoRequest.setCaption( cap );
        try{
            // Execute the method
            BRBTBot.execute( sendPhotoRequest );
        }catch(TelegramApiException e){
            exceptionSend.senException( "supper admin send image from file id => ",e,null );
            log.error( "supper admin send image from file id => ",e );
        }
    }

    public void sendMessageAdmin(Integer chat_id,String text) {
        try{
            if(chat_id != null){
                SendMessage sendMessage = new SendMessage().setChatId( String.valueOf( chat_id ) ).setText( text );
                BRBTBot.execute( sendMessage );
            }
        }catch(Exception e){
            exceptionSend.senException( "supper admin send admin message => ",e,null );
            log.error( "supper admin send admin message => ",e );
        }

    }

    public void alertSupperAdmin(Stadium stadium) {
        try{
            for (User user1 : userRepository.findAllByType( UserType.SUPPER_ADMIN )) {
                workingALot.adminMessage( user1.getLanguage(),Long.valueOf( user1.getTelegramId() ),stadium );
            }
        }catch(Exception e){
            exceptionSend.senException( "supper admin alert Supper Admin => ",e,null );
            log.error( "supper admin alert Supper Admin => ",e );
        }
    }
}
