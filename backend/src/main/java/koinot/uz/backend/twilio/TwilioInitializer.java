package koinot.uz.backend.twilio;

import com.twilio.Twilio;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class TwilioInitializer {


    private final TwilioConfiguration twilioConfiguration;

    @Autowired
    public TwilioInitializer(TwilioConfiguration twilioConfiguration) {
        this.twilioConfiguration = twilioConfiguration;
        Twilio.init( twilioConfiguration.getAccountSit(),twilioConfiguration.getAuthToken() );
        log.info( "twilio initialized with account sit " + twilioConfiguration.getAccountSit() );
    }
}
