package koinot.uz.backend.Test;

import java.io.*;
import java.net.Socket;

/**
 * @className: Client   $
 * @description: TODO
 * @date: 17 October 2021 year $
 * @time: 22:24 $
 * @author: Qudratjon Komilov
 */
public class Client {

    private static Socket clientSocket;
    private static BufferedReader in;
    private static BufferedWriter out;

    public static void main(String[] args) {
        while (true) {
            try {
                try {
                    clientSocket = new Socket("localhost" , 1999);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                    in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                    out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

                    System.out.println("formulaga qo'yish uchun son kiriting ");

                    String word = reader.readLine();
                    out.write(word + "\n");
                    out.flush();
                    String serverWord = in.readLine();
                    System.err.println(serverWord);
                } finally {
                    System.out.println("natija olindi va soket yopildi");
                    clientSocket.close();
                    in.close();
                    out.close();
                }
            } catch (IOException e) {
                System.err.println(e);
            }

        }
    }
}
