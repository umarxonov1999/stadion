package koinot.uz.backend.Test;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @className: Server   $
 * @description: TODO
 * @date: 17 October 2021 year $
 * @time: 22:23 $
 * @author: Qudratjon Komilov
 */
public class Server {
    /*
     * o'zgaruvchilarni elon qilamiz
     * */
    private static ServerSocket server; // server socket
    private static BufferedReader in; // socketda kelgan malumotni o'qish uchun BufferedReader dan foydalanamiz chunki malumotlar stringda keladi
    private static BufferedWriter out; // malumotni yozish uchunBufferedWriter dan foydalanamiz

    public static void main(String[] args) {
        while (true) {
            try {
                try {
                    server = new ServerSocket(1999); // serverni ishga tushirganimizda 1999 portni estadi
                    System.out.println("server eshitishni boshladi..."); // server eshitishni boshladi...

                    //aloqa uchu yo'l ochamiz...
                    Socket clientSocket = server.accept(); // portni tasdiqlaymiz..
                    try {
                        // kelgan malumotni o'qiymiz...
                        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                        // yozishni boshlaymiz
                        out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

                        String word = in.readLine(); // yozilgan malumotni olamiz
                        System.out.println("bizga manshu keldi " + word);

                        out.write("4/3 * PI * r^3 formula bo'yicha sizni natijangiz... " + (4 / 3) * Math.PI * Math.pow(
                                new Double(word) ,
                                3) + "\n");

                        out.flush(); // buferimizni tozalaymiz

                    } finally {
                        // socketni o'chramiz
                        clientSocket.close();
                        // hamma buferlarni o'chiramiz
                        in.close();
                        out.close();
                    }
                } finally {
                    System.out.println("serverni ham o'chiramiz");
                    server.close();
                }
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        }
    }
}
