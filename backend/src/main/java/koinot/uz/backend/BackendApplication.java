package koinot.uz.backend;

import koinot.uz.backend.exception.ExceptionSend;
import koinot.uz.backend.repository.UserRepository;
import koinot.uz.backend.service.OrderMoreWorking;
import koinot.uz.backend.service.SupperAdmin;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.telegram.telegrambots.ApiContextInitializer;


@SpringBootApplication
@EnableScheduling
@Slf4j
public class BackendApplication {

    @Autowired
    UserRepository userRepository;

    @Autowired
    SupperAdmin supperAdmin;

    @Autowired
    OrderMoreWorking orderMoreWorking;

    @Autowired
    ExceptionSend exceptionSend;

    private static ConfigurableApplicationContext context;

    public static void main(String[] args) {
        ApiContextInitializer.init();
        context = SpringApplication.run( BackendApplication.class,args );
    }

    public static void restart() {
        System.gc();
    }

    @Scheduled(fixedDelay = 45000L)
    public void afterPlay() {
        orderMoreWorking.afterPlay();
    }


    @Scheduled(cron = "0 0 3 * * *", zone = "Asia/Tashkent")
    public void photoDelete() {

        exceptionSend.sendErrorFileNohup();


        try{
            supperAdmin.sendStadiumPhoto( - 1001540481206L );
        }catch(Exception e){
            exceptionSend.senException( " photoDelete cron => ",e,null );
            log.error( " photoDelete cron => ",e );
        }

        restart();

    }
}
