package koinot.uz.backend.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.function.Predicate;

@Configuration
@EnableSwagger2
@EnableWebMvc
public class SpringFoxConfig extends WebMvcConfigurerAdapter {

    @Bean
    public Docket api() {
        return new Docket( DocumentationType.SWAGGER_2 ).apiInfo( apiInfo() )
                .select()
                .apis( RequestHandlerSelectors.any() )
                .paths( PathSelectors.any() )
                .paths( Predicate.not( PathSelectors.regex( "/error.*" ) ) )
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfo( "BRBT",
                "BRBT api documentation... CREATED BY KOINOT",
                "1.0",
                "https://www.figma.com/file/p5l4gjQy0trqopG9apk5Fc/BRBT?node-id=0%3A1",
                new Contact( "Team","http://brbt.uz/team","komilovqudratjon@gmail.com" ),
                "Api deside youtube video",
                "http://brbt.uz",
                Collections.emptyList() );
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler( "swagger-ui.html" ).addResourceLocations( "classpath:/META-INF/resources/" );

        registry.addResourceHandler( "/webjars/**" ).addResourceLocations( "classpath:/META-INF/resources/webjars/" );
    }

}
