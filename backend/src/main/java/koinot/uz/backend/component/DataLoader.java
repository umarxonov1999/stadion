package koinot.uz.backend.component;

import koinot.uz.backend.entity.OrdersActive;
import koinot.uz.backend.entity.Role;
import koinot.uz.backend.entity.Stadium;
import koinot.uz.backend.entity.User;
import koinot.uz.backend.entity.enums.RoleName;
import koinot.uz.backend.entity.enums.UserState;
import koinot.uz.backend.entity.enums.UserType;
import koinot.uz.backend.repository.OrderActiveRepository;
import koinot.uz.backend.repository.RoleRepository;
import koinot.uz.backend.repository.StadiumRepository;
import koinot.uz.backend.repository.UserRepository;
import koinot.uz.backend.service.BotAnswerString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@Component
public class DataLoader implements CommandLineRunner {

    @Value("${spring.sql.init.mode}")
    private String initialMode;

    @Autowired
    UserRepository userRepository;

    @Autowired
    StadiumRepository stadiumRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    OrderActiveRepository orderActiveRepository;

    @Override
    public void run(String... args) {

        if(initialMode.equals( "always" )){

            roleRepository.save( new Role( 1,RoleName.USER ) );
            roleRepository.save( new Role( 2,RoleName.ADMIN ) );
            roleRepository.save( new Role( 3,RoleName.SUPPER_ADMIN ) );

            userRepository.save( new User( 520405728,
                    "Qudratjon",
                    "Komilov",
                    UserState.DEFAULT,
                    UserType.SUPPER_ADMIN,
                    BotAnswerString.uzl,
                    "+998917797278" ) );

//            userRepository.save( new User( 849229680,
//                    "Asadbek",
//                    "Ahmadov",
//                    UserState.DEFAULT,
//                    UserType.SUPPER_ADMIN,
//                    BotAnswerString.uzl,
//                    "+998916469923" ) );

            // 747505857 koinot
            // 281805829 stadium
            User koinot = userRepository.save( new User( 281805829,
                    "Saidaziz",
                    "Umarov",
                    UserState.DEFAULT,
                    UserType.ADMIN,
                    BotAnswerString.uzl,
                    "+998977772109" ) );

            Calendar calendar = new GregorianCalendar();

            Stadium saveStadium = null;

            for (int i = 1; i <= 4; i++) {

                Stadium stadium = new Stadium();
                stadium.setUser( koinot );
                stadium.setName( "tanho" + i );
                stadium.setPhone_number( "+998977772109" );
                if(i == 1){
                    stadium.setLatitude( 41.300981312396026 );
                    stadium.setLongitude( 69.20899435696141 );
                }
                if(i == 2){
                    stadium.setLatitude( 41.3009579033351 );
                    stadium.setLongitude( 69.2082257430306 );
                }
                if(i == 3){
                    stadium.setLatitude( 41.30095010031291 );
                    stadium.setLongitude( 69.207768729342 );
                }
                if(i == 4){
                    stadium.setLatitude( 41.30093789680791 );
                    stadium.setLongitude( 69.20694648119873 );
                }

                stadium.setAddress( "Toshkent viloyati shayxontohur tumani Gulxaniy ko'chasi Anhor yonida" );
                calendar.set( Calendar.HOUR_OF_DAY,7 );
                calendar.set( Calendar.MINUTE,0 );
                stadium.setOpening_time( calendar.getTime() );
                calendar.set( Calendar.HOUR_OF_DAY,23 );
                calendar.set( Calendar.MINUTE,0 );
                stadium.setClosing_time( calendar.getTime() );
                stadium.setStadium_like( 0 );
                calendar.set( Calendar.HOUR_OF_DAY,19 );
                calendar.set( Calendar.MINUTE,0 );
                stadium.setChange_price_time( calendar.getTime() );
                stadium.setPrice_day_time( 100000 );
                stadium.setPrice_night_time( 120000 );
                stadium.setWidth( 20 );
                stadium.setHeight( 40 );
                stadium.setCount_order( 0 );
                stadium.setActive( true );
                stadium.setVerify( true );
                stadiumRepository.save( stadium );
            }


//            orderActiveRepository.saveAll( List.of( new OrdersActive( saveStadium,
//                            65468415,
//                            8747487,
//                            65456,
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ) ),
//                    new OrdersActive( saveStadium,
//                            65468415,
//                            8747487,
//                            65456,
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ) ),
//                    new OrdersActive( saveStadium,
//                            65468415,
//                            8747487,
//                            65456,
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ) ),
//                    new OrdersActive( saveStadium,
//                            65468415,
//                            8747487,
//                            65456,
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ) ),
//                    new OrdersActive( saveStadium,
//                            65468415,
//                            8747487,
//                            65456,
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ) ),
//                    new OrdersActive( saveStadium,
//                            65468415,
//                            8747487,
//                            65456,
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ) ),
//                    new OrdersActive( saveStadium,
//                            65468415,
//                            8747487,
//                            65456,
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ) ),
//                    new OrdersActive( saveStadium,
//                            65468415,
//                            8747487,
//                            65456,
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ) ),
//                    new OrdersActive( saveStadium,
//                            65468415,
//                            8747487,
//                            65456,
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ) ),
//                    new OrdersActive( saveStadium,
//                            65468415,
//                            8747487,
//                            65456,
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ) ),
//                    new OrdersActive( saveStadium,
//                            65468415,
//                            8747487,
//                            65456,
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ) ),
//                    new OrdersActive( saveStadium,
//                            65468415,
//                            8747487,
//                            65456,
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ) ),
//                    new OrdersActive( saveStadium,
//                            65468415,
//                            8747487,
//                            65456,
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ) ),
//                    new OrdersActive( saveStadium,
//                            65468415,
//                            8747487,
//                            65456,
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ) ),
//                    new OrdersActive( saveStadium,
//                            65468415,
//                            8747487,
//                            65456,
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ) ),
//                    new OrdersActive( saveStadium,
//                            65468415,
//                            8747487,
//                            65456,
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ) ),
//                    new OrdersActive( saveStadium,
//                            65468415,
//                            8747487,
//                            65456,
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ) ),
//                    new OrdersActive( saveStadium,
//                            65468415,
//                            8747487,
//                            65456,
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ) ),
//                    new OrdersActive( saveStadium,
//                            65468415,
//                            8747487,
//                            65456,
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ) ),
//                    new OrdersActive( saveStadium,
//                            65468415,
//                            8747487,
//                            65456,
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ),
//                            new Date( 1999 - 1900,Calendar.MARCH,3 ) ) ) );
        }
    }
}
