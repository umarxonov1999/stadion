package koinot.uz.backend.security;

import koinot.uz.backend.exception.ExceptionSend;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Slf4j
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Autowired
    ExceptionSend exceptionSend;

    @Override
    public void commence(HttpServletRequest httpServletRequest,HttpServletResponse httpServletResponse,
                         AuthenticationException e) throws IOException, ServletException {
        exceptionSend.senException( "Responding with unauthorized error => ",e,null );
        log.error( "Responding with unauthorized error. Message - {}",e.getMessage() );
        httpServletResponse.sendError( HttpServletResponse.SC_UNAUTHORIZED,
                "Sorry, You're not authorized to access this resource." );
    }
}
