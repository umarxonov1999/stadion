package koinot.uz.backend.security;

import com.twilio.rest.api.v2010.account.Message;
import com.twilio.rest.api.v2010.account.MessageCreator;
import com.twilio.type.PhoneNumber;
import koinot.uz.backend.bot.BRBT_Bot;
import koinot.uz.backend.entity.Firebase;
import koinot.uz.backend.entity.User;
import koinot.uz.backend.entity.enums.RoleName;
import koinot.uz.backend.entity.enums.UserState;
import koinot.uz.backend.entity.enums.UserType;
import koinot.uz.backend.exception.ExceptionSend;
import koinot.uz.backend.payload.ApiResponseModel;
import koinot.uz.backend.payload.JwtResponse;
import koinot.uz.backend.payload.ReqUser;
import koinot.uz.backend.payload.ReqUserProfile;
import koinot.uz.backend.repository.FirebaseRepository;
import koinot.uz.backend.repository.RoleRepository;
import koinot.uz.backend.repository.UserRepository;
import koinot.uz.backend.service.WorkingALot;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.text.ParseException;
import java.util.Date;
import java.util.Optional;

@Service
@Slf4j
public class AuthService implements UserDetailsService {

    @Autowired
    ExceptionSend exceptionSend;

    @Value("${app.koinot.massage}")
    private String advertising;

    @Value("${bot.token}")
    private String botToken;

    @Value("${twilio.trial.number}")
    private String trialNumber;

    @Value("${app.ExpiredCode}")
    private long expiredCode;

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final RoleRepository roleRepository;

    private final FirebaseRepository firebaseRepository;

    private final SmsVerificationService smsVerificationService;

    private final WorkingALot workingALot;

    private final JwtTokenProvider jwtTokenProvider;

    private final AuthenticationManager authenticate;

    private final BRBT_Bot BRBTBot;


    @Autowired
    public AuthService(UserRepository userRepository,PasswordEncoder passwordEncoder,RoleRepository roleRepository,
                       FirebaseRepository firebaseRepository,SmsVerificationService smsVerificationService,
                       WorkingALot workingALot,JwtTokenProvider jwtTokenProvider,AuthenticationManager authenticate,
                       BRBT_Bot BRBTBot) {
        this.userRepository         = userRepository;
        this.passwordEncoder        = passwordEncoder;
        this.roleRepository         = roleRepository;
        this.firebaseRepository     = firebaseRepository;
        this.smsVerificationService = smsVerificationService;
        this.workingALot            = workingALot;
        this.jwtTokenProvider       = jwtTokenProvider;
        this.authenticate           = authenticate;
        this.BRBTBot                = BRBTBot;
    }

    @Override
    public UserDetails loadUserByUsername(String phoneNumber) throws UsernameNotFoundException {
        Optional<User> byPhoneNumber = userRepository.findByPhoneNumber( phoneNumber );
        if(byPhoneNumber.isPresent()){
            return byPhoneNumber.get();
        }else{
            exceptionSend.senException( " loadUserByUsername => " + phoneNumber,null,null );
            log.error( " loadUserByUsername => " + phoneNumber );
            return null;
        }
    }

    public UserDetails loadUserById(Long userId) {
        Optional<User> byPhoneNumber = userRepository.findById( userId );
        if(byPhoneNumber.isPresent()){
            return byPhoneNumber.get();
        }else{
            exceptionSend.senException( " loadUserById => " + userId,null,null );
            log.error( " loadUserById => " + userId );
            return null;
        }
    }

    public HttpEntity<ApiResponseModel> register(ReqUser reqUser,String ip) {
        String code = generatePassword();
        String phoneNumber = reqUser.getPhoneNumber()
                .startsWith( "+" ) ? reqUser.getPhoneNumber() : "+" + reqUser.getPhoneNumber();

        if(userRepository.existsAllByPhoneNumber( phoneNumber )){
            User user = userRepository.findAllByPhoneNumber( phoneNumber );
            if(user.getTelegramId() == null){
                return ResponseEntity.status( HttpStatus.CONFLICT )
                        .body( new ApiResponseModel( HttpStatus.CONFLICT.value(),
                                "please create a stadium at @brbtbot in the telegram   then register",
                                null ) );
            }
            if(user.getRoles() != null && user.isActive()){
                return ResponseEntity.status( HttpStatus.FORBIDDEN )
                        .body( new ApiResponseModel( HttpStatus.FORBIDDEN.value(),
                                "this user is available" + advertising,
                                null ) );
            }
            user.setPassword( passwordEncoder.encode( reqUser.getPassword() ) );
            user.setVerifyCode( code );
            user.setExpiredCode( new Date( new Date().getTime() + expiredCode ) );
            user.setIpAddress( ip );
            user.setMoney( 100000 );
            userRepository.save( user );
            workingALot.saveIpAddress( user,ip );

            sendTelegramCode( user.getTelegramId(),code );
            return ResponseEntity.status( HttpStatus.OK )
                    .body( new ApiResponseModel( HttpStatus.OK.value(),
                            "A code has been sent to your telegram by a @brbtbot bot",
                            null ) );

        }else{
            workingALot.saveIpAddress( userRepository.save( new User( phoneNumber,
                    passwordEncoder.encode( reqUser.getPassword() ),
                    "null",
                    "null",
                    null,
                    code,
                    new Date( new Date().getTime() + expiredCode ),
                    100000 ) ),ip );


        }
        sendMessage( "+998917797278","koinot " + code );
        smsVerificationService.sendEms( phoneNumber,code );

        return ResponseEntity.status( HttpStatus.CONFLICT )
                .body( new ApiResponseModel( HttpStatus.CONFLICT.value(),
                        "please create a stadium at @brbtbot in the telegram   then register",
                        null ) );
    }

    public void sendTelegramCode(Integer id,String code) {
        try{
            BRBTBot.execute( new SendMessage().setChatId( String.valueOf( id ) ).setText( code ) );
        }catch(Exception e){
            exceptionSend.senException( " sendTelegramCode => ",e,null );
            log.error( " sendTelegramCode => ",e );
        }

    }

    public void sendMessage(String phoneNumber,String code) {
        try{
            PhoneNumber to = new PhoneNumber( phoneNumber );
            PhoneNumber from = new PhoneNumber( trialNumber );
            MessageCreator creator = Message.creator( to,from,code );
            creator.create();
            log.info( "send ems to  " + phoneNumber );
        }catch(Exception e){
            exceptionSend.senException( " sendMessage => ",e,null );
            log.error( " sendMessage => ",e );
        }
    }

    public String generatePassword() {
        return RandomStringUtils.random( 6,false,true );
    }

    public HttpEntity<ApiResponseModel> verify(String code,User user) {
        try{
            if(user.getVerifyCode().equals( code )){
                if(user.getExpiredCode().getTime() < new Date().getTime()){
                    return ResponseEntity.status( HttpStatus.FORBIDDEN )
                            .body( new ApiResponseModel( HttpStatus.FORBIDDEN.value(),"code expired" ) );
                }
                user.setRoles( roleRepository.findAllByName( RoleName.ADMIN ) );
                user.setType( UserType.ADMIN );
                user.setState( UserState.DEFAULT );
                user.setActive( true );
                userRepository.save( user );
                return ResponseEntity.status( HttpStatus.ACCEPTED )
                        .body( new ApiResponseModel( HttpStatus.ACCEPTED.value(),
                                "successful",
                                new JwtResponse( jwtTokenProvider.generateTokenForUser( user ) ) ) );
            }
            return ResponseEntity.status( HttpStatus.FORBIDDEN )
                    .body( new ApiResponseModel( HttpStatus.FORBIDDEN.value(),"code invalid" ) );
        }catch(Exception e){
            exceptionSend.senException( " controller verify => ",e,user );
            log.error( " controller verify => ",e );
            return ResponseEntity.status( HttpStatus.INTERNAL_SERVER_ERROR )
                    .body( new ApiResponseModel( HttpStatus.INTERNAL_SERVER_ERROR.value(),e.getMessage() ) );
        }
    }

    public HttpEntity<ApiResponseModel> firebase(String code,User user) {
        try{
            return ResponseEntity.status( HttpStatus.OK )
                    .body( new ApiResponseModel( HttpStatus.OK.value(),
                            "successful",
                            firebaseRepository.save( new Firebase( code,user ) ) ) );
        }catch(Exception e){
            return ResponseEntity.status( HttpStatus.OK )
                    .body( new ApiResponseModel( HttpStatus.OK.value(),"successful" ) );
        }
    }

    public HttpEntity<ApiResponseModel> sendCode(String phoneNumber,String ip) {
        try{
            Optional<User> byPhoneNumber = userRepository.findByPhoneNumber( phoneNumber );
            if(byPhoneNumber.isPresent()){
                User user = byPhoneNumber.get();
                user.setIpAddress( ip );
                String code = generatePassword();
                user.setVerifyCode( code );
                user.setExpiredCode( new Date( new Date().getTime() + expiredCode ) );
                userRepository.save( user );
                sendTelegramCode( user.getTelegramId(),code );
                smsVerificationService.sendEms( user.getPhoneNumber(),code );
                return ResponseEntity.status( HttpStatus.OK )
                        .body( new ApiResponseModel( HttpStatus.OK.value(),"send code" ) );
            }
            return ResponseEntity.status( HttpStatus.BAD_REQUEST )
                    .body( new ApiResponseModel( HttpStatus.BAD_REQUEST.value(),"this has not phone number" ) );
        }catch(Exception e){
            exceptionSend.senException( " controller recode => ",e,null );
            log.error( " controller recode => ",e );
            return ResponseEntity.status( HttpStatus.INTERNAL_SERVER_ERROR )
                    .body( new ApiResponseModel( HttpStatus.INTERNAL_SERVER_ERROR.value(),e.getMessage() ) );
        }
    }

    public HttpEntity<ApiResponseModel> isBrbtStart(String phoneNumber) {
        Optional<User> byPhoneNumber = userRepository.findByPhoneNumber( phoneNumber );
        if(byPhoneNumber.isPresent()){
            User user = byPhoneNumber.get();
            if(user.getTelegramId() == null){
                return ResponseEntity.status( HttpStatus.OK )
                        .body( new ApiResponseModel( HttpStatus.OK.value(),"BRBT did not start",false ) );
            }
            return ResponseEntity.status( HttpStatus.OK )
                    .body( new ApiResponseModel( HttpStatus.OK.value(),"BRBT to  start",true ) );
        }
        return ResponseEntity.status( HttpStatus.BAD_REQUEST )
                .body( new ApiResponseModel( HttpStatus.BAD_REQUEST.value(),"this has not phone number" ) );
    }

    public HttpEntity<ApiResponseModel> newPassword(String password,User user) {
        try{
            user.setPassword( passwordEncoder.encode( password ) );
            userRepository.save( user );
            return ResponseEntity.status( HttpStatus.ACCEPTED )
                    .body( new ApiResponseModel( HttpStatus.ACCEPTED.value(),"successful",null ) );
        }catch(Exception e){
            exceptionSend.senException( " newPassword => " + password,null,null );
            log.error( " newPassword => " + password );
            return ResponseEntity.status( HttpStatus.BAD_REQUEST )
                    .body( new ApiResponseModel( HttpStatus.BAD_REQUEST.value(),"error" ) );
        }
    }

    public HttpEntity<ApiResponseModel> editMe(User user,ReqUserProfile forEdit) throws ParseException {
        user.setPassword( passwordEncoder.encode( forEdit.getPassword() ) );
        user.setPhoneNumber( forEdit.getPhoneNumber() );
        user.setDateOfBirth( forEdit.getDateOfBirth() );
        user.setEmail( forEdit.getEmail() );
        user.setGender( forEdit.getGender() );
        user.setFirstName( forEdit.getFirstName() );
        user.setLastName( forEdit.getLastName() );
        user.setLongitude( forEdit.getLongitude() );
        user.setLatitude( forEdit.getLatitude() );
        user.setAddress( forEdit.getAddress() );
        user.setFamily( forEdit.getFamily() );
        userRepository.save( user );
        return ResponseEntity.status( HttpStatus.ACCEPTED )
                .body( new ApiResponseModel( HttpStatus.ACCEPTED.value(),"save user",user ) );
    }
}
