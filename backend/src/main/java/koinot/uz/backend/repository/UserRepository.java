package koinot.uz.backend.repository;

import koinot.uz.backend.colection.UserColl;
import koinot.uz.backend.entity.User;
import koinot.uz.backend.entity.enums.UserType;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    boolean existsByTelegramId(Integer telegramId);

    User findAllByTelegramId(Integer telegramId);

    List<User> findAllByType(UserType role);

    Optional<User> findByPhoneNumber(String phoneNumber);

    List<UserColl> findAllByPhoneNumberContains(String phoneNumber);

    boolean existsByPhoneNumberAndIdNot(String phoneNumber,Long id);

    List<User> findAllByPhoneNumberContainsAndFirstNameContainingAndLastNameContaining(String phoneNumber,
                                                                                       String firstName,String lastName,
                                                                                       Pageable pageable);

    boolean existsAllByPhoneNumber(String phoneNumber);

    User findAllByPhoneNumber(String phoneNumber);
}
