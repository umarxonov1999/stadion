package koinot.uz.backend.repository;

import koinot.uz.backend.entity.OrdersActive;
import koinot.uz.backend.entity.Stadium;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderActiveRepository extends JpaRepository<OrdersActive, Long> {

    List<OrdersActive> findAllByActiveAndStadiumId(boolean active,Long id);

    List<OrdersActive> findAllByActive(boolean active);

    List<OrdersActive> findAllByStadiumAndCancelOrderOrderByTimeDesc(Stadium stadium,boolean cancelOrder);


}
