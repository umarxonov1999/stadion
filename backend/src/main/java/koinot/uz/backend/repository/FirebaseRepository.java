package koinot.uz.backend.repository;

import koinot.uz.backend.entity.Firebase;
import koinot.uz.backend.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FirebaseRepository extends JpaRepository<Firebase, Long> {
    List<Firebase> findAllByUser(User user);
}
