package koinot.uz.backend.repository;

import koinot.uz.backend.entity.Stadium;
import koinot.uz.backend.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StadiumRepository extends JpaRepository<Stadium, Long> {

    List<Stadium> findAllByUserAndDeleteOrderById(User user,boolean delete);

    Stadium findByName(String name);

    boolean existsByUser(User user);

    List<Stadium> findAllByUserAndDelete(User user,boolean delete);


    boolean existsByNameAndIdNot(String name,Long id);

    boolean existsAllByName(String name);

    boolean existsByIdAndUser(Long id,User user);

    List<Stadium> findAllByActiveAndDelete(boolean active,boolean delete);


    boolean existsByNameAndUser(String name,User user);

}
