package koinot.uz.backend.repository;

import koinot.uz.backend.entity.OrdersArchive;
import koinot.uz.backend.entity.Stadium;
import koinot.uz.backend.entity.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Repository
public interface OrderArchiveRepository extends JpaRepository<OrdersArchive, Long> {

    List<OrdersArchive> findAllByStadiumAndCancelOrderAndTimeBetweenOrderByTimeAsc(Stadium stadium,boolean cancelOrder,
                                                                                   Date time,Date time2);

    List<OrdersArchive> findAllByStadiumIdAndCancelOrderOrderByTimeAsc(Long stadium_id,boolean cancelOrder);

    List<OrdersArchive> findAllByStadiumIdAndCreatedAtIsAfterAndCancelOrderAndCreatedAtNotOrderByCreatedAtAsc(
            Long stadium_id,Timestamp createdAt,boolean cancelOrder,Timestamp createdAt2);

    List<OrdersArchive> findAllByStadiumAndCancelOrderOrderByCreatedAtAsc(Stadium stadium,boolean cancelOrder,
                                                                          Pageable pageable);

    Double countAllByUserAndStadium(User user,Stadium stadium);


}
