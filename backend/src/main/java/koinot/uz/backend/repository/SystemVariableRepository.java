package koinot.uz.backend.repository;

import koinot.uz.backend.entity.SystemVariable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SystemVariableRepository extends JpaRepository<SystemVariable, Long> {
    Optional<SystemVariable> findAllByName(String name);
}
