package koinot.uz.backend.repository;

import koinot.uz.backend.entity.OrdersBlack;
import koinot.uz.backend.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface OrderBlackRepository extends JpaRepository<OrdersBlack, Long> {

    boolean existsAllByUser(User user);

    Optional<OrdersBlack> findAllByUser(User user);
}
