package koinot.uz.backend.repository;

import koinot.uz.backend.colection.StadiumPhotoColl;
import koinot.uz.backend.entity.Stadium;
import koinot.uz.backend.entity.StadiumPhoto;
import koinot.uz.backend.entity.enums.AttachmentTypeEnumWhy;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StadiumPhotoRepository extends JpaRepository<StadiumPhoto, Long> {

    List<StadiumPhoto> findByStadiumIdAndWhyOrderByCreatedAtDesc(Long stadium_id,AttachmentTypeEnumWhy why);

    int countStadiumPhotoByStadiumAndWhy(Stadium stadium,AttachmentTypeEnumWhy why);

    StadiumPhoto findAllByIdAndWhy(Long id,AttachmentTypeEnumWhy why);

    List<StadiumPhotoColl> findAllByStadiumIdAndWhyOrderByCreatedAtDesc(Long stadium_id,AttachmentTypeEnumWhy why);

    List<StadiumPhoto> findAllByWhy(AttachmentTypeEnumWhy why);

}
