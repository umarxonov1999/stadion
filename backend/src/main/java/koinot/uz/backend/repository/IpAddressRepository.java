package koinot.uz.backend.repository;

import koinot.uz.backend.entity.IpAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IpAddressRepository extends JpaRepository<IpAddress, Long> {
    boolean existsByIpAndUserId(String ip,Long user_id);
}
