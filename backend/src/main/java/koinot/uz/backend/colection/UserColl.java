package koinot.uz.backend.colection;

public interface UserColl {
    Long getId();

    String getPhoneNumber();

    String getLastName();

    String getFirstName();
}
