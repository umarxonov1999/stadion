package koinot.uz.backend.colection;

import java.util.Date;

public interface ActiveOrderColl {
    Long getId();

    //    StadiumColl getStadium();

    UserColl getUser();

    double getLatitude();

    double getLongitude();

    double getSum();

    Date getStartDate();

    Date getEndDate();

    Date getTime();

    boolean getActive();

    boolean isCancelOrder();
}
