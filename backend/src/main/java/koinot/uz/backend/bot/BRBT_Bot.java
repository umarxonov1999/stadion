package koinot.uz.backend.bot;

import koinot.uz.backend.entity.OrdersActive;
import koinot.uz.backend.entity.Stadium;
import koinot.uz.backend.entity.User;
import koinot.uz.backend.entity.enums.UserState;
import koinot.uz.backend.entity.enums.UserType;
import koinot.uz.backend.exception.ExceptionSend;
import koinot.uz.backend.repository.OrderActiveRepository;
import koinot.uz.backend.repository.OrderBlackRepository;
import koinot.uz.backend.repository.StadiumRepository;
import koinot.uz.backend.repository.UserRepository;
import koinot.uz.backend.service.*;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.ActionType;
import org.telegram.telegrambots.meta.api.methods.send.SendChatAction;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.List;
import java.util.Optional;

@Component
@Slf4j
public class BRBT_Bot extends TelegramLongPollingBot {

    @Value("${bot.token}")
    private String botToken;

    @Value("${bot.name}")
    private String botUsername;

    @Autowired
    BotService botService;

    @Autowired
    CreateStadium createStadium;

    @Autowired
    GetStadium getStadium;

    @Autowired
    UserRepository userRepository;

    @Autowired
    StadiumRepository stadiumRepository;

    @Autowired
    SupperAdmin supperAdmin;

    @Autowired
    WorkingALot workingALot;

    @Autowired
    OrderService orderService;

    @Autowired
    OrderBlackRepository blackRepository;

    @Autowired
    OrderActiveRepository activeRepository;

    @Autowired
    Restar restar;

    @Autowired
    ExceptionSend exceptionSend;

    @Override
    public String getBotUsername() {
        return botUsername;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    @SneakyThrows
    @Override
    public void onUpdateReceived(Update update) {
        //        AnswerBot(update);
    }

    @SneakyThrows
    @Override
    public void onUpdatesReceived(List<Update> updates) {
        SendChatAction sendChatAction = new SendChatAction();
        for (Update update : updates) {
            if(update.hasCallbackQuery()){
                sendChatAction.setChatId( update.getCallbackQuery().getMessage().getChatId() );
            }else{
                sendChatAction.setChatId( update.getMessage().getChatId() );
            }
            sendChatAction.setAction( ActionType.TYPING );
            execute( sendChatAction );
            try{
                AnswerBot( update );
            }catch(Exception e){
                exceptionSend.senException( "bot  AnswerBot=> ",e,null );
                log.error( "bot  AnswerBot=> ",e );
            }
        }
    }

    public void AnswerBot(Update update) {
        if(update.hasMessage()){
            Message message = update.getMessage();
            Integer id = update.getMessage().getFrom().getId();
            String language = BotAnswerString.uzl;
            User user = new User();
            if(userRepository.existsByTelegramId( id )){
                user = userRepository.findAllByTelegramId( id );
                if(user.getLanguage() != null){
                    language = user.getLanguage();
                }
            }
            if(message.hasText()){
                if(message.getText().equals( "/start" )){
                    botService.userLanguage( update );
                    botService.sendVideo( update );
                }else if(message.getText().equals( BotAnswerString.RESTART_RU ) || message.getText()
                        .equals( BotAnswerString.RESTART_EN ) || message.getText()
                        .equals( BotAnswerString.RESTART_UZL ) || message.getText()
                        .equals( BotAnswerString.RESTART_UZ )){
                    restar.registerBoss( user,update );
                }else if(message.getText().equals( BotAnswerString.CHANGE_CLIENT_PHONE_NUMBER_RU ) || message.getText()
                        .equals( BotAnswerString.CHANGE_CLIENT_PHONE_NUMBER_EN ) || message.getText()
                        .equals( BotAnswerString.CHANGE_CLIENT_PHONE_NUMBER_UZL ) || message.getText()
                        .equals( BotAnswerString.RESTART_UZ )){
                    getStadium.getPhoneClient( user,update,language );
                }
                if(user.getState() != null){
                    if(user.getState().equals( UserState.DEFAULT )){
                        if(( message.getText().equals( BotAnswerString.STADIUM_DASHBOARD_RU ) || message.getText()
                                .equals( BotAnswerString.STADIUM_DASHBOARD_UZ ) || message.getText()
                                .equals( BotAnswerString.STADIUM_DASHBOARD_UZL ) || message.getText()
                                .equals( BotAnswerString.STADIUM_DASHBOARD_EN ) ) && user.getType()
                                .equals( UserType.ADMIN )){
                            botService.getStadiumForAdmin( user,update );
                        }else if(message.getText().equals( BotAnswerString.CHANGE_LANGUAGE_RU ) || message.getText()
                                .equals( BotAnswerString.CHANGE_LANGUAGE_UZ ) || message.getText()
                                .equals( BotAnswerString.CHANGE_LANGUAGE_UZL ) || message.getText()
                                .equals( BotAnswerString.CHANGE_LANGUAGE_EN )){
                            botService.changeLanguage( user,update );
                        }
                    }
                }
            }
            if(user.getState() != null){
                if(user.getState().equals( UserState.GET_PHONE_NUMBER )){
                    createStadium.getPhoneNumber( update,user,language );
                }else if(user.getState().equals( UserState.SEND_LOCATION )){
                    createStadium.getLocation( update,user,language );
                }else if(user.getState().equals( UserState.SEND_ADDRESS )){
                    createStadium.getAddress( update,user,language );
                }else if(user.getState().equals( UserState.SEND_NAME )){
                    createStadium.getName( update,user,language );
                }else if(user.getState().equals( UserState.OPEN_TIME_STADIUM )){
                    createStadium.getStadiumStartWork( update,user,language );
                }else if(user.getState().equals( UserState.CLOSE_TIME_STADIUM )){
                    createStadium.getStadiumFinishWork( update,user,language );
                }else if(user.getState().equals( UserState.GET_PHOTO_STADIUM )){
                    createStadium.getPhoto( update,user,language );
                }else if(user.getState().equals( UserState.GET_PRICE_DAYLIGHT_STADIUM )){
                    createStadium.getPriceDaylight( update,user,language );
                }else if(user.getState().equals( UserState.GET_PRICE_NIGHT_STADIUM )){
                    createStadium.getPriceNightlight( update,user,language );
                }else if(user.getState().equals( UserState.GET_NIGHT_TIME_FOR_CHANGE_PRICE )){
                    createStadium.getTimeForChangePrice( update,user,language );
                }else if(user.getState().equals( UserState.EDIT_OPEN_TIME_STADIUM )){
                    createStadium.getEditStadiumStartWork( update,user,language );
                }else if(user.getState().equals( UserState.EDIT_CLOSE_TIME_STADIUM )){
                    createStadium.getEditStadiumFinishWork( update,user,language );
                }else if(update.getMessage().hasText() && user.getState()
                        .equals( UserState.EDIT_GET_PRICE_DAYLIGHT_STADIUM )){
                    createStadium.getEditPriceDaylight( update,user,language );
                }else if(user.getState().equals( UserState.EDIT_GET_PRICE_NIGHT_STADIUM )){
                    createStadium.getEditPriceNightlight( update,user,language );
                }else if(user.getState().equals( UserState.EDIT_SEND_LOCATION )){
                    SendChatAction sendChatAction = new SendChatAction();
                    sendChatAction.setChatId( update.getMessage().getChatId() );
                    sendChatAction.setAction( ActionType.FINDLOCATION );
                    try{
                        execute( sendChatAction );
                    }catch(Exception e){
                        exceptionSend.senException( "bot sendChatAction => ",e,null );
                        log.error( "bot sendChatAction => ",e );
                    }
                    createStadium.getEditLocation( update,user,language );
                }else if(user.getState().equals( UserState.EDIT_SEND_ADDRESS )){
                    createStadium.getEditAddress( update,user,language );
                }else if(user.getState().equals( UserState.EDIT_SEND_NAME )){
                    createStadium.getEditName( update,user,language );
                }else if(user.getState().equals( UserState.EDIT_GET_NIGHT_TIME_FOR_CHANGE_PRICE )){
                    createStadium.getEditTimeForChangePrice( update,user,language );
                }else if(user.getState().equals( UserState.EDIT_GET_PHOTO_STADIUM )){
                    createStadium.getEditPhoto( update,user,language );
                }else if(user.getState().equals( UserState.EDIT_PHONE_NUMBER )){
                    createStadium.getEditPhoneNumber( update,user,language );
                }else if(user.getState().equals( UserState.CLIENT_SEND_LOCATION )){
                    SendChatAction sendChatAction = new SendChatAction();
                    sendChatAction.setChatId( update.getMessage().getChatId() );
                    sendChatAction.setAction( ActionType.FINDLOCATION );
                    try{
                        execute( sendChatAction );
                    }catch(Exception e){
                        exceptionSend.senException( "bot sendChatAction => ",e,null );
                        log.error( "bot sendChatAction => ",e );
                    }
                    if(message.hasLocation()){
                        getStadium.getLocation( update,user,language );
                    }else if(message.hasText()){
                        if(workingALot.getRegion().containsKey( message.getText() )){
                            getStadium.selectRegion( update,user,language );
                        }else if(blackRepository.existsAllByUser( user )){
                            orderService.getOrderClock( update,user,language );
                        }
                    }
                }else if(user.getState().equals( UserState.CLIENT_SEND_PHONE_NUMBER )){
                    getStadium.getClientPhone( update,user,language );
                }else if(user.getState().equals( UserState.GET_STADIUM_SIZE )){
                    createStadium.getStadiumSize( update,user,language );
                }
            }

        }else if(update.hasCallbackQuery()){
            String data = update.getCallbackQuery().getData();
            Integer chat_id = update.getCallbackQuery().getFrom().getId();
            String language = BotAnswerString.uzl;

            User user = new User();
            if(userRepository.existsByTelegramId( chat_id )){
                user = userRepository.findAllByTelegramId( chat_id );
                if(user.getLanguage() != null){
                    language = user.getLanguage();
                }
            }
            if(user.getState().equals( UserState.CLIENT_SEND_LOCATION )){
                if(data.startsWith( BotAnswerString.cancelOrder )){
                    Long activeId = Long.valueOf( data.substring( BotAnswerString.cancelOrder.length() ) );
                    orderService.cancelOrderClient( chat_id,activeId,user,update );
                }
                if(data.startsWith( "_koinot_" )){
                    int value = Integer.parseInt( data.substring( 13 ) );
                    if(data.startsWith( BotAnswerString._koinot_prev_ )){
                        getStadium.prevSelectStadium( value,chat_id,user,update );
                    }
                    if(data.startsWith( BotAnswerString._koinot_next_ )){
                        getStadium.nextSelectStadium( value,chat_id,user,update );
                    }
                }else if(data.startsWith( "cancel" + BotAnswerString.CANCEL )){
                    workingALot.deleteMsg( Long.valueOf( chat_id ),
                            update.getCallbackQuery().getMessage().getMessageId() );
                }else if(data.startsWith( BotAnswerString.cancel_client )){
                    Stadium stadium = stadiumRepository.findByName( data.substring( BotAnswerString.cancel_client.length() ) );
                    getStadium.cancelClient( stadium,user,update,language );
                }else if(data.startsWith( BotAnswerString.koinot )){
                    Stadium stadium = stadiumRepository.findByName( data.substring( BotAnswerString.koinot.length() ) );
                    getStadium.getClientStadium( stadium,user,update,language );
                }else if(data.startsWith( BotAnswerString.get_order )){
                    if(( user.getPhoneNumber() != null && user.getPhoneNumber().length() > 8 )){
                        Stadium stadium = stadiumRepository.findByName( data.substring( BotAnswerString.get_order.length() ) );
                        orderService.getOrderDate( update,user,language,stadium );
                    }else{
                        getStadium.getPhoneClient( user,update,language );
                    }
                }else if(data.startsWith( BotAnswerString.get_time )){
                    int day = Integer.parseInt( data.substring( BotAnswerString.get_time.length(),
                            data.indexOf( BotAnswerString.get_month ) ) );
                    String stadiumName = data.substring( data.indexOf( BotAnswerString.get_stadium ) + BotAnswerString.get_stadium.length() );
                    int month = Integer.parseInt( data.substring( data.indexOf( BotAnswerString.get_month ) + BotAnswerString.get_month.length(),
                            data.indexOf( BotAnswerString.get_stadium ) ) );
                    Stadium stadium = stadiumRepository.findByName( stadiumName );

                    orderService.createOrder( stadium,user,update,language,day,month );
                }else if(data.startsWith( BotAnswerString.like )){
                    Stadium stadium = stadiumRepository.findByName( data.substring( BotAnswerString.like.length() ) );
                    orderService.like( update,user,stadium );
                }else if(data.startsWith( BotAnswerString.disLice )){
                    Stadium stadium = stadiumRepository.findByName( data.substring( BotAnswerString.disLice.length() ) );
                    orderService.disLice( update,user,stadium );
                }
            }
            if(user.getState()
                    .equals( UserState.CHOOSE_LANGUAGE ) && ( data.equals( BotAnswerString.uzl ) || data.equals(
                    BotAnswerString.uz ) || data.equals( BotAnswerString.ru ) || data.equals( BotAnswerString.en ) )){
                botService.editLanguage( update );
            }else if(user.getState().equals( UserState.DEFAULT )){
                if(data.equals( BotAnswerString.yes_uzl ) || data.equals( BotAnswerString.yes_uz ) || data.equals(
                        BotAnswerString.yes_ru ) || data.equals( BotAnswerString.yes_en ) || data.equals(
                        BotAnswerString.CREATE )){
                    createStadium.registerStadiumBoss( update );
                }else if(data.equals( BotAnswerString.nope_en ) || data.equals( BotAnswerString.nope_ru ) || data.equals(
                        BotAnswerString.nope_uz ) || data.equals( BotAnswerString.nope_uzl )){
                    getStadium.getStadium( update,user,data,language );
                }else if(stadiumRepository.existsByNameAndUser( data,user )){
                    createStadium.setActiveStadium( update,user,data,language );
                }
            }else if(user.getState().equals( UserState.VERIFY_STADIUM )){
                switch (data) {
                    case BotAnswerString.DELETE_UZ:
                    case BotAnswerString.DELETE_RU:
                    case BotAnswerString.DELETE_UZL:
                    case BotAnswerString.DELETE_EN:
                        createStadium.deleteStadium( update,user,language );
                        break;
                    case BotAnswerString.EDIT_RU:
                    case BotAnswerString.EDIT_UZL:
                    case BotAnswerString.EDIT_EN:
                    case BotAnswerString.EDIT_UZ:
                        createStadium.editStadium( update,user,language );
                        break;
                    case BotAnswerString.VERiFY_RU:
                    case BotAnswerString.VERiFY_UZL:
                    case BotAnswerString.VERiFY_UZ:
                    case BotAnswerString.VERiFY_EN:
                        createStadium.verifyStadium( update,user,language );
                        break;
                    case BotAnswerString.CHANGED:
                        createStadium.registerStadiumBoss( update );
                        break;
                }
            }else if(user.getState().equals( UserState.EDIT )){
                switch (data) {
                    case BotAnswerString.PHONE_NUMBER_UZ:
                    case BotAnswerString.PHONE_NUMBER_RU:
                    case BotAnswerString.PHONE_NUMBER_UZL:
                    case BotAnswerString.PHONE_NUMBER_EN:
                        createStadium.changePhoneNumber( update,user,language );
                        break;
                    case BotAnswerString.SIZE_UZ:
                    case BotAnswerString.SIZE_RU:
                    case BotAnswerString.SIZE_UZL:
                    case BotAnswerString.SIZE_EN:
                        createStadium.changeSize( update,user,language );
                        break;
                    case BotAnswerString.WORKING_HOURS_UZ:
                    case BotAnswerString.WORKING_HOURS_RU:
                    case BotAnswerString.WORKING_HOURS_UZL:
                    case BotAnswerString.WORKING_HOURS_EN:
                        createStadium.changeWorkingHours( update,user,language );
                        break;
                    case BotAnswerString.PRICE_UZ:
                    case BotAnswerString.PRICE_RU:
                    case BotAnswerString.PRICE_UZL:
                    case BotAnswerString.PRICE_EN:
                        createStadium.changePrice( update,user,language );
                        break;
                    case BotAnswerString.LOCATION_UZ:
                    case BotAnswerString.LOCATION_RU:
                    case BotAnswerString.LOCATION_UZL:
                    case BotAnswerString.LOCATION_EN:
                        createStadium.changeLocation( update,user,language );
                        break;
                    case BotAnswerString.ADDRESS_UZ:
                    case BotAnswerString.ADDRESS_RU:
                    case BotAnswerString.ADDRESS_UZL:
                    case BotAnswerString.ADDRESS_EN:
                        createStadium.changeAddress( update,user,language );
                        break;
                    case BotAnswerString.NAME_UZ:
                    case BotAnswerString.NAME_RU:
                    case BotAnswerString.NAME_UZL:
                    case BotAnswerString.NAME_EN:
                        createStadium.changeName( update,user,language );
                        break;
                    case BotAnswerString.TIME_OF_EVENING_PRICE_CHANGE_UZ:
                    case BotAnswerString.TIME_OF_EVENING_PRICE_CHANGE_RU:
                    case BotAnswerString.TIME_OF_EVENING_PRICE_CHANGE_UZL:
                    case BotAnswerString.TIME_OF_EVENING_PRICE_CHANGE_EN:
                        createStadium.changeTimeOfEveningPrice( update,user,language );
                        break;
                    case BotAnswerString.STADIUM_PHOTO_CHANGE_UZ:
                    case BotAnswerString.STADIUM_PHOTO_CHANGE_RU:
                    case BotAnswerString.STADIUM_PHOTO_CHANGE_UZL:
                    case BotAnswerString.STADIUM_PHOTO_CHANGE_EN:
                        createStadium.changePhoto( update,user,language );
                        break;
                    case BotAnswerString.BACK:
                        createStadium.back( update,user,language );
                        break;
                }
            }
            if(user.getType() != null){
                if(user.getType().equals( UserType.SUPPER_ADMIN )){
                    Optional<Stadium> stadiumO = stadiumRepository.findById( Long.valueOf( data.substring( 0,
                            data.indexOf( '_' ) ) ) );
                    if(stadiumO.isPresent()){
                        Stadium stadium = stadiumO.get();
                        switch (data.substring( data.indexOf( '_' ) + 1 )) {
                            case BotAnswerString.DELETE_UZ:
                            case BotAnswerString.DELETE_RU:
                            case BotAnswerString.DELETE_UZL:
                            case BotAnswerString.DELETE_EN:
                                supperAdmin.deleteStadium( update,user,stadium );
                                break;
                            case BotAnswerString.SIZE_UZ:
                            case BotAnswerString.SIZE_RU:
                            case BotAnswerString.SIZE_UZL:
                            case BotAnswerString.SIZE_EN:
                                supperAdmin.warningSizeStadium( update,user,stadium );
                                break;
                            case BotAnswerString.VERiFY_RU:
                            case BotAnswerString.VERiFY_UZL:
                            case BotAnswerString.VERiFY_UZ:
                            case BotAnswerString.VERiFY_EN:
                                supperAdmin.activeStadium( update,user,stadium );
                                break;
                            case BotAnswerString.WORKING_HOURS_RU:
                            case BotAnswerString.WORKING_HOURS_UZL:
                            case BotAnswerString.WORKING_HOURS_UZ:
                            case BotAnswerString.WORKING_HOURS_EN:
                                supperAdmin.warningWorkingHours( update,user,stadium );
                                break;
                            case BotAnswerString.PRICE_RU:
                            case BotAnswerString.PRICE_UZL:
                            case BotAnswerString.PRICE_UZ:
                            case BotAnswerString.PRICE_EN:
                                supperAdmin.warningPrice( update,user,stadium );
                                break;
                            case BotAnswerString.LOCATION_RU:
                            case BotAnswerString.LOCATION_UZL:
                            case BotAnswerString.LOCATION_UZ:
                            case BotAnswerString.LOCATION_EN:
                                supperAdmin.warningLocation( update,user,stadium );
                                break;
                            case BotAnswerString.ADDRESS_RU:
                            case BotAnswerString.ADDRESS_UZL:
                            case BotAnswerString.ADDRESS_UZ:
                            case BotAnswerString.ADDRESS_EN:
                                supperAdmin.warningAddress( update,user,stadium );
                                break;
                            case BotAnswerString.PHONE_NUMBER_RU:
                            case BotAnswerString.PHONE_NUMBER_UZL:
                            case BotAnswerString.PHONE_NUMBER_UZ:
                            case BotAnswerString.PHONE_NUMBER_EN:
                                supperAdmin.warningPhoneNumber( update,user,stadium );
                                break;
                            case BotAnswerString.NAME_RU:
                            case BotAnswerString.NAME_UZL:
                            case BotAnswerString.NAME_UZ:
                            case BotAnswerString.NAME_EN:
                                supperAdmin.warningName( update,user,stadium );
                                break;
                            case BotAnswerString.TIME_OF_EVENING_PRICE_CHANGE_RU:
                            case BotAnswerString.TIME_OF_EVENING_PRICE_CHANGE_UZL:
                            case BotAnswerString.TIME_OF_EVENING_PRICE_CHANGE_UZ:
                            case BotAnswerString.TIME_OF_EVENING_PRICE_CHANGE_EN:
                                supperAdmin.warningChangePrice( update,user,stadium );
                                break;
                            case BotAnswerString.STADIUM_PHOTO_CHANGE_RU:
                            case BotAnswerString.STADIUM_PHOTO_CHANGE_UZL:
                            case BotAnswerString.STADIUM_PHOTO_CHANGE_UZ:
                            case BotAnswerString.STADIUM_PHOTO_CHANGE_EN:
                                supperAdmin.warningPhoto( update,user,stadium );
                                break;
                        }
                    }
                }
                if(user.getType().equals( UserType.ADMIN )){
                    if(data.startsWith( BotAnswerString.verify )){
                        OrdersActive order = activeRepository.getById( Long.valueOf( data.substring( BotAnswerString.verify.length() ) ) );
                        orderService.verifyOrder( order,update,user );
                    }
                    if(data.startsWith( BotAnswerString.cancelBoss )){
                        OrdersActive order = activeRepository.getById( Long.valueOf( data.substring( BotAnswerString.cancelBoss.length() ) ) );
                        orderService.cancelOrder( order,update,user );
                    }
                }
            }
        }
    }
}
