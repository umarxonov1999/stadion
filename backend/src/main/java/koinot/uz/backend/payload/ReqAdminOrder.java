package koinot.uz.backend.payload;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReqAdminOrder {

    private Long id;

    private Long stadiumId;

    @NotNull
    @NotBlank()
    @Pattern(regexp = "^[+][9][9][8][0-9]{9}$", message = "telephone number is invalid")
    @ApiModelProperty(notes = "phone number ", name = "phoneNumber", required = true, value = "+998917797278", example = "+998917797278")
    private String phoneNumber;


    @NotNull
    @NotBlank()
    @Pattern(regexp = "^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", message = "startDate is invalid")
    @ApiModelProperty(notes = "startDate ", name = "startDate", required = true, value = "21:00", example = "21:00")
    private String startDate;


    @NotNull
    @NotBlank()
    @Pattern(regexp = "^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", message = "endDate is invalid")
    @ApiModelProperty(notes = "endDate ", name = "endDate", required = true, value = "22:00")
    private String endDate;

    //    @Pattern(regexp = "^\\d\\d\\d\\d-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[01])$", message = "time is invalid")
    @ApiModelProperty(notes = "time play day", name = "time play day", required = true, value = "[2021-03-03,2021-03-04]", example = "[2021-03-03,2021-03-04]")
    private List<String> time;
}
