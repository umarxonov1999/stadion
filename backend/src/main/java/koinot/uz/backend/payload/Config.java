package koinot.uz.backend.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Config {
    private String name;
    private String phoneNumber;
    private String day;
    private String time;

    @Override
    public String toString() {
        return "  {\n" + "        \"name\":\"" + name + "\",\n" + "        \"phoneNumber\":\"" + phoneNumber + "\",\n" + "        \"day\":\"" + day + "\",\n" + "        \"time\":\"" + time + "\"\n" + "    }";
    }
}
