package koinot.uz.backend.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RestStadium {

    private Long id;

    private double lat;

    private double lng;

    private double like;

    private double distance;


    private String name;

    private boolean roof;
}
