package koinot.uz.backend.payload;

import koinot.uz.backend.entity.enums.AttachmentTypeEnumWhy;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResUploadFile {
    private Long fileId;
    private String fileName;
    private String fileType;
    private AttachmentTypeEnumWhy why;
    private long size;
    private String link;
}
