package koinot.uz.backend.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RestWebInfo {

    private long userCount;

    private long orderCount;

    private List<RestStadium> stadiums;
}
