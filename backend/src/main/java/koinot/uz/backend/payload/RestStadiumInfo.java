package koinot.uz.backend.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RestStadiumInfo {

    private Long id;

    private String phone_number;

    private Integer stadium_like = 0;

    private String change_price_time;

    private double price_day_time;

    private double price_night_time;

    private Integer width = 0;

    private Integer height = 0;

    private double count_order;

    private String opening_time;

    private String closing_time;

    private List<ReqStadiumOrder> orders;

    private List<ResUploadFile> photo;

    private boolean roof;

    private String description;

}
