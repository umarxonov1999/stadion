package koinot.uz.backend.payload;

import io.swagger.annotations.ApiModelProperty;
import koinot.uz.backend.entity.enums.Family;
import koinot.uz.backend.entity.enums.Gender;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqUserProfile {

    @NotNull
    @NotBlank()
    @Pattern(regexp = "^[+][9][9][8][0-9]{9}$", message = "telephone number is invalid")
    @ApiModelProperty(notes = "phone number ", name = "phoneNumber", required = true, value = "+998917797278")
    private String phoneNumber;

    @NotNull(message = "password must be not null")
    @NotBlank(message = "password is empty")
    @Length(min = 4, max = 30, message = "password is invalid length is min = 4 max = 30")
    @ApiModelProperty(notes = "password", name = "password", required = true, value = "123pas")
    private String password;

    @Pattern(regexp = "^\\d\\d\\d\\d-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[01])$", message = "date Of Birthis invalid")
    @ApiModelProperty(notes = "date of birth", name = "date of birth", required = true, value = "1999-03-03")
    private String dateOfBirth;

    @ApiModelProperty(notes = "email", name = "email", required = true, value = "komilovqudratjon@gmail.com")
    @Email()
    private String email;

    private Gender gender;

    private Family family;

    @Pattern(regexp = "^(-?\\d+(\\.\\d+)?)$", message = "longitude is invalid")
    private double longitude;

    @Pattern(regexp = "^(-?\\d+(\\.\\d+)?)$", message = "latitude is invalid")
    private double latitude;

    @Pattern(regexp = "[A-z]*", message = "address is invalid")
    @ApiModelProperty(notes = "address", name = "address", required = true, value = "Buxoro")
    private String address;

    @Pattern(regexp = "[A-z]*", message = "firstname is invalid")
    @ApiModelProperty(notes = "firstName", name = "firstName", required = true, value = "Qudratjon")
    private String firstName;

    @Pattern(regexp = "[A-z]*", message = "lastName is invalid")
    @ApiModelProperty(notes = "lastName", name = "lastName", required = true, value = "Komilov")
    private String lastName;


}
