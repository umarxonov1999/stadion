package koinot.uz.backend.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StadiumDistance {
    private Long id;
    private double distance;
    private String address;
    private String name;
    private String phoneNumber;
    private Date openingTime;
    private Date closingTime;
    private Integer stadiumLike;
    private Date changePriceTime;
    private Double priceDayTime;
    private Double priceNightTime;
    private Integer width;
    private Integer height;
    private Double countOrder;
    private double lat;
    private double lng;
    private boolean roof;
    private double money;
    private String description;


}
