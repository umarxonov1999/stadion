package koinot.uz.backend.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RestMyStadiumOrder {
    private Long createdAt;

    private Long id;

    private String phoneNumber;

    private String firstName;

    private String lastName;

    private String language;

    private double latitude;

    private double longitude;

    private double sum;

    private String startDate;

    private String endDate;

    private String time;

    private boolean active;

    private boolean cancelOrder;

    private double countOrder;

}
