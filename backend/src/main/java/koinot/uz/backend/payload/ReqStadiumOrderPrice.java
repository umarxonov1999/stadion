package koinot.uz.backend.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReqStadiumOrderPrice {

    private Long id;

    private String startDate;

    private String endDate;

    private String time;
}
