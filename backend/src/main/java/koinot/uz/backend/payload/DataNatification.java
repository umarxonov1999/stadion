package koinot.uz.backend.payload;

import koinot.uz.backend.entity.enums.NatificationType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DataNatification implements Serializable {
    private String title;
    private String message;
    private NatificationType natificationType;
    private String stadium;
    private boolean status;
    private Long id;
    private String order;
}
