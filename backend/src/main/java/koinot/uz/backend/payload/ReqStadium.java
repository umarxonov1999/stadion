package koinot.uz.backend.payload;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqStadium {
    private Long id;

    @NotNull
    @NotBlank()
    @Pattern(regexp = "[A-z]*", message = "name is invalid")
    @ApiModelProperty(notes = "name", name = "name", required = true, value = "Stadium name")
    private String name;


    @NotNull
    @NotBlank()
    @Pattern(regexp = "^[+][9][9][8][0-9]{9}$", message = "telephone number is invalid")
    @ApiModelProperty(notes = "phone number ", name = "phoneNumber", required = true, value = "+998917797278")
    private String phone_number;

    @NotNull
    @NotBlank()
    @Pattern(regexp = "^(-?\\d+(\\.\\d+)?)$", message = "longitude is invalid")
    private double longitude;

    @NotNull
    @NotBlank()
    @Pattern(regexp = "^(-?\\d+(\\.\\d+)?)$", message = "latitude is invalid")
    private double latitude;

    @Pattern(regexp = "[A-z]*", message = "address is invalid")
    @ApiModelProperty(notes = "address", name = "address", required = true, value = "Buxoro")
    private String address;


    @Pattern(regexp = "^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", message = "opening_time is invalid")
    private String opening_time;

    @Pattern(regexp = "^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", message = "closing_time is invalid")
    private String closing_time;

    @NotNull
    @NotBlank()
    @Pattern(regexp = "^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", message = "change_price_time is invalid")
    private String change_price_time;

    @NotNull
    @NotBlank()
    private double price_day_time;

    @NotNull
    @NotBlank()
    private double price_night_time;

    private boolean active;

    private Integer width = 0;

    private Integer height = 0;

    private boolean roof;

    private String description;


}
