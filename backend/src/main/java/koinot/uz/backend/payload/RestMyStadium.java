package koinot.uz.backend.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RestMyStadium {

    private Long id;

    private String name;

    private int countVerify;

    private int countNotVerify;

    private double latitude;

    private String phone_number;

    private double longitude;

    private String address; //

    private String opening_time; //

    private String closing_time; //

    private Integer stadium_like = 0; //

    private String change_price_time; //

    private double price_day_time; //

    private double price_night_time; //

    private Integer width = 0;

    private Integer height = 0;

    private double count_order; //

    private boolean open;

    private boolean active;

    public RestMyStadium(Long id,String name,int countVerify,int countNotVerify,double latitude,String phone_number,
                         double longitude,String address,String opening_time,String closing_time,Integer stadium_like,
                         String change_price_time,double price_day_time,double price_night_time,Integer width,
                         Integer height,double count_order,boolean open,boolean active,boolean verify,double money,
                         boolean roof) {
        this.id                = id;
        this.name              = name;
        this.countVerify       = countVerify;
        this.countNotVerify    = countNotVerify;
        this.latitude          = latitude;
        this.phone_number      = phone_number;
        this.longitude         = longitude;
        this.address           = address;
        this.opening_time      = opening_time;
        this.closing_time      = closing_time;
        this.stadium_like      = stadium_like;
        this.change_price_time = change_price_time;
        this.price_day_time    = price_day_time;
        this.price_night_time  = price_night_time;
        this.width             = width;
        this.height            = height;
        this.count_order       = count_order;
        this.open              = open;
        this.active            = active;
        this.verify            = verify;
        this.money             = money;
        this.roof              = roof;
    }

    private boolean verify;

    private boolean roof;

    private double money;

    private List<ResUploadFile> photos;
}
