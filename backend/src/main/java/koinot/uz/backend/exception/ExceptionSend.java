package koinot.uz.backend.exception;

import koinot.uz.backend.bot.BRBT_Bot;
import koinot.uz.backend.entity.User;
import koinot.uz.backend.security.AuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.io.File;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class ExceptionSend {
    @Autowired
    BRBT_Bot BRBTBot;


    private static final Logger log = LoggerFactory.getLogger( AuthService.class );

    public void senException(String msg,Exception error,User user) {
        try{


            if(user != null){
                new Thread( () -> {
                    sendErrorForAdmin( - 1001540481206L,
                            ( "#" + msg ).replace( " ",
                                    "" ) + System.lineSeparator() + ( error == null ? "null" : error ) + System.lineSeparator() + msg + user.getPhoneNumber() + System.lineSeparator() + user.getFirstName() + System.lineSeparator() + user.getLastName() + System.lineSeparator() + user.getId() );
                } ).start();
            }else{
                new Thread( () -> {
                    sendErrorForAdmin( - 1001540481206L,
                            ( "#" + msg ).replace( " ",
                                    "" ) + System.lineSeparator() + ( error == null ? "null" : error ) + System.lineSeparator() + msg );
                } ).start();
            }
        }catch(Exception e){
            log.error( " my error => ",e );
        }

    }

    public void sendErrorFileNohup() {
        try{
            File file = new File( "nohup.out" );
            SendDocument sendDocumentRequest = new SendDocument();
            sendDocumentRequest.setChatId( - 1001540481206L );
            sendDocumentRequest.setDocument( file );
            sendDocumentRequest.setCaption( new SimpleDateFormat( "yyyy-MM-dd hh:mm" ).format( new Date() ) );
            BRBTBot.execute( sendDocumentRequest );
            PrintWriter writer = new PrintWriter( file );
            writer.print( "" );
            writer.close();
        }catch(Exception e){
            log.error( " send error file error => ",e );
        }
    }

    public void sendErrorForAdmin(Long id,String text) {
        try{
            BRBTBot.execute( new SendMessage().setChatId( id ).setText( text ) );
        }catch(Exception e){
            log.error( " my error => ",e );
        }


    }

}
