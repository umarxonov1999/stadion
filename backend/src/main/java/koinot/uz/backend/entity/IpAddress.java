package koinot.uz.backend.entity;

import koinot.uz.backend.entity.template.ReadyEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "IpAddress")
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"ip","user_id"})})
public class IpAddress extends ReadyEntity {

    private String ip;

    private boolean active = true;

    @ManyToOne
    @JsonIgnore
    private User user;

}
