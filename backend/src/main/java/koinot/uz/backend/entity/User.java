package koinot.uz.backend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import koinot.uz.backend.entity.enums.Family;
import koinot.uz.backend.entity.enums.Gender;
import koinot.uz.backend.entity.enums.UserState;
import koinot.uz.backend.entity.enums.UserType;
import koinot.uz.backend.entity.template.ReadyEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Users")
public class User extends ReadyEntity implements UserDetails {
    @Column(unique = true)
    private String phoneNumber; // qo'lda kiritadigan tel nomeri

    @JsonIgnore
    private String password; // paroli

    private String ipAddress; // paroli

    @OneToOne
//    @JsonManagedReference
    @JsonIgnore
    private Stadium activeStadium; // hozir qaysi stadium ustida amal bajarayotgani bot uchun kerak

    private String dateOfBirth; // tug'lgan kuni ishlatilmagan

    private String email; // ishlatilmagan

    @JsonIgnore
    private Date expiredCode;
    @JsonIgnore
    private String verifyCode; // android uchun code

    private String username; // ishlatilmagan bot uchun ishlatmoqchi edim

    @Enumerated(EnumType.STRING)
    private Gender gender; // ishlatilmagan

    @JsonIgnore
    private boolean deleted = false;
    @JsonIgnore
    private boolean active = false;

    private Double balance; // ishlatilmagan

    @Column(unique = true)
    private Integer telegramId;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Enumerated(EnumType.STRING)
    private UserType type;

    @Enumerated(EnumType.STRING)
    private UserState state; // bot uchun holatlari

    private String language; // tili

    private double money;

    private String address;

    @Enumerated(EnumType.STRING)
    private Family family;

    private double longitude; // userni hozir tirgan joyi

    private double latitude; // userni hozir tirgan joyi

    @ManyToMany(fetch = FetchType.EAGER)
    @JsonIgnore
    @JoinTable(name = "user_role", joinColumns = {@JoinColumn(name = "user_id")}, inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private List<Role> roles; // userni roli

    public User(Integer telegramId,String firstName,String lastName,UserState state,UserType type,String language,
                String phoneNumber) {
        this.phoneNumber = phoneNumber;
        this.telegramId  = telegramId;
        this.firstName   = firstName;
        this.lastName    = lastName;
        this.state       = state;
        this.type        = type;
        this.language    = language;
    }


    public User(String phoneNumber,String firstName,String lastName) {
        this.phoneNumber = phoneNumber;
        this.firstName   = firstName;
        this.lastName    = lastName;
    }

    public User(Integer telegramId,String firstName,String lastName,UserState state) {
        this.telegramId = telegramId;
        this.firstName  = firstName;
        this.lastName   = lastName;
        this.state      = state;
    }


    public User(String phoneNumber,String password,String firstName,String lastName,List<Role> roles) {
        this.phoneNumber = phoneNumber;
        this.password    = password;
        this.firstName   = firstName;
        this.lastName    = lastName;
        this.roles       = roles;
    }

    @JsonIgnore
    private boolean accountNonExpired = true;
    @JsonIgnore
    private boolean accountNonLocked = true;
    @JsonIgnore
    private boolean credentialsNonExpired = true;
    @JsonIgnore
    private boolean enabled = true;

    public User(String phoneNumber,String password,String firstName,String lastName,List<Role> roles,String verifyCode,
                Date expiredCode,double money) {
        this.verifyCode  = verifyCode;
        this.phoneNumber = phoneNumber;
        this.password    = password;
        this.firstName   = firstName;
        this.lastName    = lastName;
        this.roles       = roles;
        this.expiredCode = expiredCode;
        this.money       = money;
    }

    @Override
    public String getUsername() {
        return this.phoneNumber;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.roles;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }
}
