package koinot.uz.backend.entity;

import koinot.uz.backend.entity.template.ReadyEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "firebase")
public class Firebase extends ReadyEntity {

    @Column(unique = true)
    private String firebaseKey;

    private String info;

    private boolean active;

    @ManyToOne
    @JsonIgnore
    private User user;

    public Firebase(String firebaseKey,User user) {
        this.firebaseKey = firebaseKey;
        this.user        = user;
    }
}
