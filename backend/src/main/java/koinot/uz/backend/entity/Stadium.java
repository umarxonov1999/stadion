package koinot.uz.backend.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import koinot.uz.backend.entity.enums.BusinessType;
import koinot.uz.backend.entity.template.ReadyEntity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Entity(name = "stadium")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Stadium extends ReadyEntity {
    @OneToOne
    @JsonManagedReference
    private User user;

    @Column(length = 10000)
    private String name; //

    private double latitude;

    private String phone_number;

    private double longitude;

    @Column(length = 10000)
    private String address; //

    private Date opening_time; //

    private Date closing_time; //

    private Integer stadium_like = 0; //

    private Date change_price_time; //

    private double price_day_time; //

    private double price_night_time; //

    private Integer width = 0;

    private Integer height = 0;

    private double count_order; //

    private boolean open;

    private boolean active;

    private boolean verify;

    private boolean delete = false;

    // for future

    private double money;

    private String description;

    private BusinessType businessType = BusinessType.STADIUM;

    private boolean roof;
}
