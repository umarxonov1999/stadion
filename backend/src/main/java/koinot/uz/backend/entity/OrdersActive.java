package koinot.uz.backend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import koinot.uz.backend.entity.template.ReadyEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "OrdersActive")
public class OrdersActive extends ReadyEntity {

    @ManyToOne
    @JsonIgnore
    private User user;

    @ManyToOne
    @JsonIgnore
    private Stadium stadium;

    private double latitude;

    private double longitude;

    private double sum;

    private Date startDate;

    private Date endDate;

    private Date time;

    private boolean active = false;

    private boolean cancelOrder = false;

    private double countOrder;


    public OrdersActive(Stadium stadium,double latitude,double longitude,double sum,Date startDate,Date endDate,
                        Date time) {
        this.stadium   = stadium;
        this.latitude  = latitude;
        this.longitude = longitude;
        this.sum       = sum;
        this.startDate = startDate;
        this.endDate   = endDate;
        this.time      = time;
    }
}
