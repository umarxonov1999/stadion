package koinot.uz.backend.entity;

import koinot.uz.backend.entity.enums.AttachmentTypeEnumWhy;
import koinot.uz.backend.entity.template.ReadyEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "stadiumPhoto")
public class StadiumPhoto extends ReadyEntity {

    @ManyToOne
    private Stadium stadium;

    @Column(unique = true)
    private String photo;

    private String name;
    private String contentType; // ishlatilmagan
    private long size;
    private String path;
    private String extension;
    @ManyToOne
    private User user;

    @Enumerated(value = EnumType.STRING)
    private AttachmentTypeEnumWhy why;
}
