package koinot.uz.backend.entity;

import koinot.uz.backend.entity.template.ReadyEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Calendar;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "SystemVariable")
public class SystemVariable extends ReadyEntity {

    private Calendar calendar;

    @Column(unique = true)
    private String name;
}
