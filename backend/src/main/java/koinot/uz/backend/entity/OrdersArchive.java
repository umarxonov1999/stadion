package koinot.uz.backend.entity;

import koinot.uz.backend.entity.template.ReadyEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "OrdersArchive")
public class OrdersArchive extends ReadyEntity {

    @ManyToOne
    private User user;

    @ManyToOne
    private Stadium stadium;

    private double latitude;

    private double longitude;

    private double sum;

    private Date startDate;

    private Date endDate;

    private Date time;

    private boolean active;

    private boolean cancelOrder;

    private double countOrder;

}
