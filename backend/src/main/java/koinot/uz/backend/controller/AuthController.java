package koinot.uz.backend.controller;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.*;
import koinot.uz.backend.entity.User;
import koinot.uz.backend.exception.ExceptionSend;
import koinot.uz.backend.payload.*;
import koinot.uz.backend.repository.UserRepository;
import koinot.uz.backend.security.AuthService;
import koinot.uz.backend.security.CurrentUser;
import koinot.uz.backend.security.JwtTokenProvider;
import koinot.uz.backend.service.WorkingALot;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.text.ParseException;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping ( "/koinot/auth" )
@Api ( value = "Auth api" )
@Slf4j
public class AuthController {

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Autowired
    AuthenticationManager authenticate;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthService authService;

    @Autowired
    ExceptionSend exceptionSend;

    @Autowired
    WorkingALot workingALot;


    //**************** REGISTER USER ****************//
    @PermitAll
    @ApiOperation ( "this is user register " )
    @ApiResponses ( value = {
            @ApiResponse ( code = 409, message = "your request not suitable" ) ,
            @ApiResponse ( code = 200, message = "successes" ) ,
            @ApiResponse ( code = 403, message = "you are not allowed" ) , } )
    @PostMapping ( "/register" )
    @Validated
    public HttpEntity<ApiResponseModel> register(@Valid @RequestBody ReqUser reqUser , BindingResult error) {
        if (error.hasErrors()) {
            return ResponseEntity.badRequest()
                    .body(new ApiResponseModel(HttpStatus.BAD_REQUEST.value() ,
                            "field" ,
                            error.getFieldErrors()
                                    .stream()
                                    .map(fieldError -> new ErrorsField(fieldError.getField() ,
                                            fieldError.getDefaultMessage()))));
        }
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        String ip = request.getRemoteAddr();
        if (ip.length() > 0) {
            return authService.register(reqUser , ip);
        }
        return ResponseEntity.badRequest()
                .body(new ApiResponseModel(HttpStatus.CONFLICT.value() , "invalid ip address"));
    }

    //**************** LOGIN USER ****************//
    @PermitAll
    @PostMapping ( "/login" )
    @ApiOperation ( "phone number and password" )
    @Validated
    public HttpEntity<ApiResponseModel> login(@Valid @RequestBody ReqUser reqUser , BindingResult error) {
        if (error.hasErrors()) {
            return ResponseEntity.badRequest()
                    .body(new ApiResponseModel(HttpStatus.BAD_REQUEST.value() ,
                            "field" ,
                            error.getFieldErrors()
                                    .stream()
                                    .map(fieldError -> new ErrorsField(fieldError.getField() ,
                                            fieldError.getDefaultMessage()))));
        }
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        String ip = request.getRemoteAddr();
        if (ip.length() > 0) {
            return ResponseEntity.status(HttpStatus.ACCEPTED)
                    .body(new ApiResponseModel(HttpStatus.OK.value() ,
                            "login" ,
                            getApiToken(reqUser.getPhoneNumber() , reqUser.getPassword())));
        }
        return ResponseEntity.badRequest()
                .body(new ApiResponseModel(HttpStatus.BAD_REQUEST.value() , "invalid ip address" , null));

    }


    //**************** SEND CODE  ****************//
    @PostMapping ( "/sendCode/{phoneNumber}" )
    public HttpEntity<ApiResponseModel> sendCode(@PathVariable String phoneNumber) {
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        String ip = request.getRemoteAddr();
        if (ip.length() > 0) {
            return authService.sendCode(phoneNumber , ip);
        }
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(new ApiResponseModel(HttpStatus.CONFLICT.value() , "invalid ip address"));
    }


    //**************** VERIFY USER ****************//
    @PostMapping ( "/verify/{code}/{phoneNumber}" )
    public HttpEntity<ApiResponseModel> verify(@PathVariable String code , @PathVariable String phoneNumber) {
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();

        String ip = request.getRemoteAddr();

        Optional<User> byPhoneNumber = userRepository.findByPhoneNumber(phoneNumber);
        if (byPhoneNumber.isPresent()) {
            User user = byPhoneNumber.get();
            if (ip.isEmpty()) {
                return ResponseEntity.status(HttpStatus.CONFLICT)
                        .body(new ApiResponseModel(HttpStatus.CONFLICT.value() , "invalid ip address"));
            }
            if (!ip.equals(user.getIpAddress())) {
                return ResponseEntity.status(HttpStatus.CONFLICT)
                        .body(new ApiResponseModel(HttpStatus.CONFLICT.value() , "this is another device"));
            }
            return authService.verify(code , user);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ApiResponseModel(HttpStatus.BAD_REQUEST.value() , "this has not phone number"));
    }


    //**************** NEW PASSWORD ****************//
    @PostMapping ( "/newPassword/{password}" )
    @ApiImplicitParams ( @ApiImplicitParam ( name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token" ) )
    public HttpEntity<ApiResponseModel> newPassword(@PathVariable String password , @ApiIgnore @CurrentUser User user) {
        return authService.newPassword(password , user);
    }

    //**************** IS START ON BRBT USER ****************//
    @GetMapping ( "/isBrbtStart/{phoneNumber}" )
    public HttpEntity<ApiResponseModel> isBrbtStart(@PathVariable String phoneNumber) {
        return authService.isBrbtStart(phoneNumber);
    }

    //**************** VERIFY USER ****************//
    @PostMapping ( "/firebase/{code}" )
    @ApiImplicitParams ( @ApiImplicitParam ( name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token" ) )
    public HttpEntity<ApiResponseModel> firebase(@ApiIgnore @CurrentUser User user , @PathVariable String code) {
        return authService.firebase(code , user);
    }

    // **************** GET  YOURSELF  ****************//
    @GetMapping ( "/me" )
    @ApiImplicitParams ( @ApiImplicitParam ( name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token" ) )
    public HttpEntity<ApiResponseModel> getUser(@ApiIgnore @CurrentUser User user) {
        if (user == null) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body(new ApiResponseModel(HttpStatus.FORBIDDEN.value() , "forbidden" , null));
        }
        return ResponseEntity.status(HttpStatus.ACCEPTED)
                .body(new ApiResponseModel(HttpStatus.ACCEPTED.value() , "user info" , user));
    }

    // **************** GET  YOURSELF  ****************//
    @PostMapping ( "/editMe" )
    @Validated
    @ApiImplicitParams ( @ApiImplicitParam ( name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token" ) )
    public HttpEntity<ApiResponseModel> editMe(@ApiIgnore @CurrentUser User user ,
                                               @Valid @RequestBody ReqUserProfile forEdit ,
                                               BindingResult error) throws ParseException {
        if (error.hasErrors()) {
            return ResponseEntity.badRequest()
                    .body(new ApiResponseModel(HttpStatus.BAD_REQUEST.value() ,
                            "field" ,
                            error.getFieldErrors()
                                    .stream()
                                    .map(fieldError -> new ErrorsField(fieldError.getField() ,
                                            fieldError.getDefaultMessage()))));
        }
        return authService.editMe(user , forEdit);
    }

    public JwtResponse getApiToken(String phoneNumber , String password) {
        Authentication authentication = authenticate.authenticate(new UsernamePasswordAuthenticationToken(phoneNumber ,
                password));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return new JwtResponse(jwtTokenProvider.generateToken(authentication));
    }

    @ExceptionHandler
    @ResponseStatus ( code = HttpStatus.BAD_REQUEST )
    public HttpEntity<ApiResponseModel> handleException(@ApiIgnore @CurrentUser User user , Exception e) {
        exceptionSend.senException(" AuthController handleException => " , e , user);
        log.error(" AuthController handleException => " , e);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ApiResponseModel(HttpStatus.BAD_REQUEST.value() , e.getMessage() , e.hashCode()));
    }
}
