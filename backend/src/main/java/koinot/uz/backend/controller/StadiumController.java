package koinot.uz.backend.controller;

import io.swagger.annotations.*;
import koinot.uz.backend.entity.User;
import koinot.uz.backend.exception.ExceptionSend;
import koinot.uz.backend.payload.ApiResponseModel;
import koinot.uz.backend.payload.ErrorsField;
import koinot.uz.backend.payload.ReqStadium;
import koinot.uz.backend.security.CurrentUser;
import koinot.uz.backend.service.StadiumService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import springfox.documentation.annotations.ApiIgnore;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/koinot/stadium")
@Slf4j
public class StadiumController {


    @Autowired
    StadiumService stadiumService;

    @Autowired
    ExceptionSend exceptionSend;


    //**************** GET STADIUM USER ****************//
    @GetMapping()
    public HttpEntity<ApiResponseModel> getStadium() {
        return ResponseEntity.status( HttpStatus.OK )
                .body( new ApiResponseModel( HttpStatus.OK.value(),"here it is",stadiumService.getStadium() ) );
    }

    //**************** GET STADIUM ORDERS  ****************//
    @GetMapping("/{id}")
    public HttpEntity<ApiResponseModel> getStadiumOrder(@PathVariable(required = false) Long id) {
        return ResponseEntity.status( HttpStatus.OK )
                .body( new ApiResponseModel( HttpStatus.OK.value(),
                        "here it is",
                        stadiumService.getStadiumOrder( id ) ) );
    }


    // **************  GET USER  ****************//
    @GetMapping("/search")
    @ApiResponses(value = {})
    public HttpEntity<?> findTheNearestEmptyStadium(@RequestParam(value = "lat") Double lat,
                                                    @RequestParam(value = "lng") Double lng,
                                                    @RequestParam(value = "r0", required = false) Optional<Integer> r0,
                                                    @RequestParam(value = "r", required = false) Optional<Integer> r) {
        try{
            return stadiumService.searchStadiumComfort( lat,lng,r0,r );
        }catch(Exception e){
            return ResponseEntity.badRequest()
                    .body( new ApiResponseModel( HttpStatus.BAD_REQUEST.value(),
                            "The request is incorrect",
                            List.of( new ErrorsField( "error",e.getMessage() ) ) ) );

        }
    }


    //**************** GET STADIUM PHOTO UPLOAD ****************//
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
    @PostMapping("/uploadPhotoFileList/{id}")
    public HttpEntity<ApiResponseModel> uploadFile(@PathVariable Long id,MultipartHttpServletRequest request,
                                                   @ApiIgnore @CurrentUser User user) {
        return stadiumService.uploadPhotoFileList( request,id,user );
    }

    //**************** GET STADIUM PHOTO  ****************//
    @GetMapping("/byteFile/{id}") //
    public HttpEntity<?> byteFile(@PathVariable(required = false) Long id) throws IOException {
        return stadiumService.byteFile( id );
    }

    //**************** GET STADIUM PHOTO  ****************//
    @ApiIgnore
    @GetMapping("/image/{name}") //
    public HttpEntity<?> image(@PathVariable String name) throws IOException {
        return stadiumService.image( name );
    }

    //**************** GET STADIUM PHOTO DELETE ****************//
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
    @DeleteMapping("/deletePhoto/{id}") //
    public HttpEntity<ApiResponseModel> deletePhoto(@PathVariable Long id,@ApiIgnore @CurrentUser User user) {
        return stadiumService.deletePhoto( id,user );
    }

    //**************** GET STADIUM  ****************//
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
    @GetMapping("/myStadium")
    public HttpEntity<ApiResponseModel> myStadium(@ApiIgnore @CurrentUser User user) {
        return stadiumService.myStadium( user );
    }

    //**************** GET STADIUM ORDERS ACTIVE ****************//
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
    @GetMapping("/myStadiumOrder/{id}")
    public HttpEntity<ApiResponseModel> myStadiumOrder(@ApiIgnore @CurrentUser User user,@PathVariable Long id) {
        return stadiumService.myStadiumOrder( user,id );
    }

    //**************** GET STADIUM ORDERS SET ACTIVE ****************//
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
    @GetMapping("/activeOrder/{id}")
    public HttpEntity<ApiResponseModel> activeOrder(@ApiIgnore @CurrentUser User user,@PathVariable Long id) {
        return stadiumService.activeOrder( user,id );
    }

    //**************** GET STADIUM ORDERS ACTIVE ****************//
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
    @GetMapping("/cancelOrder/{id}")
    public HttpEntity<ApiResponseModel> cancelOrder(@ApiIgnore @CurrentUser User user,@PathVariable Long id) {
        return stadiumService.cancelOrder( user,id );
    }

    //**************** GET SAVE OR EDIT STADIUM ****************//
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
    @PostMapping("/saveOrEdit")
    public HttpEntity<ApiResponseModel> saveOrEditStadium(@ApiIgnore @CurrentUser User user,
                                                          @RequestBody ReqStadium reqStadium,BindingResult error) {
        if(error.hasErrors()){
            return ResponseEntity.badRequest()
                    .body( new ApiResponseModel( HttpStatus.BAD_REQUEST.value(),
                            "field",
                            error.getFieldErrors()
                                    .stream()
                                    .map( fieldError -> new ErrorsField( fieldError.getField(),
                                            fieldError.getDefaultMessage() ) ) ) );
        }
        return stadiumService.saveOrEditStadium( user,reqStadium );
    }

    //**************** GET SAVE OR EDIT STADIUM ****************//
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
    @PostMapping("/delete/{id}")
    public HttpEntity<ApiResponseModel> delete(@ApiIgnore @CurrentUser User user,@PathVariable Long id) {
        return stadiumService.delete( user,id );
    }

    //**************** GET SAVE OR EDIT STADIUM ****************//
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
    @GetMapping("/getCancel/{id}")
    public HttpEntity<ApiResponseModel> getCancel(@ApiIgnore @CurrentUser User user,@PathVariable Long id) {
        return stadiumService.getCancel( user,id );
    }

    //**************** GET SAVE OR EDIT STADIUM ****************//
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
    @PostMapping("/deleteCancel/{id}")
    public HttpEntity<ApiResponseModel> deleteCancel(@ApiIgnore @CurrentUser User user,@PathVariable Long id) {
        return stadiumService.deleteCancel( user,id );
    }

    @ExceptionHandler
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public HttpEntity<ApiResponseModel> handleException(@ApiIgnore @CurrentUser User user,Exception e) {
        exceptionSend.senException( " StadiumController handleException => ",e,user );
        log.error( " StadiumController handleException => ",e );
        return ResponseEntity.status( HttpStatus.BAD_REQUEST )
                .body( new ApiResponseModel( HttpStatus.BAD_REQUEST.value(),e.getMessage(),e.hashCode() ) );
    }
}
