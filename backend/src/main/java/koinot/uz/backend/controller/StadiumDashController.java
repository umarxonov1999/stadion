package koinot.uz.backend.controller;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import koinot.uz.backend.entity.User;
import koinot.uz.backend.exception.ExceptionSend;
import koinot.uz.backend.payload.ApiResponseModel;
import koinot.uz.backend.security.CurrentUser;
import koinot.uz.backend.service.StadiumDashService;
import koinot.uz.backend.utils.AppConstants;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.sql.Timestamp;

@ApiOperation(value = "/koinot/stadiumDash", tags = "stadion xo'jayini uchun statistika")
@PreAuthorize("hasAnyAuthority('ADMIN')")
@RestController
@RequestMapping("/koinot/stadiumDash")
@Slf4j
public class StadiumDashController {

    @Autowired
    StadiumDashService stadiumService;
    @Autowired
    ExceptionSend exceptionSend;


    //**************** GET STADIUM ORDERS ACTIVE ****************//
    @GetMapping("/graph/{stadiumId}")
    @ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
    public HttpEntity<ApiResponseModel> graph(
            @RequestParam(value = "startDate", defaultValue = AppConstants.BEGIN_DATE) Timestamp startDate,
            @RequestParam(value = "endDate", defaultValue = AppConstants.END_DATE) Timestamp endDate,
            @ApiIgnore @CurrentUser User user,@PathVariable Long stadiumId) {
        return stadiumService.graph( user,stadiumId,startDate,endDate );
    }

    //**************** GET STADIUM ORDERS ACTIVE ****************//
    @GetMapping("/archiveAll/{stadiumId}")
    @ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
    public HttpEntity<ApiResponseModel> archiveAll(@ApiIgnore @CurrentUser User user,@PathVariable Long stadiumId) {
        return stadiumService.archiveAll( user,stadiumId );
    }

    //**************** GET STADIUM ORDERS ACTIVE ****************//
    @GetMapping("/archiveAfterCreateTime/{stadiumId}")
    @ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
    public HttpEntity<ApiResponseModel> archiveAfterCreateTime(
            @RequestParam(value = "time", defaultValue = AppConstants.END_DATE) Long time,
            @ApiIgnore @CurrentUser User user,@PathVariable Long stadiumId) {
        return stadiumService.archiveAfterCreateTime( user,stadiumId,time );
    }

    @ExceptionHandler
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public HttpEntity<ApiResponseModel> handleException(@ApiIgnore @CurrentUser User user,Exception e) {
        exceptionSend.senException( " StadiumDashController handleException => ",e,user );
        log.error( " StadiumDashController handleException => ",e );
        return ResponseEntity.status( HttpStatus.BAD_REQUEST )
                .body( new ApiResponseModel( HttpStatus.BAD_REQUEST.value(),e.getMessage(),e.hashCode() ) );
    }
}
