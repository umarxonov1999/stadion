package koinot.uz.backend.controller;

import koinot.uz.backend.entity.User;
import koinot.uz.backend.exception.ExceptionSend;
import koinot.uz.backend.payload.ApiResponseModel;
import koinot.uz.backend.repository.StadiumRepository;
import koinot.uz.backend.security.CurrentUser;
import koinot.uz.backend.service.StadiumService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import springfox.documentation.annotations.ApiIgnore;

@Controller
//@RequestMapping("/")
@Slf4j
public class WebController {

    @Autowired
    StadiumRepository stadiumRepository;

    @Autowired
    ExceptionSend exceptionSend;

    @Autowired
    StadiumService stadiumService;


    @GetMapping("/")
    public String web() {
        return "brbt";
    }

    @GetMapping("/team")
    public String team() {
        return "team";
    }

    @ExceptionHandler
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ApiResponseModel handleException(@ApiIgnore @CurrentUser User user,Exception e) {
        exceptionSend.senException( " WebController handleException => ",e,user );
        log.error( " WebController handleException => ",e );
        return new ApiResponseModel( HttpStatus.BAD_REQUEST.value(),e.getMessage() );
    }
}
