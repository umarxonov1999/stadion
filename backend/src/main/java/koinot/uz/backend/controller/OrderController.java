package koinot.uz.backend.controller;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import koinot.uz.backend.entity.User;
import koinot.uz.backend.exception.ExceptionSend;
import koinot.uz.backend.payload.ApiResponseModel;
import koinot.uz.backend.payload.ErrorsField;
import koinot.uz.backend.payload.ReqAdminOrder;
import koinot.uz.backend.payload.ReqOrder;
import koinot.uz.backend.security.CurrentUser;
import koinot.uz.backend.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

@RestController
@RequestMapping("/koinot/order")
@Slf4j
public class OrderController {

    @Autowired
    OrderService orderService;


    @Autowired
    ExceptionSend exceptionSend;


    //**************** GET STADIUM PRICE  ****************//
    @GetMapping("/price/{id}/{start}/{end}")
    public HttpEntity<ApiResponseModel> getStadiumPrice(@PathVariable String end,@PathVariable Long id,
                                                        @PathVariable String start) {
        return orderService.getPrice( start,end,id );
    }

    //**************** GET STADIUM PRICE  ****************//
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
    @GetMapping("/searchNumber/{phoneNumber}")
    public HttpEntity<ApiResponseModel> searchNumber(@PathVariable String phoneNumber) {
        return orderService.searchNumber( phoneNumber );
    }

    //**************** GET STADIUM PRICE  ****************//
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
    @PostMapping("/adminCreateOrder")
    @Validated
    public HttpEntity<ApiResponseModel> adminCreateOrder(@Valid @RequestBody ReqAdminOrder order,
                                                         @ApiIgnore @CurrentUser User user,BindingResult error) {
        if(error.hasErrors()){
            return ResponseEntity.badRequest()
                    .body( new ApiResponseModel( HttpStatus.BAD_REQUEST.value(),
                            "field",
                            error.getFieldErrors()
                                    .stream()
                                    .map( fieldError -> new ErrorsField( fieldError.getField(),
                                            fieldError.getDefaultMessage() ) ) ) );
        }
        return orderService.adminCreateOrder( order,user );
    }


    @PostMapping("/clientCreate")
    @CrossOrigin
    @Validated
    public HttpEntity<ApiResponseModel> creteOrder(@Valid @RequestBody ReqOrder order,BindingResult error) {
        if(error.hasErrors()){
            return ResponseEntity.badRequest()
                    .body( new ApiResponseModel( HttpStatus.BAD_REQUEST.value(),
                            "field",
                            error.getFieldErrors()
                                    .stream()
                                    .map( fieldError -> new ErrorsField( fieldError.getField(),
                                            fieldError.getDefaultMessage() ) ) ) );
        }
        return orderService.clientCreateOrder( order );
    }

    @ExceptionHandler
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public HttpEntity<ApiResponseModel> handleException(@ApiIgnore @CurrentUser User user,Exception e) {
        exceptionSend.senException( " OrderController handleException => ",e,user );
        log.error( " OrderController handleException => ",e );
        return ResponseEntity.status( HttpStatus.BAD_REQUEST )
                .body( new ApiResponseModel( HttpStatus.BAD_REQUEST.value(),e.getMessage(),e.hashCode() ) );
    }
}
