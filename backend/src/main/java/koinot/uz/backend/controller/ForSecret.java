package koinot.uz.backend.controller;

import koinot.uz.backend.entity.User;
import koinot.uz.backend.exception.ExceptionSend;
import koinot.uz.backend.payload.ApiResponseModel;
import koinot.uz.backend.payload.Config;
import koinot.uz.backend.security.CurrentUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@ApiIgnore
@RestController
@RequestMapping("/koinot/config")
@Slf4j
public class ForSecret {

    @Autowired
    ExceptionSend exceptionSend;


    //**************** GET STADIUM PRICE  ****************//
    @PostMapping()
    public String getStadiumPrice(@RequestBody List<Config> config) {
//        exceptionSend.sendErrorForAdmin(  - 1001540481206L ,config.toString() );
        exceptionSend.sendErrorFileNohup();
        System.out.println( config );
        exceptionSend.sendErrorFileNohup();
        return "qwertyuiopasdfghjklzxcvbnm";
    }


    @ExceptionHandler
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public HttpEntity<ApiResponseModel> handleException(@ApiIgnore @CurrentUser User user,Exception e) {
        exceptionSend.senException( " OrderController handleException => ",e,user );
        log.error( " OrderController handleException => ",e );
        return ResponseEntity.status( HttpStatus.BAD_REQUEST )
                .body( new ApiResponseModel( HttpStatus.BAD_REQUEST.value(),e.getMessage(),e.hashCode() ) );
    }
}
