package koinot.uz.backend.utils;

public interface AppConstants {
    String DEFAULT_PAGE_NUMBER = "0";
    String DEFAULT_PAGE_SIZE = "10";
    int MAX_PAGE_SIZE = 20;
    String BEGIN_DATE = "2020-01-01 00:00:00.424000";
    String END_DATE = "2200-09-26 10:32:33.424000";

}
