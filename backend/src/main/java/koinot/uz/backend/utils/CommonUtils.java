package koinot.uz.backend.utils;


import koinot.uz.backend.exception.BadRequestException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

public class CommonUtils {
    public static void validatePageNumberAndSize(int page,int size) {
        if(page < 0){
            throw new BadRequestException( "Sahifa soni noldan kam bo'lishi mumkin emas." );
        }

        if(size > AppConstants.MAX_PAGE_SIZE){
            throw new BadRequestException( "Sahifa soni " + AppConstants.MAX_PAGE_SIZE + " dan ko'p bo'lishi mumkin emas." );
        }
    }


    public static Pageable getPageable(int page,int size) {
        validatePageNumberAndSize( page,size );
        return PageRequest.of( page,size,Sort.Direction.DESC,"createdAt" );
    }

    public static Pageable getPageableRating(int page,int size) {
        validatePageNumberAndSize( page,size );
        return PageRequest.of( page,size,Sort.Direction.DESC,"rating" );
    }

    public static Pageable getPageableLook(int page,int size) {
        validatePageNumberAndSize( page,size );
        return PageRequest.of( page,size,Sort.Direction.DESC,"look" );
    }

    public static Pageable getPageableById(int page,int size) {
        validatePageNumberAndSize( page,size );
        return PageRequest.of( page,size,Sort.Direction.DESC,"id" );
    }

    public static Timestamp validTimestamp(Timestamp timestamp,Boolean isFrom) {
        if(isFrom) return timestamp == null ? new Timestamp( 1 ) : timestamp;
        return timestamp == null ? new Timestamp( System.currentTimeMillis() ) : timestamp;
    }

    public static String thousandSeparator(Integer a) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance( Locale.US );
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();

        symbols.setGroupingSeparator( ' ' );
        formatter.setDecimalFormatSymbols( symbols );
        return formatter.format( a.longValue() );
    }

}
