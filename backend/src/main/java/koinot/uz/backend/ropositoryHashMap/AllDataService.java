package koinot.uz.backend.ropositoryHashMap;


import koinot.uz.backend.entity.OrdersActive;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class AllDataService {

    private static HashMap<Long, OrdersActive> orderActiveMap;
    private static HashMap<Long, List<OrdersActive>> stadiumId;

    private static AllDataService INSTANCE = new AllDataService();

    private AllDataService() {
        initialize();
    }

    public static AllDataService getInstance() {
        return INSTANCE;
    }

    private static void initialize() {
        orderActiveMap = new HashMap<Long, OrdersActive>();
        stadiumId      = new HashMap<Long, List<OrdersActive>>();
    }

    public List<OrdersActive> getAllOrdersActives() {
        synchronized (AllDataService.class) {
            return new ArrayList<OrdersActive>( orderActiveMap.values() );
        }
    }


    //////////////////////////***********  GET   ***********//////////////////////////////

    public List<OrdersActive> getOrdersActives(Long byStadium) {
        synchronized (AllDataService.class) {
            try{
                return stadiumId.get( byStadium );
            }catch(Exception e){
                return null;
            }
        }
    }

    public boolean orderActiveExist(long id) {
        synchronized (AllDataService.class) {
            return orderActiveMap.containsKey( id );
        }
    }

    public OrdersActive getOrdersActive(long id) {
        synchronized (AllDataService.class) {
            return orderActiveMap.get( id );
        }
    }


    /////////////////////////////////////////////////////////


    //////////////////////////***********  SET   ***********//////////////////////////////

    public void addOrdersActive(OrdersActive orderActive) {
        synchronized (AllDataService.class) {
            orderActiveMap.put( orderActive.getId(),orderActive );
            addToStatusMap( orderActive );
        }
    }

    public void addAllOrdersActive(List<OrdersActive> orderActives) {
        synchronized (AllDataService.class) {
            orderActives.forEach( ordersActive -> {
                orderActiveMap.put( ordersActive.getId(),ordersActive );
                addToStatusMap( ordersActive );
            } );

        }
    }

    public void updateOrdersActive(OrdersActive orderActive) {
        synchronized (AllDataService.class) {
            // Update the id map first.
            updateStatusChange( orderActive );
            orderActiveMap.put( orderActive.getId(),orderActive );
        }
    }

    public OrdersActive removeOrdersActive(long id) {
        synchronized (AllDataService.class) {
            removeFromStatusMap( orderActiveMap.get( id ) );
            return orderActiveMap.remove( id );
        }
    }

    /////////////////////////////////////////////////////////


    private void addToStatusMap(OrdersActive orderActive) {
        synchronized (AllDataService.class) {
            Long byStadium = orderActive.getStadium().getId();
            List<OrdersActive> orderActives;

            // If an existing entry will be updated.
            if(stadiumId.containsKey( byStadium )){
                orderActives = stadiumId.get( byStadium );
            }
            // If a new entry is created.
            else{
                orderActives = new ArrayList<OrdersActive>();
                stadiumId.put( byStadium,orderActives );
            }

            orderActives.add( orderActive );
        }
    }


    private void updateStatusChange(OrdersActive updatedOrdersActive) {
        synchronized (AllDataService.class) {
            // Continue only if the given order active already exist
            if(orderActiveMap.containsKey( updatedOrdersActive.getId() )){
                OrdersActive existingOrdersActive = orderActiveMap.get( updatedOrdersActive.getId() );

                // Check if the id is changed.
                if(! existingOrdersActive.getStadium().getId().equals( updatedOrdersActive.getStadium().getId() )){
                    // Remove the order active from the map of previous id
                    removeFromStatusMap( existingOrdersActive );
                    // Add the order active to the map of current id
                    addToStatusMap( updatedOrdersActive );
                }
            }
        }
    }

    private void removeFromStatusMap(OrdersActive orderActive) {
        synchronized (AllDataService.class) {
            // Continue only if the given order active already exist
            if(orderActiveMap.containsKey( orderActive.getId() )){
                if(stadiumId.containsKey( orderActive.getStadium().getId() )){
                    stadiumId.get( orderActive.getStadium().getId() ).remove( orderActive );
                }
            }
        }
    }
}
