package koinot.uz.backend.ropositoryHashMap;

import koinot.uz.backend.entity.OrdersActive;
import koinot.uz.backend.repository.OrderActiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AllData {

    @Autowired
    OrderActiveRepository orderActiveRepository;

    public List<OrdersActive> getOrder() {
        return AllDataService.getInstance().getAllOrdersActives();
    }

    public void refresh() {
        AllDataService.getInstance().addAllOrdersActive( orderActiveRepository.findAllByActive( true ) );
    }

}
